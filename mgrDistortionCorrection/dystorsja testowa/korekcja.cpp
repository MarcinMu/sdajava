// dystorsja testowa.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <string>
#include <algorithm>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int wyswietl_zdjecie (IplImage* obraz,char nazwa_okna [20]);

int pomoc()
{
	cout<<"\n\t\t\t\tPomoc:\n\n\n";
	cout<<"Parametry wejsciowe programu:\n";
	cout<<"-h Pomoc\n";
	cout<<"-t Tryb pracy\n";
	cout<<"-s Sciezka dostepu do katalogu z danymi\n";
	cout<<"-r Rozszerzenie plikow\n";
	cout<<"-l Liczba plansz testowych\n";
	cout<<"-n Nazwa pliku, ktory ma byc poddany korekcji\n";
	cout<<"-p Nazwa pliku zawierajacego parametry korekcji dystorsji\n";
	cout<<"-m nazwa pliku zawierajacego maske do korekcji winietowania \n";
	cout<<"-i informacje o programie\n";

	cout<<"\n Jesli nie podasz danych przy wywolaniu funkcji zostaniesz poproszony o ich podanie.\n";
	cout<<"\n Czy kontynuowac dzialanie programu? (T/N)";
	
	char odp[30];
	scanf("%s", & odp);
	if ((strcmp(odp,"T") == 0)||((strcmp(odp,"t"))==0)||(strcmp(odp,"Y")==0)||(strcmp(odp,"y")==0)) 		return 1;
	return 0;
}

int info()
{
	cout<<"\n Korzystasz z programu korekcji dystorsji i winietowania KoDiW\n";
	cout<<"Autor programu: Marcin Musial\n";
	cout<<"Program umo�liwia:\n\tKalibracje toru optycznego pod wzgledem dystorsji\n\tUsuwanie dystorsji\n\tUsuwanie winietowania\n";
	cout<<"\n Czy kontynuowac dzialanie programu? (T/N)";
	
	char odp[30];
	scanf("%s", & odp);
	if ((strcmp(odp,"T") == 0)||((strcmp(odp,"t"))==0)||(strcmp(odp,"Y")==0)||(strcmp(odp,"y")==0)) 		return 1;
	return 0;
}




CV_IMPL double kalib( const CvMat* objectPoints,
                    const CvMat* imagePoints, const CvMat* npoints,
                    CvSize imageSize, CvMat* cameraMatrix, CvMat* distCoeffs,
                    CvMat* rvecs, CvMat* tvecs, int flags )
{
    const int NINTRINSIC = 9;
    CvMat *matM, *_m, *_Ji, *_Je, *_err;
    CvLevMarq solver;
    double reprojErr = 0;

    double A[9], k[5] = {0,0,0,0,0};
    CvMat matA = cvMat(3, 3, CV_64F, A), _k;
    int i, nimages, maxPoints = 0, ni = 0, pos, total = 0, nparams, npstep, cn;
    double aspectRatio = 0.;

    // 0. check the parameters & allocate buffers
								if( !CV_IS_MAT(objectPoints) || !CV_IS_MAT(imagePoints) ||
									!CV_IS_MAT(npoints) || !CV_IS_MAT(cameraMatrix) || !CV_IS_MAT(distCoeffs) )
									CV_Error( CV_StsBadArg, "One of required vector arguments is not a valid matrix" );

								if( imageSize.width <= 0 || imageSize.height <= 0 )
									CV_Error( CV_StsOutOfRange, "image width and height must be positive" );

								if( CV_MAT_TYPE(npoints->type) != CV_32SC1 ||
									(npoints->rows != 1 && npoints->cols != 1) )
									CV_Error( CV_StsUnsupportedFormat,
										"the array of point counters must be 1-dimensional integer vector" );

    nimages = npoints->rows*npoints->cols;
    npstep = npoints->rows == 1 ? 1 : npoints->step/CV_ELEM_SIZE(npoints->type);

								if( rvecs )
								{
									cn = CV_MAT_CN(rvecs->type);
									if( !CV_IS_MAT(rvecs) ||
										(CV_MAT_DEPTH(rvecs->type) != CV_32F && CV_MAT_DEPTH(rvecs->type) != CV_64F) ||
										((rvecs->rows != nimages || (rvecs->cols*cn != 3 && rvecs->cols*cn != 9)) &&
										(rvecs->rows != 1 || rvecs->cols != nimages || cn != 3)) )
										CV_Error( CV_StsBadArg, "the output array of rotation vectors must be 3-channel "
											"1xn or nx1 array or 1-channel nx3 or nx9 array, where n is the number of views" );
								}

								if( tvecs )
								{
									cn = CV_MAT_CN(tvecs->type);
									if( !CV_IS_MAT(tvecs) ||
										(CV_MAT_DEPTH(tvecs->type) != CV_32F && CV_MAT_DEPTH(tvecs->type) != CV_64F) ||
										((tvecs->rows != nimages || tvecs->cols*cn != 3) &&
										(tvecs->rows != 1 || tvecs->cols != nimages || cn != 3)) )
										CV_Error( CV_StsBadArg, "the output array of translation vectors must be 3-channel "
											"1xn or nx1 array or 1-channel nx3 array, where n is the number of views" );
								}

								if( (CV_MAT_TYPE(cameraMatrix->type) != CV_32FC1 &&
									CV_MAT_TYPE(cameraMatrix->type) != CV_64FC1) ||
									cameraMatrix->rows != 3 || cameraMatrix->cols != 3 )
									CV_Error( CV_StsBadArg,
										"Intrinsic parameters must be 3x3 floating-point matrix" );

								if( (CV_MAT_TYPE(distCoeffs->type) != CV_32FC1 &&
									CV_MAT_TYPE(distCoeffs->type) != CV_64FC1) ||
									(distCoeffs->cols != 1 && distCoeffs->rows != 1) ||
									(distCoeffs->cols*distCoeffs->rows != 4 &&
									distCoeffs->cols*distCoeffs->rows != 5) )
									CV_Error( CV_StsBadArg,
										"Distortion coefficients must be 4x1, 1x4, 5x1 or 1x5 floating-point matrix" );

    for( i = 0; i < nimages; i++ )
    {
        ni = npoints->data.i[i*npstep];
									if( ni < 4 )
									{
										char buf[100];
										sprintf( buf, "The number of points in the view #%d is < 4", i );
										CV_Error( CV_StsOutOfRange, buf );
									}
        maxPoints = MAX( maxPoints, ni );
        total += ni;
    }

    matM = cvCreateMat( 1, total, CV_64FC3 );
    _m = cvCreateMat( 1, total, CV_64FC2 );

    cvConvertPointsHomogeneous( objectPoints, matM );  // konwersja na wsp�rz�dne jednorodne
    cvConvertPointsHomogeneous( imagePoints, _m );

    nparams = NINTRINSIC + nimages*6;
    _Ji = cvCreateMat( maxPoints*2, NINTRINSIC, CV_64FC1 );
    _Je = cvCreateMat( maxPoints*2, 6, CV_64FC1 );
    _err = cvCreateMat( maxPoints*2, 1, CV_64FC1 );
    cvZero( _Ji );

    _k = cvMat( distCoeffs->rows, distCoeffs->cols, CV_MAKETYPE(CV_64F,CV_MAT_CN(distCoeffs->type)), k);
    if( distCoeffs->rows*distCoeffs->cols*CV_MAT_CN(distCoeffs->type) == 4 )
        flags |= CV_CALIB_FIX_K3;

    // 1. initialize intrinsic parameters & LM solver
    if( flags & CV_CALIB_USE_INTRINSIC_GUESS )
    {
									cvConvert( cameraMatrix, &matA );
									if( A[0] <= 0 || A[4] <= 0 )
										CV_Error( CV_StsOutOfRange, "Focal length (fx and fy) must be positive" );
									if( A[2] < 0 || A[2] >= imageSize.width ||
										A[5] < 0 || A[5] >= imageSize.height )
										CV_Error( CV_StsOutOfRange, "Principal point must be within the image" );
									if( fabs(A[1]) > 1e-5 )
										CV_Error( CV_StsOutOfRange, "Non-zero skew is not supported by the function" );
									if( fabs(A[3]) > 1e-5 || fabs(A[6]) > 1e-5 ||
										fabs(A[7]) > 1e-5 || fabs(A[8]-1) > 1e-5 )
										CV_Error( CV_StsOutOfRange,
											"The intrinsic matrix must have [fx 0 cx; 0 fy cy; 0 0 1] shape" );
									A[1] = A[3] = A[6] = A[7] = 0.;
									A[8] = 1.;

									if( flags & CV_CALIB_FIX_ASPECT_RATIO )
										aspectRatio = A[0]/A[4];
									cvConvert( distCoeffs, &_k );
    }
    else
    {
        CvScalar mean, sdv;
        cvAvgSdv( matM, &mean, &sdv );  // wyliczenie �redniej i odchylenia standardowego
									if( fabs(mean.val[2]) > 1e-5 || fabs(sdv.val[2]) > 1e-5 )
										CV_Error( CV_StsBadArg,
										"For non-planar calibration rigs the initial intrinsic matrix must be specified" );
        for( i = 0; i < total; i++ )
            ((CvPoint3D64f*)matM->data.db)[i].z = 0.;

        if( flags & CV_CALIB_FIX_ASPECT_RATIO )
        {
            aspectRatio = cvmGet(cameraMatrix,0,0);  //odczytanie warto�ci z macierzy cameraMatrix
            aspectRatio /= cvmGet(cameraMatrix,1,1);
									if( aspectRatio < 0.01 || aspectRatio > 100 )
										CV_Error( CV_StsOutOfRange,
											"The specified aspect ratio (=A[0][0]/A[1][1]) is incorrect" );
        }
        cvInitIntrinsicParams2D( matM, _m, npoints, imageSize, &matA, aspectRatio ); //ustalenie warto�ci pocz�tkowych
    }

    solver.init( nparams, 0, cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS,30,DBL_EPSILON) ); // wprowadzenie danych do algortymu LevMarq

    {
    double* param = solver.param->data.db;
    uchar* mask = solver.mask->data.ptr;

    param[0] = A[0]; param[1] = A[4]; param[2] = A[2]; param[3] = A[5];
    param[4] = k[0]; param[5] = k[1]; param[6] = k[2]; param[7] = k[3];
    param[8] = k[4];

	/////////////////////
		//cout<<"\nWiersz 551 \t";
	//int liczba=flags;
  //  string konwert;
	//while(liczba!=0)
	//{
	//	    if (liczba%2==1) konwert="1"+konwert;
   //         if (liczba%2==0) konwert="0"+konwert;
   //         liczba=liczba/2;
	//}
	//cout<<konwert;
	//cin>>liczba;
	//////////////////////////////
									if( flags & CV_CALIB_FIX_FOCAL_LENGTH )
										mask[0] = mask[1] = 0;
									if( flags & CV_CALIB_FIX_PRINCIPAL_POINT )
										mask[2] = mask[3] = 0;
									if( flags & CV_CALIB_ZERO_TANGENT_DIST )
									{
										param[6] = param[7] = 0;
										mask[6] = mask[7] = 0;
									}
									if( flags & CV_CALIB_FIX_K1 )
										mask[4] = 0;
									if( flags & CV_CALIB_FIX_K2 )
										mask[5] = 0;
									if( flags & CV_CALIB_FIX_K3 )
										mask[8] = 0;
									}

    // 2. initialize extrinsic parameters
    for( i = 0, pos = 0; i < nimages; i++, pos += ni )
    {
        CvMat _Mi, _mi, _ri, _ti;
        ni = npoints->data.i[i*npstep];

        cvGetRows( solver.param, &_ri, NINTRINSIC + i*6, NINTRINSIC + i*6 + 3 );
        cvGetRows( solver.param, &_ti, NINTRINSIC + i*6 + 3, NINTRINSIC + i*6 + 6 );

        cvGetCols( matM, &_Mi, pos, pos + ni );
        cvGetCols( _m, &_mi, pos, pos + ni );

        cvFindExtrinsicCameraParams2( &_Mi, &_mi, &matA, &_k, &_ri, &_ti );
    }

    // 3. run the optimization
    for(;;)
    {
        const CvMat* _param = 0;
        CvMat *_JtJ = 0, *_JtErr = 0;
        double* _errNorm = 0;
        bool proceed = solver.updateAlt( _param, _JtJ, _JtErr, _errNorm );
        double *param = solver.param->data.db, *pparam = solver.prevParam->data.db;

        if( flags & CV_CALIB_FIX_ASPECT_RATIO )
        {
            param[0] = param[1]*aspectRatio;
            pparam[0] = pparam[1]*aspectRatio;
        }

        A[0] = param[0]; A[4] = param[1];
        A[2] = param[2]; A[5] = param[3];
        k[0] = param[4]; k[1] = param[5]; k[2] = param[6];
        k[3] = param[7];
        k[4] = param[8];

        if( !proceed )
            break;
        
        reprojErr = 0;

        for( i = 0, pos = 0; i < nimages; i++, pos += ni )
        {
            CvMat _Mi, _mi, _ri, _ti, _dpdr, _dpdt, _dpdf, _dpdc, _dpdk, _mp, _part;
            ni = npoints->data.i[i*npstep];

            cvGetRows( solver.param, &_ri, NINTRINSIC + i*6, NINTRINSIC + i*6 + 3 );
            cvGetRows( solver.param, &_ti, NINTRINSIC + i*6 + 3, NINTRINSIC + i*6 + 6 );

            cvGetCols( matM, &_Mi, pos, pos + ni );
            cvGetCols( _m, &_mi, pos, pos + ni );

            _Je->rows = _Ji->rows = _err->rows = ni*2;
            cvGetCols( _Je, &_dpdr, 0, 3 );
            cvGetCols( _Je, &_dpdt, 3, 6 );
            cvGetCols( _Ji, &_dpdf, 0, 2 );
            cvGetCols( _Ji, &_dpdc, 2, 4 );
            cvGetCols( _Ji, &_dpdk, 4, NINTRINSIC );
            cvReshape( _err, &_mp, 2, 1 );

            if( _JtJ || _JtErr )
            {
                cvProjectPoints2( &_Mi, &_ri, &_ti, &matA, &_k, &_mp, &_dpdr, &_dpdt,
                                  (flags & CV_CALIB_FIX_FOCAL_LENGTH) ? 0 : &_dpdf,
                                  (flags & CV_CALIB_FIX_PRINCIPAL_POINT) ? 0 : &_dpdc, &_dpdk,
                                  (flags & CV_CALIB_FIX_ASPECT_RATIO) ? aspectRatio : 0);
            }
            else
                cvProjectPoints2( &_Mi, &_ri, &_ti, &matA, &_k, &_mp );

            cvSub( &_mp, &_mi, &_mp );

            if( _JtJ || _JtErr )
            {
                cvGetSubRect( _JtJ, &_part, cvRect(0,0,NINTRINSIC,NINTRINSIC) );
                cvGEMM( _Ji, _Ji, 1, &_part, 1, &_part, CV_GEMM_A_T );

                cvGetSubRect( _JtJ, &_part, cvRect(NINTRINSIC+i*6,NINTRINSIC+i*6,6,6) );
                cvGEMM( _Je, _Je, 1, 0, 0, &_part, CV_GEMM_A_T );

                cvGetSubRect( _JtJ, &_part, cvRect(NINTRINSIC+i*6,0,6,NINTRINSIC) );
                cvGEMM( _Ji, _Je, 1, 0, 0, &_part, CV_GEMM_A_T );

                cvGetRows( _JtErr, &_part, 0, NINTRINSIC );
                cvGEMM( _Ji, _err, 1, &_part, 1, &_part, CV_GEMM_A_T );

                cvGetRows( _JtErr, &_part, NINTRINSIC + i*6, NINTRINSIC + (i+1)*6 );
                cvGEMM( _Je, _err, 1, 0, 0, &_part, CV_GEMM_A_T );
            }

            double errNorm = cvNorm( &_mp, 0, CV_L2 );
            reprojErr += errNorm*errNorm;
        }
        if( _errNorm )
            *_errNorm = reprojErr;
    }

    // 4. store the results
    cvConvert( &matA, cameraMatrix );
    cvConvert( &_k, distCoeffs );

								for( i = 0; i < nimages; i++ )
								{
									CvMat src, dst;
									if( rvecs )
									{
										src = cvMat( 3, 1, CV_64F, solver.param->data.db + NINTRINSIC + i*6 );
										if( rvecs->rows == nimages && rvecs->cols*CV_MAT_CN(rvecs->type) == 9 )
										{
											dst = cvMat( 3, 3, CV_MAT_DEPTH(rvecs->type),
												rvecs->data.ptr + rvecs->step*i );
											cvRodrigues2( &src, &matA );
											cvConvert( &matA, &dst );
										}
										else
										{
											dst = cvMat( 3, 1, CV_MAT_DEPTH(rvecs->type), rvecs->rows == 1 ?
												rvecs->data.ptr + i*CV_ELEM_SIZE(rvecs->type) :
												rvecs->data.ptr + rvecs->step*i );
											cvConvert( &src, &dst );
										}
									}
									if( tvecs )
									{
										src = cvMat( 3, 1, CV_64F, solver.param->data.db + NINTRINSIC + i*6 + 3 );
										dst = cvMat( 3, 1, CV_MAT_TYPE(tvecs->type), tvecs->rows == 1 ?
												tvecs->data.ptr + i*CV_ELEM_SIZE(tvecs->type) :
												tvecs->data.ptr + tvecs->step*i );
										cvConvert( &src, &dst );
									 }
								}
    
    return reprojErr;
}



///////////////////////////////////////////


IplImage* winietowanie_kalibracja(char base[108], char do_korekcji_win[30] ,char rozsze[8],	char plik_maski[30])
{

	char plik_wej[128];
	char plik_do_poprawy[128];
	
	//wczytanie pliku maski
	sprintf(plik_wej, "%s%s%s", base, plik_maski, rozsze); 
	IplImage *obraz = cvLoadImage( plik_wej); 
	//wczytanie oryginalnego obrazu
	sprintf(plik_do_poprawy, "%s%s%s" , base, do_korekcji_win, rozsze);
	IplImage *obraz_do_popr= cvLoadImage(plik_do_poprawy);

	//zmiana rozmiaru maski
	IplImage* obraz2 = cvCreateImage( cvGetSize(obraz_do_popr), obraz_do_popr->depth, obraz_do_popr->nChannels );
	cvResize(obraz, obraz2);

	//wst�pna obr�bka maski
	IplImage *szary_obraz = cvCreateImage( cvGetSize( obraz2 ), 8, 1 ); 
	cvCvtColor( obraz2, szary_obraz, CV_BGR2GRAY );
		

	// wykonanie negatywu maski
	for(int i=0;i<szary_obraz->height;i++) for(int j=0;j<szary_obraz->width;j++) for(int k=0;k<szary_obraz->nChannels;k++)
	{
		szary_obraz->imageData[i*szary_obraz->widthStep+j*szary_obraz->nChannels+k]=255-szary_obraz->imageData[i*szary_obraz->widthStep+j*szary_obraz->nChannels+k];
		
	}

	// sumowanie obrazu i maski
	for(int i=0;i<obraz_do_popr->height;i++) for(int j=0;j<obraz_do_popr->width;j++) 
		{	
		CvScalar s;
		for(int k=0;k<obraz_do_popr->nChannels;k++)
			{
			CvScalar s1;
			CvScalar s2;
			s1=cvGet2D(obraz_do_popr,i,j); 
			s2=cvGet2D(szary_obraz,i,j); 
			s.val[k] =		s1.val[k]    +    s2.val[0];
			}
		cvSet2D(obraz_do_popr,i,j,s); 
		}

	return obraz_do_popr;

}


int wyswietl_zdjecie (IplImage* obraz,char nazwa_okna [20])
{
	
	cvNamedWindow ( nazwa_okna );
	cvShowImage ( nazwa_okna, obraz ); // wy�wietlenie obrazu
	cvWaitKey( 0 );
	cvDestroyWindow (nazwa_okna);
	return 1;
}

int kalibracja_dystorsji(char base[108],char rozsze[8],int n_boards)
{	
//	int n_boards = 0;
	const int board_dt = 20;
	int board_w;
	int board_h;
	char plik[120];
	board_w = 6; // liczba kwadrat�w w szachownicy w poziomie
	board_h = 9; // j.w. w pionie
	int board_n = board_w * board_h;
	CvSize board_sz = cvSize( board_w, board_h );
	//CvCapture* capture = cvCreateCameraCapture( 0 ); //opcje do przechwytywania obrazu z kamery.
	//assert( capture );

	
	// Tworzenie macierzy na dane.
	CvMat* image_points		= cvCreateMat( n_boards*board_n, 2, CV_32FC1 );
	CvMat* object_points		= cvCreateMat( n_boards*board_n, 3, CV_32FC1 );
	CvMat* point_counts			= cvCreateMat( n_boards, 1, CV_32SC1 );
	CvMat* intrinsic_matrix		= cvCreateMat( 3, 3, CV_32FC1 );
	CvMat* distortion_coeffs	= cvCreateMat( 5, 1, CV_32FC1 );

	CvPoint2D32f* corners = new CvPoint2D32f[ board_n ]; 
	int corner_count;
	int successes = 0;
	int step, frame = 0;
												
			//wczytanie obrazu planszy testowej
			sprintf(plik, "%s%d%s", base, 1, rozsze);  
	        IplImage *image = cvLoadImage( plik );
			IplImage *gray_image = cvCreateImage( cvGetSize( image ), 8, 1 ); 
			//IplImage *image_z_rogami =  cvCloneImage(image); 
			

	//cvNamedWindow( "Obraz wej�ciowy" );
	//cvShowImage( "Obraz wej�ciowy", image ); // wy�wietlenie oryginalnego obrazu
	//IplImage *image = cvQueryFrame( capture );  // opcja do obs�ugi kamery



	while( successes < n_boards ){
			

			sprintf(plik, "%s%d%s", base, successes+1, rozsze);  
	        IplImage *image = cvLoadImage( plik );
			IplImage *gray_image = cvCreateImage( cvGetSize( image ), 8, 1 ); 
		//if( frame++ % board_dt == 0 ){ //opcja alternatywna do obs�ugi kamery
			//znajdywanie rog�w kwadrat�w w obrazie
			int found = cvFindChessboardCorners( image, board_sz, corners,
				&corner_count, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS );

		
			cvCvtColor( image, gray_image, CV_BGR2GRAY );
			cvFindCornerSubPix( gray_image, corners, corner_count, cvSize( 11, 11 ), 
				cvSize( -1, -1 ), cvTermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));

			// rysowanie rog�w kwadrat�w na obrazie

			cvDrawChessboardCorners( image, board_sz, corners, corner_count, found );
			//cvShowImage( "Obraz wej�ciowy", image );
			printf ("%s", ".");
						

			// Zapisywanie wsp�rz�dnych punkt�w
			if( corner_count == board_n ){
				step = successes*board_n;
				for( int i=step, j=0; j < board_n; ++i, ++j ){
					CV_MAT_ELEM( *image_points, float, i, 0 ) = corners[j].x;
					CV_MAT_ELEM( *image_points, float, i, 1 ) = corners[j].y;
					CV_MAT_ELEM( *object_points, float, i, 0 ) = j/board_w;
					CV_MAT_ELEM( *object_points, float, i, 1 ) = j%board_w;
					CV_MAT_ELEM( *object_points, float, i, 2 ) = 0.0f;
				}
				CV_MAT_ELEM( *point_counts, int, successes, 0 ) = board_n;
				successes++;


			}
		//} 

		// Obs�uga pauza i ESC, obecnie nie u�ywana.

		//int c = cvWaitKey( 15 );
		
		//if( c == 'p' ){
		//	c = 0;
		//	while( c != 'p' && c != 27 ){
		//		c = cvWaitKey( 250 );
		//	}
		//}
		//if( c == 27 )
		//	return 0;
		//wczytywanie kolejnych obraz�w

	} 

	// Przechowywanie danych
	CvMat* object_points2 = cvCreateMat( successes*board_n, 3, CV_32FC1 );
	CvMat* image_points2 = cvCreateMat( successes*board_n, 2, CV_32FC1 );
	CvMat* point_counts2 = cvCreateMat( successes, 1, CV_32SC1 );
	
	// przepisywanie danych do prawid�owych macierzy
	for( int i = 0; i < successes*board_n; ++i ){
		CV_MAT_ELEM( *image_points2, float, i, 0) = CV_MAT_ELEM( *image_points, float, i, 0 );
		CV_MAT_ELEM( *image_points2, float, i, 1) = CV_MAT_ELEM( *image_points, float, i, 1 );
		CV_MAT_ELEM( *object_points2, float, i, 0) = CV_MAT_ELEM( *object_points, float, i, 0 );
		CV_MAT_ELEM( *object_points2, float, i, 1) = CV_MAT_ELEM( *object_points, float, i, 1 );
		CV_MAT_ELEM( *object_points2, float, i, 2) = CV_MAT_ELEM( *object_points, float, i, 2 );
	}

	for( int i=0; i < successes; ++i ){
		CV_MAT_ELEM( *point_counts2, int, i, 0 ) = CV_MAT_ELEM( *point_counts, int, i, 0 );
	}
	cvReleaseMat( &object_points );
	cvReleaseMat( &image_points );
	cvReleaseMat( &point_counts );

	// Zako�czono zbieranie danych.
	// Initiliazie the intrinsic matrix such that the two focal lengths
	// have a ratio of 1.0

	CV_MAT_ELEM( *intrinsic_matrix, float, 0, 0 ) = 1.0;
	CV_MAT_ELEM( *intrinsic_matrix, float, 1, 1 ) = 1.0;

	// Kalibracja aparatu

	CvTermCriteria termCrit;

		termCrit.type=0;

		termCrit.max_iter=5;
		termCrit.epsilon=12;

	kalib( object_points2, image_points2, point_counts2, cvGetSize( image ), 
	intrinsic_matrix, distortion_coeffs, NULL, NULL, CV_CALIB_FIX_ASPECT_RATIO); 

	// Zapisanie parametr�w do pliku.
	char miejsce_zapisu[120];
	char miejsce_zapisu2[120];
	sprintf(miejsce_zapisu, "%s%d%s", base,n_boards,"Parametry_dystorsji.xml"); 
	sprintf(miejsce_zapisu2, "%s%d%s", base,n_boards,"Parametry_dystorsji2.xml");
	cvSave( "Parametry_dystorsji2.xml", intrinsic_matrix );
	cvSave( miejsce_zapisu, distortion_coeffs );

	return 1;

	
}


IplImage* usuwanie_dystorsji(char plik[128],char base [108],char do_korekcji_wewn[30],char rozsze[8],char plik_parametry[30],int tryb_pracy)
{

	char plik_wej[128];
	sprintf(plik_wej, "%s%s%s", base,do_korekcji_wewn, rozsze); 
	IplImage *image = cvLoadImage( plik_wej); //wczytanie oryginalnego obrazu
	char miejsce_zapisu[120];
	sprintf(miejsce_zapisu, "%s%s", base,plik_parametry); 

	CvMat *intrinsic = (CvMat*)cvLoad( "Parametry_dystorsji2.xml" );
	CvMat *distortion = (CvMat*)cvLoad( miejsce_zapisu );

	IplImage* mapx = cvCreateImage( cvGetSize( image ), IPL_DEPTH_32F, 1 );
	IplImage* mapy = cvCreateImage( cvGetSize( image ), IPL_DEPTH_32F, 1 );
	
	cvInitUndistortMap( intrinsic, distortion, mapx, mapy );
	
		IplImage *t = cvCloneImage( image );

		cvRemap( t, image, mapx, mapy ); // undistort image
		cvReleaseImage( &t );


		// Handle pause/unpause and esc
		int c = cvWaitKey( 15 );
		if( c == 'p' ){
			c = 0;
			while( c != 'p' && c != 27 ){
				c = cvWaitKey( 250 );
			}
		}
		if( c == 27 )
			return 0;
		//image = cvQueryFrame( capture );
	//}
		return image;
}


int main(int argc, CHAR *argv[])
{
	// zmienne pomocnicza
	char plik[116], base[108], rozsze[8],plikwyj[128], popraw[12],do_korekcji[30];
	char plik_parametry[30];
	char plik_maski[30];
	IplImage *obraz_wyjsciowy;
	char katalog [108];
	int l_obrazow =1;
	int l_plansz=1;
	int tryb_pracy=3;
	IplImage *winieta_skorygowany;
	char winieta_sciezka[128];
	sprintf (popraw, "poprawiony");
	int prawidlowo[20];
	//inicjalizacja danych
	for (int u=0;u<20;u++)
	{
		prawidlowo[u]=0;	
	}


	//int do_korekcjiint=1;
	// wczytywanie strumienia wej�ciowego
	// dost�pne strumienie : 
	//-h Pomoc
	//-t Tryb pracy  1
	//-s �cie�ka dost�pu do katalogu z danymi  2
	//-r rozszerzenie plik�w  3
	//-l liczba plansz testowych  4
	//-n nazwa pliku, kt�ry ma by� poddany korekcji  5
	//-p nazwa pliku zawieraj�cego parametry korekcji dystorsji  6
	//-m nazwa pliku zawieraj�cego mask� do korekcji winietowania  7

	if (argc==1) 
		prawidlowo[0]=0;

	int in_i=1;
	printf ("\n\n%s\n", "\t\t\Witaj w programie korekcji dystorsji i winietowania\t\t");
	printf ("\n%s\n\n\n", "\t\t\t\t\tKoDiW");
	while ((in_i<argc)&&(argv[in_i][0]=='-'))
	{
		string in_data=argv[in_i];
		if(in_data=="-h"||in_data=="-help"||in_data=="-HELP")
		{
			int odp=pomoc();
			if (odp==0) return 0;
		}
		else if (in_data=="-t")
		{
			in_i++;
			tryb_pracy=atoi(argv[in_i]);
			if (tryb_pracy==0) prawidlowo[1]=0;
			else			   prawidlowo[1]=1;
		}
		else if (in_data=="-s")
		{
			in_i++;
			strcpy(base,argv[in_i]);
			prawidlowo[2]=1;
		}
		else if(in_data=="-r")
		{
			in_i++;
			strcpy(rozsze,argv[in_i]);
			prawidlowo[3]=1;
		}
		else if(in_data=="-l")
		{
			in_i++;
			l_plansz=atoi(argv[in_i]);
			if (l_plansz==0) prawidlowo[4]=0;
			else             prawidlowo[4]=1;
		}
		else if(in_data=="-n")
		{
			in_i++;
			strcpy(do_korekcji,argv[in_i]);		
			prawidlowo[5]=1;
		}
		else if(in_data=="-p")
		{
			in_i++;
			strcpy(plik_parametry,argv[in_i]);
			prawidlowo[6]=1;
		}
		else if(in_data=="-m")
		{
			in_i++;
			strcpy(plik_maski,argv[in_i]);
			prawidlowo[7]=1;
		}
		else if(in_data=="-i"||in_data=="-info"||in_data=="-INFO")
		{
			int odp=info();
			if (odp==0) return 0;
		}
		in_i++;
	}

	if(prawidlowo[0]==1) cout<<"\nWprowadzone parametry uruchomieniowe programu:";
	if(prawidlowo[1]==1) cout<<"\nTryb pracy:\t"<<tryb_pracy;
	if(prawidlowo[2]==1) cout<<"\nScieszka dostepu:\t"<<base;
	if(prawidlowo[3]==1) cout<<"\nRozrzezenie:\t"<<rozsze;
	if((prawidlowo[4]==1)&&(tryb_pracy==1||tryb_pracy==3)) cout<<"\nLiczba plansz testowych:\t"<<l_plansz;
	if((prawidlowo[5]==1)&&(tryb_pracy==2||tryb_pracy==3||tryb_pracy==4))cout<<"\nNazwa pliku ktory ma byc poddany korekcji:\t"<<do_korekcji;
	if((prawidlowo[6]==1)&&(tryb_pracy==2)) cout<<"\nNazwa pliku z parametrami dystorsji:\t"<<plik_parametry;
	if((prawidlowo[7]==1)&&(tryb_pracy==4)) cout<<"\nNazwa pliku zawierajacego maske do korekcji winietowania:\t"<<plik_maski;

	//cout<<"\nPodaj nastepujace dane:\n";
	if(prawidlowo[1]==0) 
	{
		printf ("\n%s\n", "Podaj tryb pracy\n 1: Dystorsja - kalibracja toru optycznego \n 2: Dystorsja - korekcja zdjecia \n 3: Dystorsja - kalibracja toru optycznego z jednoczesna korekcja\t \n 4: Korekcja winietowania\n ");
		scanf("%d", & tryb_pracy);
	}
	if(prawidlowo[2]==0) 
	{
		printf ("\n%s\n", "Podaj sciezke dostepu do katalogu z danymi\n");
		scanf("%s", & base);		
	}
	if(prawidlowo[3]==0) 
	{
		printf ("\n%s\n", "Wpisz rozszerzenie plikow\t");
		scanf("%s", & rozsze);	
	}
	if(prawidlowo[4]==0)
	{
		if (tryb_pracy==1||tryb_pracy==3)
		{
			printf ("\n%s\n", "Podaj liczbe plansz testowych\t");
			scanf("%d", & l_plansz);
		}		
	}
	if(prawidlowo[5]==0)
	{
		if (tryb_pracy==2||tryb_pracy==3||tryb_pracy==4)
		{
			printf ("\n%s\n", "Podaj nazwe pliku ktory ma byc poddany korekcji\t");
			scanf("%s", & do_korekcji);
		}	
	}
	if(prawidlowo[6]==0)
	{
		if(tryb_pracy==2) 
		{
				printf ("\n%s\n", "Podaj nazwe pliku zawierajacego parametry korekcyjne\t");
				scanf("%s", & plik_parametry);	
		}
	}
	if((prawidlowo[7]==0)&&(tryb_pracy==4))
	{
		printf ("\n%s\n", "Podaj nazwe pliku zawierajacego maske do korekty winietowania");
		scanf("%s", & plik_maski);
	}
	//

	
	//if (strcmp(base,"1") == 0) 		sprintf(base, "%s", "C:\\b\\"); 
	//sprintf(rozsze, ".jpg");
	

	if (tryb_pracy==4)  //korekta winietowania
	{
		winieta_skorygowany=winietowanie_kalibracja(base,do_korekcji, rozsze,plik_maski);
		sprintf(winieta_sciezka, "%s%s%s%s",base,do_korekcji,"poprawiony",rozsze);
		cvSaveImage(winieta_sciezka, winieta_skorygowany);
		wyswietl_zdjecie (winieta_skorygowany,"Obraz po poprawie");
	}
			
	if (tryb_pracy==1||tryb_pracy==2||tryb_pracy==3)
	{
			if (tryb_pracy==1||tryb_pracy==3)
			{
			//for (l_obrazow=1;l_obrazow<l_plansz;l_obrazow++)
			//{
				//sprintf(plik, "%s%d%s", base,numer_zdjecia, rozsze); 
				sprintf(plikwyj, "%s%d%s%s", base,l_obrazow, popraw,rozsze);
				kalibracja_dystorsji(base,rozsze,l_plansz); //uruchamiamy funkcje wykrywania dystorsji
				//cvSaveImage(plikwyj, obraz_wyjsciowy);
		//	}
			}

				
				//cvNamedWindow( "Obraz wyj�ciowy" );
				//cvShowImage( "Obraz wyj�ciowy", obraz_wyjsciowy ); // Poka� skorygowany obraz
		if (tryb_pracy==2||tryb_pracy==3)	
		{
	
			if(tryb_pracy==2) 
			{
				obraz_wyjsciowy=usuwanie_dystorsji(plikwyj,base,do_korekcji,rozsze,plik_parametry,tryb_pracy);
				//zapisywanie skorygowanego obrazu do pliku
			}
			else 
			{

				sprintf(plik_parametry, "%d%s",l_plansz,"Parametry_dystorsji.xml");
				obraz_wyjsciowy=usuwanie_dystorsji(plikwyj,base,do_korekcji,rozsze,plik_parametry,tryb_pracy);

			}	
			int czy_ok = wyswietl_zdjecie (obraz_wyjsciowy,"obraz po korekcji");
			sprintf(plikwyj, "%s%s%s%s", base,do_korekcji, popraw,rozsze);
			cvSaveImage(plikwyj, obraz_wyjsciowy);
			if (czy_ok ==1) 				printf ("\n%s\n", "Dzialanie programu zakonczylo sie poprawnie");
		}
		
	}		
	//cvReleaseImage (&obraz_wyjsciowy);
//	cvReleaseImage (&winieta_skorygowany);
	cvWaitKey(0);
	return 0;
}

