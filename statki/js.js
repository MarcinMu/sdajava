function sprawdzCzyMoznaWstawic(plansza, x, y){
    if( plansza[x][y] !== 1 && sprawdzCzyWokolJestPusto(plansza, x, y)){
        return true;   
    } else{
        return false;  
    }    
}

function sprawdzCzyWokolJestPusto(plansza, x, y){
    if ((typeof(plansza[x-1]) === 'undefined' || (plansza[x-1][y] !== 1 && plansza[x-1][y+1] !== 1 && plansza[x-1][y-1] !== 1))
        && plansza[x][y-1] !== 1 && plansza[x][y-1] !== 1 && plansza[x][y+1] !== 1 && 
        (typeof(plansza[x+1]) === 'undefined' || (plansza[x+1][y] !== 1 && plansza[x+1][y+1] !== 1 && plansza[x+1][y-1] !== 1))){
        return true;
    } else {
        return false;
    } 
}

function czyObokPlywaMaszt (plansza, x, y){
    if (sprawdzCzyWokolJestPusto (plansza, x, y)){
        return true;
    }
    var i = 0;
    var j = -1;
    var maszty = [0];
    var kolejnosc = [[-1, 0], [1, 0], [0, 1], [0, -1]];
    
    while ( i < kolejnosc.length){
        if (typeof(plansza[x+i]) !== 'undefined'){
            while ( j < 2 ) {
                if ((typeof(plansza[x+i][y+j]) !== 'undefined' ) && (i !== 0 && j !== 0) && (i !== 0 && j !== 0)){
                {if (plansza[x+i][y+j] == 2 ){
                        if (czyObokPlywaMaszt(plansza, x+i, y+j)){
                            return true;
                        }
                        
                    }
                }
                j++;
            }    
            }
        }
        i++;
    }
    return false;
}

function policzCzteromasztowce(plansza){
    var liczbaMasztow = 0;
    for (var i = 0; i<10; i++){
        for (var j = 0; j<10; j++){
            liczbaMasztow+=plansza[i][j];
        }
    }
    if (liczbaMasztow!=4){alert("Mamy problem, stocznia nie wyprodukowała 4 masztowca");}
}

function ustawDuzoStatkow(liczbaStatkow, rozmiarPlanszyX, rozmiarPlanszyY) {
    var plansza = [];
    var zaMaloMiejscaNaTeStatki = 0;
    var wylosowanaPozycjaX = 0;
    var wylosowanaPozycjaY = 0;
    var liczbaTrojek = 2;
    var liczbaDwojek = 3;
    var KaczkaAwaryna = 0; 
    for (var i = 0; i < rozmiarPlanszyX; i++){
        plansza[i] = [];
        for(var j = 0; j < rozmiarPlanszyY; j++){
            plansza[i][j] = 0;
        }
    }
    if (rozmiarPlanszyX > 4 && rozmiarPlanszyY > 4){
        if (Math.random()>0.5){
            wylosowanaPozycjaX = Math.floor(Math.random()*(rozmiarPlanszyX-3));
            wylosowanaPozycjaY = Math.floor(Math.random()*rozmiarPlanszyY);
            plansza[wylosowanaPozycjaX][wylosowanaPozycjaY] = 1;    
            plansza[wylosowanaPozycjaX+1][wylosowanaPozycjaY] = 1;    
            plansza[wylosowanaPozycjaX+2][wylosowanaPozycjaY] = 1;    
            plansza[wylosowanaPozycjaX+3][wylosowanaPozycjaY] = 1; 
        } else {
            wylosowanaPozycjaX = Math.floor(Math.random()*(rozmiarPlanszyX));
            wylosowanaPozycjaY = Math.floor(Math.random()*(rozmiarPlanszyY-3)); 
            plansza[wylosowanaPozycjaX][wylosowanaPozycjaY] = 1;
            plansza[wylosowanaPozycjaX][wylosowanaPozycjaY+1] = 1;
            plansza[wylosowanaPozycjaX][wylosowanaPozycjaY+2] = 1;
            plansza[wylosowanaPozycjaX][wylosowanaPozycjaY+3] = 1;
        }
        liczbaStatkow--;        
    }
    if (rozmiarPlanszyX > 3 && rozmiarPlanszyY > 3){
        while (liczbaTrojek > 0 && liczbaStatkow > 0){
            if (Math.random()>0.5){
                wylosowanaPozycjaX = Math.floor(Math.random()*(rozmiarPlanszyX-2));
                wylosowanaPozycjaY = Math.floor(Math.random()*rozmiarPlanszyY);
                if (sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX, wylosowanaPozycjaY) &&
                    sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX+1, wylosowanaPozycjaY) &&
                    sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX+2, wylosowanaPozycjaY)) {
                        plansza[wylosowanaPozycjaX][wylosowanaPozycjaY] = 1;    
                        plansza[wylosowanaPozycjaX+1][wylosowanaPozycjaY] = 1;    
                        plansza[wylosowanaPozycjaX+2][wylosowanaPozycjaY] = 1;
                        liczbaStatkow--;
                        liczbaTrojek--;
                        zaMaloMiejscaNaTeStatki = 0;
                }
            } else {
                wylosowanaPozycjaX = Math.floor(Math.random()*(rozmiarPlanszyX));
                wylosowanaPozycjaY = Math.floor(Math.random()*(rozmiarPlanszyY-2)); 
                if (sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX, wylosowanaPozycjaY) &&
                    sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX, wylosowanaPozycjaY+1) &&
                    sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX, wylosowanaPozycjaY+2)) {
                        plansza[wylosowanaPozycjaX][wylosowanaPozycjaY] = 1;    
                        plansza[wylosowanaPozycjaX][wylosowanaPozycjaY+1] = 1;    
                        plansza[wylosowanaPozycjaX][wylosowanaPozycjaY+2] = 1;
                        liczbaStatkow--;
                        liczbaTrojek--;
                        zaMaloMiejscaNaTeStatki = 0;
                }        
            }
            zaMaloMiejscaNaTeStatki++; 
            if (zaMaloMiejscaNaTeStatki>200){
                document.getElementById("informacje").innerHTML = "</BR>Nie upchne tutaj tych wszystkich statkow</BR>";
                return false;
            }
        }   
    }
    if (rozmiarPlanszyX > 2 && rozmiarPlanszyY > 2){
        while (liczbaDwojek > 0 && liczbaStatkow > 0){
            if (Math.random()>0.5){
                wylosowanaPozycjaX = Math.floor(Math.random()*(rozmiarPlanszyX-1));
                wylosowanaPozycjaY = Math.floor(Math.random()*rozmiarPlanszyY);
                if (sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX, wylosowanaPozycjaY) &&
                    sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX+1, wylosowanaPozycjaY)) {
                        plansza[wylosowanaPozycjaX][wylosowanaPozycjaY] = 1;    
                        plansza[wylosowanaPozycjaX+1][wylosowanaPozycjaY] = 1;    
                        liczbaStatkow--;
                        liczbaDwojek--;
                        zaMaloMiejscaNaTeStatki = 0;
                }
            } else {
                wylosowanaPozycjaX = Math.floor(Math.random()*(rozmiarPlanszyX));
                wylosowanaPozycjaY = Math.floor(Math.random()*rozmiarPlanszyY-1); 
                if (sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX, wylosowanaPozycjaY) &&
                    sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX, wylosowanaPozycjaY+1)) {
                        plansza[wylosowanaPozycjaX][wylosowanaPozycjaY] = 1;    
                        plansza[wylosowanaPozycjaX][wylosowanaPozycjaY+1] = 1;    
                        liczbaStatkow--;
                        liczbaDwojek--;
                        zaMaloMiejscaNaTeStatki = 0;
                }        
            }
            zaMaloMiejscaNaTeStatki++; 
            if (zaMaloMiejscaNaTeStatki > 200){
                document.getElementById("informacje").innerHTML = "</BR>Nie upchne tutaj tych wszystkich statkow.</BR>";
                return false;
            }
        }   
    }
    while (liczbaStatkow > 0 ) {
        var wylosowanaPozycjaX = Math.floor(Math.random()*rozmiarPlanszyX);
        var wylosowanaPozycjaY = Math.floor(Math.random()*rozmiarPlanszyY);
        if (sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX, wylosowanaPozycjaY)) {            
            plansza[wylosowanaPozycjaX][wylosowanaPozycjaY]  = 1;
            liczbaStatkow--;
            zaMaloMiejscaNaTeStatki = 0;
        } else{
            zaMaloMiejscaNaTeStatki++;  
            if (zaMaloMiejscaNaTeStatki>200){
                document.getElementById("informacje").innerHTML = "</BR>Nie upchne tutaj tych wszystkich statkow.</BR>";
                return false;
            }
        }
    }
    return plansza;
}

function zatopiony(plansza, x, y){
    //var czyObokPlywaMaszt = 
    return sprawdzCzyWokolJestPusto(plansza, x, y);
}

function tlumaczNaLitery(index){
    var alfabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'];
    return alfabet[index];
}

function wyswietlPlansze(plansza, pokazStatki, placeToDisplay){
    var rozmiarPlanszyX = plansza.length;
    var rozmiarPlanszyY = plansza [0].length;  
    var textPlanszy = " &nbsp;&nbsp; | ";
    for(var z = 0; z <rozmiarPlanszyY; z++){
        textPlanszy += " " + tlumaczNaLitery(z) + " | "; 
    }
    textPlanszy += "</BR>";
    for(var g = 0; g<rozmiarPlanszyX; g++){
        var g1 = g +1;
        if (g < 9) {
            textPlanszy += "&nbsp;" + g1 + " | " ;
        }else {
            textPlanszy +=  g1 + " | " ;
        }
        for(var h = 0; h<rozmiarPlanszyY; h++){
            if (plansza[g][h] == 1 && pokazStatki == true){
                textPlanszy += "  S  | ";
            } else if (plansza[g][h] == 0 || plansza[g][h] == 1 ) {
                textPlanszy += "  -  | ";
            } else if (plansza[g][h] == 2) {
                textPlanszy += "  Z  | ";
            } else if(plansza[g][h] == 3) {
                textPlanszy += "  X  | ";
            }else {
                return false;
            }         
        }
        textPlanszy += "</BR>";
    }
    document.getElementById(placeToDisplay).innerHTML = textPlanszy;
    return true;
}

function takeCoord(rozmiarPlanszyX, rozmiarPlanszyY){
    var translate = [];
    translate["A"] = 0;
    translate["B"] = 1;
    translate["C"] = 2;
    translate["D"] = 3;
    translate["E"] = 4;
    translate["F"] = 5;
    translate["G"] = 6;
    translate["H"] = 7;
    translate["I"] = 8;
    translate["J"] = 9;
    var y = 0; 
    do {
        var coordinates = prompt("Podaj współrzędne strzału");
        if (coordinates =='god' || coordinates =='exit'){
           return coordinates;
        }
        var coordinatesTable = coordinates.split("");
        y = coordinatesTable[0].toUpperCase();
        x = coordinatesTable[1] -1 ;
        if (coordinatesTable.length>2){
           coordinatesTable[1] = parseInt(coordinatesTable[1] + coordinatesTable[2]);
           x = coordinatesTable[1] - 1;
        }
    } while (typeof(translate[y]) === 'undefined' || translate[y] >= rozmiarPlanszyY || x < 0 || x >= rozmiarPlanszyX || isNaN(x))
    document.getElementById("informacje").innerHTML ="</BR> Strzelasz: " + y + coordinatesTable[1];
    coordinatesTable[2] = y;
    coordinatesTable[0] = translate[y];
    coordinatesTable[3] = coordinatesTable[1];
    coordinatesTable[1] = x; 
    return coordinatesTable;
}

function game(planszaStatki, planszaGracza, liczbaMasztow){
    var cheater = false;
    var history = [];
    var rozmiarPlanszyX = planszaStatki.length;
    var rozmiarPlanszyY = planszaStatki[0].length;
    var liczbaMasztowGracza = liczbaMasztow;
    while(liczbaMasztow > 0 && liczbaMasztowGracza > 0){
        var powtorzStrzal = true;
        var powtorzStrzalKomputera = true;      
        while(powtorzStrzal){
            powtorzStrzal = false;
            var strzal = takeCoord(rozmiarPlanszyX, rozmiarPlanszyY);
            if (strzal == 'god'){
                cheater = true;
                powtorzStrzal = true;
                wyswietlPlansze(planszaStatki, cheater, "plansza");
            } else if (strzal == 'exit'){
                return history + "</BR>Poddałeś się </BR> :(" ; 
            } else {
                var strzalY = strzal[0];
                var strzalX = strzal[1]; 
                if ( planszaStatki [strzalX][strzalY] == 0 ){
                    document.getElementById("informacje2").innerHTML = "    Pudło</BR>";
                    planszaStatki[strzalX][strzalY] = 3;
                    history.push([strzal[2], strzal[3], "Pudło"]);
                } else if ( planszaStatki [strzalX][strzalY] == 2 || planszaStatki [strzalX][strzalY] == 3 ) {
                    document.getElementById("informacje2").innerHTML = "     Już tu strzelałeś mośku. Strzelaj jeszcze raz</BR>";
                    history.push([strzal[2], strzal[3], "Dubel"]);
                    powtorzStrzal = true;
                } else if ( planszaStatki [strzalX][strzalY] == 1 && !zatopiony( planszaStatki, strzalX, strzalY )){
                    planszaStatki[strzalX][strzalY] = 2;
                    document.getElementById("informacje2").innerHTML = "    Trafiony</BR>";
                    liczbaMasztow--;
                    powtorzStrzal = true;
                    history.push([strzal[2], strzal[3], "Trafiony"]);
                } else if ( planszaStatki [strzalX][strzalY] == 1 && zatopiony( planszaStatki, strzalX, strzalY )){
                    planszaStatki[strzalX][strzalY] = 2;
                    document.getElementById("informacje2").innerHTML = "    Trafiony Zatopiony</BR>";
                    liczbaMasztow--;
                    history.push([strzal[2], strzal[3], "Trafiony Zatopiony"]);
                }
                if (liczbaMasztow == 0 ){
                    wyswietlPlansze(planszaStatki, cheater, "plansza");
                    alert ("Yupii Wygrałeś !!!")
                    return history + "  Yupii wygrałeś !!!";
                }
            } 
            wyswietlPlansze(planszaStatki, cheater, "plansza");
        }
        while (powtorzStrzalKomputera){
            powtorzStrzalKomputera = false;
            var siStrzalY = Math.floor(Math.random()*(rozmiarPlanszyY));
            var siStrzalX = Math.floor(Math.random()*(rozmiarPlanszyX)); 
            document.getElementById("informacjesi").innerHTML ="</BR> Komputer strzela w: " + tlumaczNaLitery(siStrzalY) + parseInt(siStrzalX + 1);
            if ( planszaGracza [siStrzalX][siStrzalY] == 0 ){
                document.getElementById("informacje2si").innerHTML = "    Pudło</BR>";
                planszaGracza[siStrzalX][siStrzalY] = 3;
                //history.push([siStrzalX, siStrzalY, "Pudło"]);
            } else if ( planszaGracza [siStrzalX][siStrzalY] == 2 || planszaGracza [siStrzalX][siStrzalY] == 3 ) {
                document.getElementById("informacje2si").innerHTML = "     Już tu strzelałeś mośku. Strzelaj jeszcze raz</BR>";
                //history.push([siStrzalX, siStrzalY, "Dubel"]);
                powtorzStrzalKomputera = true;
            } else if ( planszaGracza [siStrzalX][siStrzalY] == 1 && !zatopiony( planszaGracza, siStrzalX, siStrzalY )){
                planszaGracza[siStrzalX][siStrzalY] = 2;
                document.getElementById("informacje2si").innerHTML = "    Trafiony</BR>";
                liczbaMasztowGracza--;
                powtorzStrzalKomputera = true;
                //history.push([siStrzalX, siStrzalY, "Trafiony"]);
            } else if ( planszaGracza [siStrzalX][siStrzalY] == 1 && zatopiony( planszaGracza, siStrzalX, siStrzalY )){
                planszaGracza[siStrzalX][siStrzalY] = 2;
                document.getElementById("informacje2si").innerHTML = "    Trafiony Zatopiony</BR>";
                liczbaMasztowGracza--;
                //history.push([siStrzalX, siStrzalY, "Trafiony Zatopiony"]);
            }
            if (liczbaMasztowGracza == 0 ){
                wyswietlPlansze(planszaGracza, true, "plansza2");
                alert ("Przegrałeś :(  !!!")
                return history + "  Przegrałeś :(  !!!";
            }
            wyswietlPlansze(planszaGracza, true, "plansza2");
        } 
    }
    return history;
}
function policzCzas(tartTime, endTime){
    var gameTime = endTime - tartTime;
    var sec = parseInt(gameTime/1000);
    var min = parseInt(sec/60);
    sec = sec % 60;
    sec = sec< 10 ? "0" + sec : sec; 
    var hour = parseInt(min/60);
    min = min % 60; 
    return "</BR>Czas gry: </BR> Godzin: " + hour + "  Minut: " + min + "  Sekund: " + sec;
}

function coverage(dim, shots){
    return Math.round((shots/Math.pow(dim, 2)* 100)*100)/100;
}

function policzMaszty(liczbaStatkow){
    if (liczbaStatkow == 1){
        return 4;
    } 
    if (liczbaStatkow < 4){
        return (4 + (liczbaStatkow - 1) * 3);
    }  
    if (liczbaStatkow < 7){
        return (10 + (liczbaStatkow - 3) * 2);
    }
    return (16 + (liczbaStatkow - 6 ));   
}

function ustalPoziomTrudnosci(){
    var poziomTrudnosci = prompt("Na jakim poziomie trudnosci ma grac komputer (od 1 do 5) ?");
    while (isNaN(poziomTrudnosci) || poziomTrudnosci > 5 || poziomTrudnosci < 0 ){
        var poziomTrudnosci = prompt("Podałeś niezrozumiałą wartość. Na jakim poziomie trudnosci ma grac komputer (od 1 do 5) ?");
        if (poziomTrudnosci == 'exit'){
            return 1;
        }
    }
}

function statki() {
    var rozmiarPlanszyX = 10;
    var rozmiarPlanszyY = 10;
    var liczbaStatkow = 10;
    var tekstKoncowy = '';
    var startTime = new Date();
    var liczbaMasztow = policzMaszty(liczbaStatkow);
    var poziomTrudnosci = ustalPoziomTrudnosci();
    var planszaStatki = ustawDuzoStatkow(liczbaStatkow , rozmiarPlanszyX, rozmiarPlanszyY);
    var planszaGracza = ustawDuzoStatkow(liczbaStatkow , rozmiarPlanszyX, rozmiarPlanszyY);
    wyswietlPlansze(planszaStatki, false, "plansza");
    wyswietlPlansze(planszaGracza, true, "plansza2");
    var wynik = game(planszaStatki, planszaGracza, liczbaMasztow);
    var endTime = new Date();
    if (wynik [wynik.length-1] !== "(") {
        tekstKoncowy += "</BR>Gratuluje wygranej gry!</BR>";
    }
    tekstKoncowy += "Przebieg gry był następujący:</BR>" + wynik + "</BR>" + "Pokrycie srzałów: "
         + coverage( rozmiarPlanszyX, wynik.length) +"%" + policzCzas(startTime, endTime);
    document.getElementById("endinfo").innerHTML = tekstKoncowy;
    wyswietlPlansze(planszaStatki, true, "plansza");
    wyswietlPlansze(planszaGracza, true, "plansza2");
}
statki();
