package Matrix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;

public class ReadExchangeRate {
	private String file;
	private HashMap<String, Double> exchangeRates ;

	public final HashMap<String, Double> getExchangeRates() {
		return exchangeRates;
	}
	
	public ReadExchangeRate(String file) {
		super();
		this.file = file;
		this.exchangeRates = new HashMap<String, Double>();
		this.getFileContent();
	}
	
	private void getFileContent() {
		try {
			BufferedReader bufferredReader = new BufferedReader(new FileReader(this.file));			
			String line = "";
			line = bufferredReader.readLine();
			while(line != null) {
				String [] ratesTable = line.split("\t");
				this.exchangeRates.put(ratesTable[0], Double.parseDouble(ratesTable[1].replace(",", ".")));
				line = bufferredReader.readLine();
			}
			this.exchangeRates.put("PLN", 1.0);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} 

		
	}
	
}
