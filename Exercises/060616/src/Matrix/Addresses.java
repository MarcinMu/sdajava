package Matrix;

public class Addresses {
	private String streetName;
	private int lastBlockNumber;
	
	public Addresses(String streetName, int lastNumber) {
		this.streetName = streetName;
		this.lastBlockNumber = lastNumber;
		this.printAddresses();
	}
	
	private void printAddresses () {
		String [] staircase = {"A", "B" };
		for (int i = 1; i <= this.lastBlockNumber; i+=2) {
			for (int j = 0; j < 2; j++) {
				for(int k = 1; k <= 6; k++ ) {
					System.out.print(this.streetName + " ");
					System.out.print(i);
					System.out.print(" / "+ staircase[j]);
					System.out.println(" / "+ k);
				}
			}
		}
	}

}
