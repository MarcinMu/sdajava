package Matrix;

public class Matrix {
	
	private int [][] myMatrix; 
	private int rows;
	private int columns;

	public Matrix(int rows, int columns) {
		super();
		this.rows = rows;
		this.columns = columns;
		this.myMatrix = new int [rows][columns];
		this.createMatrix();
		this.drawMatrix();
	}

	
	public void createMatrix() {
		for(int i = 0; i < this.rows; i++) {
			for(int j = 0; j < this.columns; j++) {
				this.myMatrix[i][j] = (int) (Math.random() *2);
			}
		}
	}
	public void drawMatrix() {
		for(int i = 0; i < this.rows; i++) {
			for(int j = 0; j < this.columns; j++) {
				System.out.print(this.myMatrix[i][j] + " ");
			}
			System.out.println("");
		}
	}

}
