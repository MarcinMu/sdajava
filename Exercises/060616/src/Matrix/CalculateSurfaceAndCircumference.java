package Matrix;

public class CalculateSurfaceAndCircumference {
	private double a;
	private double b;
	private double c;
	
	public CalculateSurfaceAndCircumference(double a, double b) {
		this.a = a;
		this.b = b;
		this.c = 0;
		this.calculateSurface();
		this.calculateCircuit();
	}
	
	public CalculateSurfaceAndCircumference(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.calculateSurface();
		this.calculateCircuit();
	}


	private void calculateCircuit() {
		if (this.c == 0) {
			System.out.println("Circumference of rectangle is: " + (2 * (this.a + this.b))); 
		} else {
			System.out.println("Circumference of triangle is: " + (this.a + this.b + this.c));
		}
	}


	private void calculateSurface() {
		if (this.c == 0) {
			System.out.println("Surface of rectangle is: " + this.a * this.b); 
		} else {
			double p = 0.5 * (this.a + this.b + this.c);
			double area = Math.sqrt(p * (p - this.a) * (p - this.b) * (p - this.c));
			System.out.println("Surface of triangle is: "+ area); 
		}
	}
	
	
	
}
