package Matrix;

public class PlayWithStrings {

	private String a;
	private String b;

	public PlayWithStrings(String a, String b) {
		this.a = a;
		this.b = b;
	}

	public void findMIssingCharsInBothStrings() {
		String alphabet = this.generateAlphabet();
		System.out.print("In string: " + this.a + this.b + " following letters are missing: ");
		this.findMissingOrTheSameCharacter((this.a + this.b), alphabet, false);
		System.out.println("");

	}

	public void findTheSameChars() {
		System.out.print("In string: " + this.a + " and  " + this.b + " following letters are the same: ");
		this.findMissingOrTheSameCharacter(this.a, this.b, true);
		System.out.println("");
	}

	public void findDifference() {
		System.out.println("Different letters int strings " + this.a +" and " + this.b + " are following: ");
		this.findMissingOrTheSameCharacter(this.a, this.b, false);
		this.findMissingOrTheSameCharacter(this.b, this.a, false);
		System.out.println("");
	}

	private void findMissingOrTheSameCharacter(String myString, String myString2, boolean findTheSame) {
		for (int i = 0; i < myString2.length(); i++) {
			boolean isThereLetter = false;
			for (int j = 0; j < myString.length(); j++) {
				if (myString2.charAt(i) == myString.charAt(j)) {
					isThereLetter = true;
				}
			}
			if(findTheSame) {
				if (isThereLetter) {
					System.out.print(myString2.charAt(i) + ", ");
				}
			} else {
				if (!isThereLetter) {
					System.out.print(myString2.charAt(i) + ", ");
				}
			}

		}
	}

	private String generateAlphabet() {
		String alphabet = "";
		for (int i = 97; i <= 122; i++) {
			alphabet += (char) i;
		}
		return alphabet;
	}
}
