package Matrix;

import java.util.HashMap;

public class ExchangeRate {
	private String readFile;
	ReadExchangeRate readExchangeRate;

	public ExchangeRate() {
		super();
		this.readFile = "06-06-2016.txt";
		this.readExchangeRate = new ReadExchangeRate(this.readFile);
	}
	
	public double calculateRates(double firstAmount, String startAbbrev, String endAbbrev ) {
		double secondAmount = 0;
		secondAmount = firstAmount * this.readExchangeRate.getExchangeRates().get(startAbbrev) /this.readExchangeRate.getExchangeRates().get(endAbbrev) ;
		return secondAmount;
	}

}
