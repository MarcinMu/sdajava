package useCasesTest;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;

import useCases.CountWords;

public class CountWordsTest {

	@Test
	public void countWordsTest() {
		//given
		String [] mySentences = {"ff ddd eee dfdfs dddff fff ", "fsd", "fdfd ddd", " a b c e d f 99 "};
		int [] results = {6, 1, 2, 7};
		
		for(int i = 0; i < mySentences.length; i++){
			//when
			int myResult = new CountWords().countWords(mySentences[i]);
			//then
			Assert.assertEquals(results[i], myResult);
		}
	}
	
	@Test
	public void getDataFromUserTest() {
		//given
		String myString = "My String";
		ByteArrayInputStream in = new ByteArrayInputStream(myString.getBytes());
		System.setIn(in);
		CountWords countWords = new CountWords();
		countWords.setMyScanner(new Scanner(System.in));
		//when
		String takenInput = countWords.getDataFromUser();
		//then
		Assert.assertEquals(takenInput, myString);
		//after
		System.setIn(System.in);
	}
}
