package useCasesTest;

import org.junit.Assert;
import org.junit.Test;

import useCases.QuadraticEquation;

public class QuadraticEquationTest {
	
	@Test
	public void calculateDeltaTest() {
		//given
		QuadraticEquation myEquation = new QuadraticEquation();
		myEquation.setA(1);
		myEquation.setB(1);
		myEquation.setC(-2);
		//when
		double result = myEquation.calculateDelta();
		//then
		Assert.assertEquals(9, result, 0.0001);
	}
	
	@Test
	public void solveEquationTest() {
		//given
		QuadraticEquation myEquation = new QuadraticEquation();
		myEquation.setA(1);
		myEquation.setB(1);
		myEquation.setC(-2);
		double delta = 9;
		//when
		myEquation.solveEquation(delta);
		//then
		Assert.assertEquals(-2.0, myEquation.getResult1(), 0.0001);
		Assert.assertEquals(1, myEquation.getResult2(), 0.0001);
	}
	
}
