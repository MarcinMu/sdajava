package useCasesTest;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;

import useCases.GuessNumber;

public class GuessNumberTest {
	
	@Test
	public void takeNumberFromUserTest() {
		//given
		String [] myInput = {"5", "1", "10"};
		int [] myOutput = {5, 1, 10};
		for(int i = 0; i < myInput.length; i++) {
			ByteArrayInputStream in = new ByteArrayInputStream(myInput[i].getBytes());
			System.setIn(in);
			GuessNumber guessNumber = new GuessNumber();
			guessNumber.setMyScanner(new Scanner(System.in));
			//when
			int takenNumber = guessNumber.takeNumberFromUser();
			//then
			Assert.assertEquals(myOutput[i], takenNumber);
			//after
			System.setIn(System.in);
		}
	}
	
	@Test
	public void userEnterWrongNumber() {
		//given
		String [] usersInput = {"-1", "0", "11"};
		for(int i = 0; i < usersInput.length; i++) {
			ByteArrayInputStream in = new ByteArrayInputStream(usersInput[i].getBytes());
			System.setIn(in);
			GuessNumber guessNumber = new GuessNumber();
			guessNumber.setMyScanner(new Scanner(System.in));
		}
		//when
		
		//then
	}
}
