package useCases;

import java.util.Scanner;

public class GuessNumber {
	
	private Scanner myScanner;
	
	public void run() {
		myScanner =  new Scanner(System.in);
		int numberToGuess = (int) (Math.random() * 10 + 1) ;
		int userNumber = takeNumberFromUser();
		while(numberToGuess != userNumber ) {
			userNumber = takeNumberFromUser();
		}
		myScanner.close();
		System.out.println("Congratulations. You guessed");
	}
	
	public int takeNumberFromUser() {
		System.out.println("Give me number from 1 to 10");
		int userNumber =  myScanner.nextInt();
		if (userNumber > 0 && userNumber <= 10) {
			return userNumber;
		}
		return takeNumberFromUser();
	}

	public final Scanner getMyScanner() {
		return myScanner;
	}

	public final void setMyScanner(Scanner myScanner) {
		this.myScanner = myScanner;
	}
	
}
