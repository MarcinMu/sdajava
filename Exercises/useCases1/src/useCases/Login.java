package useCases;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Login {
	
	Scanner myScanner;
	private List<String> login;
	private List<String> password;
	
	public Login() {
		this.login = new ArrayList<>();
		this.password = new ArrayList<>();
		this.login.add("Tyrion");
		this.password.add("qwe123");
		this.login.add("user");
		this.password.add("user");
		this.myScanner = new Scanner(System.in);
		this.run();
	}

	private boolean log() {
		System.out.println("Give me your login");
		String userLogin = this.myScanner.nextLine();
		System.out.println("Give me your password");
		String userPassword = this.myScanner.nextLine();
		if(userLogin.isEmpty() || userPassword.isEmpty()) {
			return false;
		}
		for (int i = 0; i < login.size(); i++ ) {
			if(this.login.get(i).equals(userLogin) && this.password.get(i).equals(userPassword)) {
				return true;
			}
		}
		return false;
	}

	private void run() {
		if(this.log()) {
			System.out.println("Welcome");
		} else  {
			System.out.println("Wrong login or password please try again");
			this.run();
		}
	}

}
