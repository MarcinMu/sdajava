package useCases;

import java.util.Scanner;

public class QuadraticEquation {
	private double a;
	private double b;
	private double c;
	Scanner myScanner;
	private double result1;
	private double result2;

	public QuadraticEquation() {
		this.a = 0;
		this.b = 0;
		this.c = 0;
	}

	public void run() {
		this.myScanner = new Scanner(System.in);
		this.getData();
		double delta = this.calculateDelta();
		int numberOfResults = this.solveEquation(delta);
		this.printResults(numberOfResults);
	}
	
	private void printResults(int numberOfResults) {
		if (numberOfResults == 0) {
			System.out.println("I wasn't able to calculate value. Good bye! ");
			return;
		} else if (numberOfResults == 1) {
			System.out.println("Your result is following: ");
			System.out.println(result1);
		} else {
			System.out.println("Your results are following: ");
			System.out.println(result1);
			System.out.println(result2);
		}
		System.out.println("Thank you for good cooperation. Good bye!");

	}

	public int solveEquation(double delta) {
		if (delta < 0) {
			return 0;
		} else if (delta == 0) {
			this.result1 = -this.b / (2 * this.a);
			return 1;
		} else {
			this.result1 = (-this.b - Math.sqrt(delta)) / (2 * this.a);
			this.result2 = (-this.b + Math.sqrt(delta)) / (2 * this.a);
			return 2;
		}
	}

	public double calculateDelta() {
		return (this.b * this.b - 4 * this.a * this.c);
	}

	private void getData() {
		try {
			System.out.println("Give me a");
			String a = myScanner.nextLine();
			if (!a.isEmpty()) {
				this.a = Double.parseDouble(a);
			}
			System.out.println("Give me b");
			String b = myScanner.nextLine();
			if (!b.isEmpty()) {
				this.b = Double.parseDouble(b);
			}
			System.out.println("Give me c");
			String c = myScanner.nextLine();
			if (!c.isEmpty()) {
				this.c = Double.parseDouble(c);
			}
			if (this.a == 0 && this.b == 0) {
				System.out.println("Give me at least a or b different than 0");
				this.getData();
			}
		} catch (NumberFormatException e) {
			System.out.println("Please enter numbers only");
			this.getData();
		}
	}

	public final double getA() {
		return a;
	}

	public final void setA(double a) {
		this.a = a;
	}

	public final double getB() {
		return b;
	}

	public final void setB(double b) {
		this.b = b;
	}

	public final double getC() {
		return c;
	}

	public final void setC(double c) {
		this.c = c;
	}

	public final double getResult1() {
		return result1;
	}

	public final void setResult1(double result1) {
		this.result1 = result1;
	}

	public final double getResult2() {
		return result2;
	}

	public final void setResult2(double result2) {
		this.result2 = result2;
	}
	
}
