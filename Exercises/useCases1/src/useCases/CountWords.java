package useCases;

import java.util.Scanner;

public class CountWords {
	Scanner myScanner;
	
	public void run() {
		myScanner = new Scanner(System.in);
		boolean success = false;
		while (!success) {
			String sentence = getDataFromUser();
			int wordsCounter = countWords(sentence);
			if (wordsCounter < 5) {
				System.out.println("Your sentence is shorter than 5 words");
			} else {
				System.out.println("Words number is following:" + wordsCounter);
				success = true;
			}
		}
		System.out.println("Thank you for your cooperation. Have a nice day!");
		myScanner.close();
	}

	public String getDataFromUser() {
		
		System.out.println("Give me your sentence and press enter");
		String sentence = null;
		sentence = myScanner.nextLine();
		if(sentence.isEmpty()) {
			sentence = this.getDataFromUser();
		}
		return sentence;
	}

	public int countWords(String sentence) {
		int counter = 0; 
		boolean lastCharWasLetter = false;
		for (int i = 0 ; i < sentence.length(); i++) {
			if (sentence.charAt(i) == ' ' || i == sentence.length() - 1) {
				if(lastCharWasLetter) {
					counter++;
				} 
				lastCharWasLetter = false;
			} else {
				lastCharWasLetter = true;
			}		
		}
		return counter;
		//return sentence.split(" ").length;
	}

	public final Scanner getMyScanner() {
		return myScanner;
	}

	public final void setMyScanner(Scanner myScanner) {
		this.myScanner = myScanner;
	}
}
