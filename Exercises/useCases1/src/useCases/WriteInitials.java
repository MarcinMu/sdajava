package useCases;

import java.io.PrintWriter;
import java.util.Scanner;

public class WriteInitials {

	private Scanner myScanner;

	public void run() {
		this.myScanner = new Scanner(System.in);
		this.writeToFile(this.takeInitials(this.takeDataFromUser()));
		System.out.println("Thank you for your cooperation. Have a nice day!");
	}

	public String takeDataFromUser() {
		System.out.println("Give me your first and last name");
		String result = myScanner.nextLine();
		if (result.split(" ").length == 2) {
			return result;
		}
		System.out.println("Data are incorrect");
		return this.takeDataFromUser();
	}

	public String takeInitials(String name) {
		for (int i = 0; i < name.length(); i++) {
			if (name.charAt(i) == ' ') {
				return ("" + name.charAt(0) + name.charAt(i + 1)).toUpperCase();
			}
		}
		return null;
	}

	public void writeToFile(String initials) {
		try {
			PrintWriter save = new PrintWriter("resource/initials.txt");
			save.println(initials);
			save.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
