
public class RandomBinaryTree {
	
	private Peak root;
	private int z;
	private double p;
	public int index;

	public RandomBinaryTree(int z) {
		super();
		this.root = null;
		this.p = 1/2;
		this.z = z; 
		this.index = 0;
		int randomNumber = (int) Math.random() * this.z; 
		this.root = new Peak(randomNumber, 0);
	}
	
	public RandomBinaryTree(int z, double p) {
		super();
		this.root = null;
		this.p = p;
		this.z = z; 
		int randomNumber = (int) (Math.random() * this.z); 
		this.root = new Peak(randomNumber, 0);
	}
	
	
	public void addElement(int index){
		Peak tempElement = new Peak((int)(Math.random() * this.z ), index);
		Peak currentBranch = root;
		boolean set = false;
		while (!set) {
			if(currentBranch.getLeft() == null && currentBranch.getRigth() != null) {
				currentBranch.setLeft(tempElement);
				set = true;
			} else if (currentBranch.getLeft() != null && currentBranch.getRigth() == null) {
				currentBranch.setRight(tempElement);
				set = true;
			} else if (currentBranch.getLeft() == null && currentBranch.getRigth() == null) {
				if (Math.random() < p) {
					currentBranch.setLeft(tempElement);
				} else {
					currentBranch.setRight(tempElement);
				}
				set = true;
			} else {
				if (Math.random() < p) {
					currentBranch = currentBranch.getLeft();
				} else {
					currentBranch = currentBranch.getRigth();
				}
			}			
		}
	}
	
	public void printAllTree() {
		Peak currentBranch = root;
		printItAndItsChildren(currentBranch);
	}
	
	public void printItAndItsChildren (Peak currentBranch) {
		System.out.println(currentBranch.getIndex() + " " +currentBranch.getX());
		if(currentBranch.getLeft() != null){
			System.out.println("left: ");
			printItAndItsChildren(currentBranch.getLeft());
		}  if (currentBranch.getRigth() != null) {
			System.out.println("right:");
			printItAndItsChildren(currentBranch.getRigth());
		}
	}
	
	public void printTree(){
		inorder(this.root);
	}
	
	public void inorder (Peak currentBranch ) {
		if (currentBranch != null) {
			System.out.println("");
			System.out.print("index: " + currentBranch.getIndex()+ " x: " + currentBranch.getX()+ "  ");
			if (currentBranch.getLeft() != null) {
				System.out.print("left: "+ currentBranch.getLeft().getIndex()+ " x: " + currentBranch.getLeft().getX()+ "  ");
			} 
			if (currentBranch.getRigth() != null) {
				System.out.print("right: "+ currentBranch.getRigth().getIndex()+ " x: " + currentBranch.getRigth().getX()+ "  ");
			} 
			inorder(currentBranch.getLeft());
			inorder(currentBranch.getRigth());
		}
	}
	public boolean checkIfElementInTree(int elementToFind) {
		return isElementInTree(elementToFind, this.root);
	}
	
	private boolean isElementInTree (int elementToFind, Peak node) {
		boolean isInTree = false;
		if (node!= null) {
			if (node.getX() == elementToFind) {
				System.out.println(node.getIndex()+"  " + node.getX()  );
				isInTree = true;
				return true;
			} else {
				isInTree = isElementInTree(elementToFind, node.getLeft()); 
				if ( isInTree == false ) {
					isElementInTree(elementToFind, node.getRigth());
				}	
			}

		} 
		return isInTree;
	}
	
	public int getTreeHeight() {
		return countTreeHeight(this.root);
	}
	public int countTreeHeight(Peak node) {
		if (node == null ) {
			return 0;
		}
		int leftHeight = countTreeHeight(node.getLeft());
		int rightHeight = countTreeHeight(node.getRigth());
		if(leftHeight >= rightHeight) return leftHeight+1;
		else return rightHeight + 1;
	}
	public static boolean isIsomorphic (Peak t1, Peak t2) {
		
		return false;
	}
	
	public void setBalancedTree(int n, int z) {
		this.root = setBalancedSubTree(n, z);
	}
	
	public Peak setBalancedSubTree(int n, int z) {
		int nl = 0; 
		int np = 0; 
		if(n > 0 ) {
			nl = n/2;
			np = n - nl - 1;
			int x = (int) (Math.random()* z + 1);
			this.index++;
			Peak newVertex = new Peak(x, this.index);
			newVertex.setLeft(setBalancedSubTree(nl, z));
			newVertex.setRight(setBalancedSubTree(np, z));
			return newVertex;
		}
		return null;
	}
	
}
