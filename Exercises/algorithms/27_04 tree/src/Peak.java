
public class Peak {
	private int x;
	private Peak left;
	private Peak right;
	private int index;
	

	public Peak(int x, int index) {
		super();
		this.x = x;
		this.left = null;
		this.right = null;
		this.index = index;
	}
	
	public Peak(int x) {
		super();
		this.x = x;
		this.left = null;
		this.right = null;
		this.index = 0;
	}
	
	public void setLeft(Peak left) {
		this.left = left;
	}
	
	public void setRight(Peak right) {
		this.right = right;
	}
	
	public Peak getLeft() {
		return left;
	}
	
	public Peak getRigth() {
		return right;
	}
	
	public int getX() {
		return x;
	}
	
	public int getIndex() {
		return index;
	}
}
