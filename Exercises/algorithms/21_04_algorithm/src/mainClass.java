import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.util.Arrays;
import java.util.Vector;
import java.util.zip.ZipEntry;

import javax.lang.model.element.VariableElement;
import javax.print.attribute.standard.RequestingUserName;

import org.omg.CORBA.PUBLIC_MEMBER;

public class mainClass {
	
	public static int[] generateArray (int size, int range) {
		int [] returnedArray = new int[size];
		for (int i = 0; i < size; i++) {
			returnedArray[i] = (int) Math.floor(Math.random()*range+1);
		}
		return returnedArray;
	}
	
	public static int getMinValueFromArray(int[] table) {
		int minValue = table[0];
		for(int i = 1; i < table.length; i++) {
			if(table[i] < minValue) {
				minValue = table[i];
			}
		}
		return minValue;
	}
	
	public static int findDigitsSum (int number) {
		System.out.println("We are summing: "+number); 
		int sum = 0;
		while(number != 0){
			sum += number%10;
			number /= 10;
		}
		return sum;
	}
	
	public static int [] bubbleSort (int [] tableToSort) {
		boolean swapTookPlace = false;
		do{
			swapTookPlace = false;
			for(int i = 0; i < tableToSort.length-1; i++) {
				if(tableToSort[i]>tableToSort[i+1]) {
					int pomElement = tableToSort[i];
					tableToSort[i]=tableToSort[i+1];
					tableToSort[i+1]=pomElement;
					swapTookPlace = true;
				}
			}
		} while(swapTookPlace);
		return tableToSort;
	}
	
	public static boolean [][] generateGraph (int size, double probability){
		boolean [][] graph = new boolean[size][size];
		for (int i = 0; i < size-1; i++) {
			for (int j = i+1; j < size; j++) {
				graph[i][j] = graph[j][i] = (Math.random() <= probability);
			}
		}
		return graph;
	}
	
	public static int[][] generateGraphNK(int peakNumber, int edgeNumber) {
		int[][] graph = new int[peakNumber*(peakNumber-1)/2][2];
		int graphIndex = 0; 
		for (int i = 0; i < peakNumber - 1; i++){
			for (int j = i + 1; j < peakNumber; j++) {
				graph[graphIndex][0] = i+1;
				graph[graphIndex][1] = j+1;
				graphIndex++;
			}
		}
		for(int i = 0; i < edgeNumber; i++) {
			int chosenEdge = (int) (Math.random() * peakNumber*(peakNumber-1)/2);
			int [] temp = new int[2];
			temp = graph[chosenEdge];
			graph[chosenEdge] = graph[peakNumber*(peakNumber-1)/2 - i-1];
			graph[peakNumber*(peakNumber-1)/2 - i-1] = temp;
		}
		
		return graph;
	}
	public static boolean[][] genearateGraphAndTransform(int N, int K) {
		int pomTab [][] = generateGraphNK(N, K);
		return transformEA(pomTab, K, N);
	}
	public static void genearateGraphAndTransformAE(){
		boolean pomTab[][] = generateGraph(20, 0.5);
		int pom[][]= transformAE(pomTab);	
		printTwoDimensialTable(pom, 20);
	}
	public static boolean[][] transformEA (int [][] E, int h, int size) {
		boolean [][] a = new boolean[size][size];
		for(int i = E.length-1; i>=E.length-h; i--) {
			a[E[i][0]-1] [E[i][1]-1] = true;
			a[E[i][1]-1] [E[i][0]-1] = true;
		}
		return a;
	}
	
	public static int[][] transformAE (boolean [][] a) {
		int N = a.length;
		int h = N*(N-1)/2;
		int [][] E = new int [h][2];
		
		int indexE=0;
		for(int i=0; i<N-1;i++) {
			for (int j=1; j<N; j++) {
				if(a[i][j] == true) {
					E[indexE][0] = i;
					E[indexE][1] = j;
					indexE++;
				}
			}
		}
		return E;
	}
	public static newClass[] generateGraphAndTransformAS(int N, float P) {
		boolean pomTab[][] = generateGraph(N, P);
		return transformAS(N, pomTab);
	}
	public static newClass[] transformAS(int N,  boolean[][] A) {
		newClass[] s = new newClass[N];
		for(int i =0; i<N; i++) {
			s[i] = new newClass();
		}
		for (int i =0; i < N -1; i++) {
			for (int j = i+1; j<N; j++) {
				if(A[i][j]){
					s[i].count++;
					s[i].tab.add(j+1);
					s[j].tab.add(i+1);
					s[j].count++;
				}
			}
		}
		return s;
	}
	public static int findNumberOfTriangles (int N, boolean[][] A) {
		int numberOfTriangles =0; 
		newClass[] helpArray = transformAS(N, A);
		for(int i = 0; i < helpArray.length-1; i++ ) {
			for(int j = i+1; j<helpArray[i].tab.size(); j++){
				Vector<Integer> array1 = helpArray[i].tab;
				Vector<Integer> array2 = helpArray[j].tab;
				for (int k=j; k < array1.size(); k++) {
					if(array1.get(k)!= null && array2.indexOf( array1.get(k) )>= 0 ) {
						numberOfTriangles++;
					}
				}
			}
		}
		return numberOfTriangles;
	}
	
	public static void printTwoDimensialTable (int tableToPrint [][], int numberOfElements) {
		for(int i = 0; i < numberOfElements; i++) {
			System.out.println("Row "+i+" : "+ Arrays.toString(tableToPrint[tableToPrint.length - i - 1]));
		}
	}
	
	public static void printTwoDimensialTable (boolean tableToPrint [][]) {
		for(int i = 0; i < tableToPrint.length; i++) {
			System.out.println("Row "+i+" : "+ Arrays.toString(tableToPrint[i]));
		}
	}
	
	public static void graphIntegrityInitiation () {
		int N = 4;
		boolean [] verticalesArray= new boolean [N];
		boolean [][] myGraph = generateGraph(N, (float) 0.45);
		boolean[] result = graphIntegrityCheck(N, 1, myGraph, verticalesArray);
		boolean goodGraph = true;
		for(int i = 0; i < N; i++) {
			if(verticalesArray[i]==false){
				goodGraph= false;
			}
		} 
		if (goodGraph == true) {
			System.out.println("this graph is good one");
		} else {
			System.out.println("Bad luck, this graph isn't good one");
		}
		System.out.println("Used data are following: ");
		System.out.println(Arrays.toString(result));
		printTwoDimensialTable(myGraph);
	}
	
	public static boolean [] graphIntegrityCheck (int N, int v,  boolean[][] A, boolean [] verticalesArray) {
		
		for (int i = 0; i < N; i++) {
			if(A[v][i] && verticalesArray[i]==false) {
				verticalesArray[i] = true;
				graphIntegrityCheck(N, i, A, verticalesArray);
			}
		}
		return verticalesArray;
	}
	
	public static Ez[] generateGnf (int N, int F) {
		int h = N * (N-1)/2;
		int [][] E = generateGraphNK(N, h);
		int [] c = new int [N];
		
		Ez[] resultE = new Ez[h];
		for (int i = 0; i < h; i++) {
			resultE[i]= new Ez(E[i][0], E[i][1], false);
		}
		
		do {
			int z_index = (int) Math.floor(Math.random() * h); 
			if(c[resultE[z_index].a-1] < F && c[resultE[z_index].b-1] < F) {
				c[resultE[z_index].a-1]++;
				c[resultE[z_index].b-1]++;
				
				Ez tempObject = resultE[z_index];
				resultE[z_index] = resultE[h-1];
				resultE[h-1] = tempObject;
				
				resultE[z_index].c = true;
			}
			h--;
		} while(h>0);
		return resultE;
	}

	public static void printEzObject(Ez object) {
		System.out.println("A: " + object.a + "B: " + object.b + "C: " + object.c);
	}
	
	public static void main(String[] args) {
//		int [] generatedArray = generateArray(20, 100);
//		System.out.println("Generated array is: "+Arrays.toString(generatedArray));
//		System.out.println("Minimum value in this array is: "+getMinValueFromArray(generatedArray));
//		System.out.println("Sum of digits: "+findDigitsSum(4567));
//		System.out.println("Array after sorting: "+Arrays.toString(bubbleSort(generatedArray)));
//		System.out.println("My wonderful graph: ");
//		printTwoDimensialTable(generateGraph(20, 0.5));
//		System.out.println("My 2nd wonderful graph: " );
//		boolean [][] matrix = genearateGraphAndTransform(4, 6);
//		for(int i=0; i<matrix.length; i++) {
//			System.out.println(Arrays.toString(matrix[i]));
//		}
//		genearateGraphAndTransformAE();
//		newClass[] matrix = generateGraphAndTransformAS(10, (float)0.1);
//		System.out.println(findNumberOfTriangles(4, matrix));
//		printTwoDimensialTable(matrix);
//		for (int i =0; i < matrix.length; i++) {
//			System.out.println()  (matrix[i].toString());
//		}
		Ez e2Array [] = generateGnf (4, 2);
		for(int i = 0; i < e2Array.length; i++) {
			System.out.println(e2Array[i].a+ " "+ e2Array[i].b + " "+ e2Array[i].c);
		}
		GraphTransformation myGraphTransformation = new GraphTransformation();
		myGraphTransformation.genereateEz();
//		graphIntegrityInitiation();
	}
	
	public static ColorGraph letsColor(GraphTransformation graf) {
		ColorGraph colors = new ColorGraph();
		int highestVal = graf.degSort[0];
		
		colors.tabc = new int[graf.vertex];
		while (highestVal>0) {
			for(int i = 0; i< graf.deg.length; i++) {
				if(graf.deg[i] == highestVal) {
					colors.tabc[i] = checkColor(colors, graf.A, i);
				}
			}
			highestVal --;
		}
		return colors;
	}
	public static int checkColor (ColorGraph colors, boolean[][] tabGraph, int vertexIndex) {
		int numberColor = 1;
		boolean setColor = false;
		do {
			for (int row = 0; row < tabGraph.length; row++ ) {
				if(tabGraph[row][vertexIndex] == true && colors.tabc[row] == numberColor){
					numberColor++;
					setColor = true;
					break;
				}
			}
		} while(setColor);
	
		return numberColor;
	}
	
}
