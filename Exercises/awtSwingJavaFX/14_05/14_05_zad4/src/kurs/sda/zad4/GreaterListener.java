package kurs.sda.zad4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GreaterListener implements ActionListener {

	Main myMain;
	
	public GreaterListener(Main myMain) {
		super();
		this.myMain = myMain;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Compare myComparator = new Compare();
		boolean result = myComparator.compareNumber(this.myMain);
		if (result) {
			this.myMain.label.setText("A is bigger than B");
		} else {
			this.myMain.label.setText("A isn't bigger than B");
		}
	}
	

}
