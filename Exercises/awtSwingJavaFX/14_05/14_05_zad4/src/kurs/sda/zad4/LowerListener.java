package kurs.sda.zad4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LowerListener implements ActionListener {
	Main myMain;
	
	public LowerListener(Main myMain) {
		super();
		this.myMain = myMain;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Compare myComparator = new Compare();
		boolean result = myComparator.compareNumber(this.myMain);
		if (result) {
			this.myMain.label.setText("A isn't smaller than B");
		} else {
			this.myMain.label.setText("A is smaller than B");
		}

	}

}
