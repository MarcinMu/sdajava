package kurs.sda.zad4;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Main extends Frame {

	TextField numberA, numberB;
	Button greater, lower;
	Label label;

	public Main() {
		setTitle("Zad 4");
		setSize(600, 300);
		setLayout(new FlowLayout());
		setVisible(true);

		numberA = new TextField();
		numberB = new TextField();
		greater = new Button("Wieksza");
		lower = new Button("Mniejsza");
		label = new Label("Result would be here ");

		add(numberA);
		add(numberB);
		add(greater);
		add(lower);
		add(label);

		greater.addActionListener(new GreaterListener(this));
		lower.addActionListener(new LowerListener(this));
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		

	}

	public static void main(String[] args) {
		new Main();
	}

}
