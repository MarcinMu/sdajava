package kurs.sda.zad4;

public class Compare {
	public boolean compareNumber(Main myMain) {
		Double a = 0.0;
		Double b = 0.0;
		try {
			a = Double.parseDouble(myMain.numberA.getText());
			b = Double.parseDouble(myMain.numberB.getText());
			
		} catch (NumberFormatException e) {
			System.out.println("Put correct numbers please");
		}
		
		return (a > b);
	}
}
