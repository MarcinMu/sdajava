import org.junit.Assert;
import org.junit.Test;

public class PrinterTest {
	
	@Test
	public void letterSpaceString() {
		// given
		String textToBeChanged = "Lorem Ipsum";
		Zad1 zad1 = new Zad1();
		Printer myPrinter = new Printer(zad1);
		// when
		String textChanged = myPrinter.letterSpacing(textToBeChanged);
		// then
		Assert.assertEquals(textChanged, "L o r e m   I p s u m ");
	}
}
