import java.awt.*;
import java.awt.event.*;

import javax.swing.text.AbstractDocument.BranchElement;

class ButtonDemo1 extends Frame implements ActionListener {

	    Button printButton;
	    Button closeButton;
	    public ButtonDemo1() {
	        setTitle("Hi! I am a Frame");
	        setSize(400, 400);
	        setLayout(new FlowLayout());
	        setVisible(true);

	        // Just add a button (optional)
	        printButton = new Button("OK!");
	        closeButton = new Button("Zamknij");
	        
	        closeButton.addActionListener(this);
	        printButton.addActionListener(this);
	        
	        closeButton.setBounds(200, 200, 200, 50);
	       
	        
	        add(printButton);
	        add(closeButton);
	        
	        addWindowListener(new WindowAdapter() {
	            public void windowClosing(WindowEvent we) {
	            	
	                System.exit(0);
	            }
	        });
	    }

//	    public static void main(String args[]) {
//	        new ButtonDemo1();
//	    }

		@Override
		public void actionPerformed(ActionEvent e) {
			String str = e.getActionCommand();
			System.out.println(str);
			
		}
	}

