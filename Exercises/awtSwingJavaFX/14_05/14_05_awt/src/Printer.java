import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Printer implements ActionListener {
	
	Zad1 ex; 
	
	public Printer(Zad1 ex) {
		super();
		this.ex = ex;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.out.println("Wcisni�to Przycisk");
		String upperCasedtitle =this.ex.getTitle().toUpperCase();
		this.ex.setTitle(letterSpacing(upperCasedtitle));
	}
	
	public String letterSpacing(String text) {
		String changedTitle = "";
		for (int i = 0; i < text.length(); i++){
			changedTitle += text.charAt(i) + " ";
		}
		return changedTitle;
	}
	
}
