import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.HeadlessException;

public class Zad1 extends Frame {
	Button printButton;
	Button closeButton;

	public Zad1() throws HeadlessException {
		super();
		setTitle("My wonderful window");
		setSize(400, 400);
		setLayout(new FlowLayout());
		setVisible(true);
		
		printButton = new Button("Wydrukuj tekst");
		closeButton = new Button("Zamknij");

		add(printButton);
		add(closeButton);
		
		printButton.addActionListener(new Printer(this));
		closeButton.addActionListener(new Close());

	}

	public static void main(String args[]) {
		new Zad1();
	}

}
