import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.TextField;

public class Main extends Frame{
	
	Button okButton;
	TextField myText;
	Button closeButton;

	
	public Main() throws HeadlessException {
		super();
		setTitle("My wonderful window");
		setSize(400, 400);
		setLayout(new FlowLayout());
		setVisible(true);
		
		okButton = new Button("Wypisz odwr�cony tekst");
		myText = new TextField();
		closeButton = new Button("Zamknij");
		
		add(okButton);
		add(myText);
		add(closeButton);
		
		okButton.addActionListener(new SetNumber(this));
		closeButton.addActionListener(new Close());
		
		
	}



	public static void main(String[] args) {
		new Main();

	}

}
