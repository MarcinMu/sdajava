import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SetNumber implements ActionListener {
	
	Main myMain;
	
	
	public SetNumber(Main myMain) {
		super();
		this.myMain = myMain;
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		Integer a = 0;
		try {
			 a = Integer.parseInt(this.myMain.myText.getText());
		} catch (NumberFormatException e) {
			System.out.println("Wrong Number: " + e);
		}
		a = -a;
		this.myMain.myText.setText(((a).toString()) );
		System.out.println(a);
		

	}

}
