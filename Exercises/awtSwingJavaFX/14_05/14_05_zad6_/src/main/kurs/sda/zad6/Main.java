package kurs.sda.zad6;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.TextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Main extends Frame {

	Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b0;
	Button c;
	Button ce;
	Button plus, minus, divide, muliply, equal;
	TextField result;
	TextField currentValue;

	public Main() {
		setTitle("Calculator better than in winddows");
		setSize(500, 500);
		setLayout(new FlowLayout());
		setVisible(true);

		b1 = new Button("1");
		b2 = new Button("2");
		b3 = new Button("3");
		b4 = new Button("4");
		b5 = new Button("5");
		b6 = new Button("6");
		b7 = new Button("7");
		b8 = new Button("8");
		b9 = new Button("9");
		b0 = new Button("0");
		c = new Button("c");
		ce = new Button("ce");
		plus = new Button("+");
		minus = new Button("-");
		divide = new Button("/");
		muliply = new Button("*");
		equal = new Button("=");
		result = new TextField("");
		currentValue = new TextField("");

		add(result);
		add(currentValue);
		add(b1);
		add(b2);
		add(b3);
		add(b4);
		add(b5);
		add(b6);
		add(b7);
		add(b8);
		add(b9);
		add(b0);
		add(c);
		add(ce);
		add(plus);
		add(minus);
		add(divide);
		add(muliply);
		add(equal);

		b1.addActionListener(new ButtonListener(this));
		b2.addActionListener(new ButtonListener(this));
		b3.addActionListener(new ButtonListener(this));
		b4.addActionListener(new ButtonListener(this));
		b5.addActionListener(new ButtonListener(this));
		b6.addActionListener(new ButtonListener(this));
		b7.addActionListener(new ButtonListener(this));
		b8.addActionListener(new ButtonListener(this));
		b9.addActionListener(new ButtonListener(this));
		b0.addActionListener(new ButtonListener(this));
		c.addActionListener(new ButtonListener(this));
		ce.addActionListener(new ButtonListener(this));
		plus.addActionListener(new ButtonListener(this));
		minus.addActionListener(new ButtonListener(this));
		divide.addActionListener(new ButtonListener(this));
		muliply.addActionListener(new ButtonListener(this));
		equal.addActionListener(new ButtonListener(this));

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
	}

	public static void main(String[] args) {
		new Main();
	}

}
