package application;

import java.awt.Desktop.Action;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Controller implements Initializable {

	@FXML
	private Label sectionLabel;

	@FXML
	private Label themeLabel;

	@FXML
	private Label contentLabel;

	@FXML
	private TextArea content;

	@FXML
	private TextField theme;

	@FXML
	private TextField section;

	@FXML
	private Button add;

	@FXML
	private Button remove;

	@FXML
	private TableView table;

	@FXML
	private TableColumn<TaskModel, String> tableSubject;
	
	boolean isEdition = false;

	public ObservableList<TaskModel> list;

	public Controller() {
		this.list = FXCollections.observableArrayList();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		table.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> paramObservableValue, Number prevRowIndex,
					Number currentRowIndex) {
				try {
					TaskModel x = list.get((int) currentRowIndex);
					theme.setText(x.getTheme().getValue());
					content.setText(x.getContent().getValue());
					section.setText(x.getSection().getValue());
					
					int i = table.getSelectionModel().selectedIndexProperty().getValue();
					if(i >= 0) {
						isEdition = true;
						add.setText("Edit");
					} else {
						isEdition = false;
						add.setText("Add");
					}
					System.out.println(i);
					
					//
					// System.out.println(x.getName().getValue() + " " +
					// x.getSurname().getValue());
				} catch (Exception e) {
					System.out.println("list is empty");
				}

			}

		});
		this.tableSubject.setCellValueFactory(param -> param.getValue().getSection());
		this.table.setItems(list);
	}

	@FXML
	public void add(ActionEvent e) {
		if (isEdition) {
			System.out.println("To do edition");
			isEdition = false;
		} else {
			this.list.add(new TaskModel(this.theme.getText(), this.section.getText(), this.content.getText()));
		}
	}

	public void remove(ActionEvent e) {
		if (this.list.size() > 0) {
			this.list.remove(this.list.get(table.getSelectionModel().selectedIndexProperty().getValue()));
		}
	}

	public void edit(ActionEvent e) {

	}

}
