package application;

import javafx.beans.property.SimpleStringProperty;

public class TaskModel {
	private SimpleStringProperty theme;
	private SimpleStringProperty section;
	private SimpleStringProperty content;
	
	public TaskModel(String theme, String section, String content) {
		super();
		this.theme = new SimpleStringProperty(theme);
		this.section = new SimpleStringProperty(section);
		this.content = new SimpleStringProperty(content);
	}

	public SimpleStringProperty getTheme() {
		return theme;
	}

	public void setTheme(SimpleStringProperty theme) {
		this.theme = theme;
	}

	public SimpleStringProperty getSection() {
		return section;
	}

	public void setSection(SimpleStringProperty section) {
		this.section = section;
	}

	public SimpleStringProperty getContent() {
		return content;
	}

	public void setContent(SimpleStringProperty content) {
		this.content = content;
	}
	
	

}
