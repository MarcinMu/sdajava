package application;



import javafx.scene.image.Image;

public class Turtle {
	Image[] turtleArray;
	boolean moveRight;
	int posX;
	int posY;
	int moveLeftBorder;
	int moveRightBorder;
	int timer;
	
	public Turtle(int startPosX, int startPosY) {
		super();
		this.turtleArray = new Image[2];
		turtleArray[0] = new Image("/res/t1.png");
		turtleArray[1] = new Image("/res/t2.png");
		this.moveRight = true;
		this.posX = startPosX;
		this.posY = startPosY;
		moveLeftBorder = 0;
		moveRightBorder = Main.width - (int ) turtleArray[0].getWidth();
		this.timer = 0;
	}
	
	public int getStartPosX() {
		if(this.timer == 5) {
			this.turtleMove();
			this.timer = 0;
		} else {
			this.timer++;
		}
		
		return posX;
	}

	public int getStartPosY() {
		return posY;
	}

	public Image getFrame() {
		if(moveRight ) {
			return turtleArray[0];
		}
		return turtleArray[1];
	}
	private void turtleMove() {
		if(moveRight && this.posX < this.moveRightBorder){
			posX +=5;
		}
		if(this.posX >= this.moveRightBorder) {
			moveRight = false;
		}
		if(!moveRight && this.posX > this.moveLeftBorder){
			posX-=5;
		}
		if(this.posX == this.moveLeftBorder) {
			moveRight = true;
		}
	}
	
}
