package application;
import java.util.HashSet;

import javafx.scene.Scene;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class MoveLeftRigtht{

	HashSet<String> currentActiveKeys = new HashSet<String>();

	private Scene theScene;
	private static boolean reachedMaxPos = false;
	static boolean isUpKeyPressed = false;
	

	public MoveLeftRigtht( Scene theScene) {

		this.theScene = theScene;
		this.addMarioEvents();
	}
	
	public void addMarioEvents() {
		this.theScene.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent arg0) {
				currentActiveKeys.add(arg0.getCode().toString());
				
				if (currentActiveKeys.contains("LEFT")) {
					AnimatedImage.isAnim = true;
					AnimatedImage.moveRight = false;
					Main.marioPosX -=5;
				} 
				if (currentActiveKeys.contains("RIGHT")) {
					AnimatedImage.isAnim = true;
					AnimatedImage.moveRight = true;
					Main.marioPosX +=5;
				}
				if (currentActiveKeys.contains("UP") && !MoveLeftRigtht.isUpKeyPressed) {
					
					AnimatedImage.isAnim = true;
					AnimatedImage.isJump = true;
					boolean isAnimated = false;
					MoveLeftRigtht.isUpKeyPressed = true;
					AnimationTimer jumpAnimation = new AnimationTimer() {
						
						@Override
						public void handle(long now) {
							int stopMarioPosY = Main.height - 238;
							if(stopMarioPosY < Main.marioPosY && !MoveLeftRigtht.reachedMaxPos) {
								Main.marioPosY -=5;
								if (stopMarioPosY == Main.marioPosY)
									MoveLeftRigtht.reachedMaxPos = true;
							}
							if(stopMarioPosY +100 > Main.marioPosY && MoveLeftRigtht.reachedMaxPos) {
								Main.marioPosY +=5;
								if (stopMarioPosY +100 == Main.marioPosY) {
									MoveLeftRigtht.reachedMaxPos = false;
									stop();
									MoveLeftRigtht.isUpKeyPressed = false;
								}	
							}
						}
					};
					if(AnimatedImage.isJump) {
						jumpAnimation.start();
						isAnimated = true;
					}
					else {
						jumpAnimation.stop();
						isAnimated = false;
					}
				}
								
			}
		});
		
		this.theScene.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent arg0) {
				currentActiveKeys.remove(arg0.getCode().toString());
				AnimatedImage.isAnim = false;
				AnimatedImage.isJump = false;
			}
		});
	}	
}
