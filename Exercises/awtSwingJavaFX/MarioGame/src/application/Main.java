package application;


import java.awt.SystemTray;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
	final static int width = 604;
	final static int height = 480;
	public static int marioPosX;
	public static int marioPosY;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) {
		
		final long startNanoTime = System.nanoTime();
		
		primaryStage.setTitle("Mario");
		Group root = new Group();
		Scene theScene = new Scene(root);
		primaryStage.setScene(theScene);
		Canvas canvas = new Canvas(Main.width, Main.height);

		root.getChildren().add(canvas);

		GraphicsContext gc = canvas.getGraphicsContext2D();
		primaryStage.show();
		
		AnimatedImage marioMove = new AnimatedImage();
		Image backgroundImage = new Image("/res/grass.png");
		Turtle killer = new Turtle( 0, Main.height - 120);
		marioMove.setDuration(0.5);

		MoveLeftRigtht moveLeftRigtht = new MoveLeftRigtht(theScene);
		
		Main.marioPosX = Main.width/2 - (int)( marioMove.getFrame(0).getWidth()/2);
		Main.marioPosY = Main.height - 138;
		
		new AnimationTimer() {
			
			@Override
			public void handle(long now) {
				double t = (now - startNanoTime) / Math.pow(10, 9);
				gc.clearRect(0, 0, Main.width, Main.height);
				gc.drawImage(backgroundImage, 0, 0);
				gc.drawImage(killer.getFrame(), killer.getStartPosX(), killer.getStartPosY());
				gc.drawImage(marioMove.getFrame(t), Main.marioPosX, Main.marioPosY );
				
			}
		}.start();
		
		
		
	}

}
