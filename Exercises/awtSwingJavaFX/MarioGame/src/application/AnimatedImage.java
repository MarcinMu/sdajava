package application;
import javafx.scene.image.Image;

public class AnimatedImage {
    private double duration;
    public static boolean isAnim;
    public static boolean isJump = false;
    static boolean moveRight = true;
	public Image[] imageArray;
	public Image[] imageArrayJump;

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}
	
	public Image getFrame(double time){
		if(AnimatedImage.isJump == false) {
			return getStaticFrame( time);
		} 
		return getJumpFrame( time);
	}
	
	public Image getStaticFrame(double time)
    {
        int index = (int)((time % (imageArray.length/2 * duration)) / duration);
        
        if(AnimatedImage.isAnim == false) {
        	if (moveRight == true) {
        		return imageArray[0];
        	}
        	return imageArray[2];
        } 
        if(moveRight == true) {
        	return imageArray[index];
        }
        return imageArray[index+2];
        
    }
	
	public Image getJumpFrame(double time)
    {
        int index = (int)((time % (imageArrayJump.length/3 * duration)) / duration);
        
        if(AnimatedImage.isAnim == false) {
        	if (moveRight == true) {
        		return imageArrayJump[0];
        	}
        	return imageArrayJump[3];
        } 
        if(moveRight == true) {
        	return imageArrayJump[index];
        }
        return imageArrayJump[index+2];
        
    }

	public AnimatedImage() {
		readPic();
	}
	
	private void readPic() {
	    imageArray = new Image[4];
		imageArray[0] = new Image("/res/0.png");
		imageArray[1] = new Image("/res/1.png");
		imageArray[2] = new Image("/res/0R.png");
		imageArray[3] = new Image("/res/1R.png");
		
		imageArrayJump = new Image[6];
		imageArrayJump[0] = new Image("/res/2.png");
		imageArrayJump[1] = new Image("/res/3.png");
		imageArrayJump[2] = new Image("/res/4.png");
		imageArrayJump[3] = new Image("/res/2R.png");
		imageArrayJump[4] = new Image("/res/3R.png");
		imageArrayJump[5] = new Image("/res/4R.png");

	}
}