package game.number;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.RadialGradient;

public class Controller {

	@FXML
	private Label info;
	
	@FXML
	private TextField userInput;
	
	@FXML
	private Button guess;
	
	private int randomNumber;

	public Controller() {
		this.randomNumber = (int) (Math.random() * 99 + 1);
		System.out.println(this.randomNumber);
	}
	
	
	
	
	
	public void checkAnswer(ActionEvent e) {
		try {
			int enteredNumber = Integer.parseInt(this.userInput.getText()) ;
			if(enteredNumber == this.randomNumber ) {
				this.info.setText("Congratulations. You've guessed the number.");
				this.guess.setDisable(true);
				this.userInput.setDisable(true);
			} else if (enteredNumber > this.randomNumber  ){
				this.info.setText("You entered wrong value. Try again lower. ");
			} else if (enteredNumber < this.randomNumber  ){
				this.info.setText("You entered wrong value. Try again larger. ");
			}
		} catch (NumberFormatException e2) {
			System.out.println("Not a number in input" );	
		}
	}

	
}
