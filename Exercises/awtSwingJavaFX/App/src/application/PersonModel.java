package application;

import java.text.SimpleDateFormat;
import java.time.LocalDate;

import javafx.beans.property.*;

public class PersonModel {
	private SimpleStringProperty name;
	private SimpleStringProperty surname;
	private SimpleLongProperty id;
	private ObjectProperty<LocalDate> date;

	public PersonModel(String name, String surname, Long id, LocalDate date) {
		this.name = new SimpleStringProperty(name);
		this.surname = new SimpleStringProperty(surname);
		this.id = new SimpleLongProperty(id);
		this.date = new SimpleObjectProperty<LocalDate>(date);
	}

	public SimpleStringProperty getName() {
		return name;
	}

	public void setName(SimpleStringProperty name) {
		this.name = name;
	}

	public SimpleStringProperty getSurname() {
		return surname;
	}

	public void setSurname(SimpleStringProperty surname) {
		this.surname = surname;
	}

	public SimpleLongProperty getId() {
		return ( id);
	}

	public void setId(SimpleLongProperty id) {
		this.id = id;
	}

	public SimpleObjectProperty<LocalDate> getDateProperty() {
		return (SimpleObjectProperty<LocalDate>) date;
	}

	public LocalDate getDate() {
		return date.get();
	}

	public void setDate(SimpleObjectProperty<LocalDate> date) {
		this.date = date;
	}


}
