package application;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;

public class Controller implements Initializable {

	public ObservableList<PersonModel> list;

	@FXML
	private Label name;
	@FXML
	private Label surname;
	@FXML
	private Label ID;
	@FXML
	private Label birthDate;
	@FXML
	private TextField nameText;
	@FXML
	private TextField surnameText;
	@FXML
	private TextField IDText;
	@FXML
	private DatePicker birthDatePicker;
	@FXML
	private Button save;
	@FXML
	private Button clear;
	@FXML
	private Button delete;
	@FXML
	private TableView<PersonModel> table;
	@FXML
	private TableColumn<PersonModel, String> userName;
	@FXML
	private TableColumn<PersonModel, Long> userID;
	@FXML
	private TableColumn<PersonModel, String> userSurname;

	public PersonListController prc = new PersonListController();

	@FXML
	private void clearTextFields(ActionEvent event) {
		this.nameText.clear();
		this.surnameText.clear();
		this.IDText.clear();
		this.birthDatePicker.setValue(null);
		//this.list.add(new PersonModel("imie3", "nazwisko3", 123, 456));
	}
	
	@FXML
	private void deleteTextFields(ActionEvent event) {
		this.list.remove(this.list.get(table.getSelectionModel().selectedIndexProperty().getValue()));
	}



	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		table.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
		     @Override
		     public void changed(ObservableValue<? extends Number> paramObservableValue,Number prevRowIndex, Number currentRowIndex) {
		    	 try {
		        	 PersonModel x = list.get((int) currentRowIndex);
			    	 nameText.setText(x.getName().getValue());
			    	 surnameText.setText(x.getSurname().getValue());
			    	 IDText.setText(Long.toString(x.getId().getValue()));
			    	 birthDatePicker.setValue(x.getDate());
			    	 System.out.println(x.getDate());

			          System.out.println(x.getName().getValue() + " " +
			        		  x.getSurname().getValue());
				} catch (Exception e) {
					System.out.println("list is empty");
				} 
		    	 


		     }
		});

		this.userName.setCellValueFactory(param->param.getValue().getName());


		this.userSurname.setCellValueFactory(new Callback
					<TableColumn.CellDataFeatures<PersonModel,String>,
					ObservableValue<String>>() {

			@Override
			public ObservableValue<String> call(CellDataFeatures<PersonModel,
					String> param) {
				return param.getValue().getSurname();
			}
		});
		
		this.userID.setCellValueFactory(param->param.getValue().getId().asObject());

		this.list = FXCollections.observableArrayList();
//		this.list.add(new PersonModel("imie0", "nazwisko0", 123, LocalDate.now()));
//		this.list.add(new PersonModel("imie1", "nazwisko2", 123, LocalDate.now()));

		this.table.setItems(this.list);
	}
	private boolean checkIfExistsId(){
		boolean help=false;
		for (PersonModel personModel : list) {
			System.out.println(personModel.getId().getValue());
			System.out.println((this.IDText.getText()));

			if((personModel.getId().getValue()).equals(Long.parseLong(this.IDText.getText()))){
				help = true;
				System.out.println(personModel.getId().getValue());
				System.out.println(equals(this.IDText.getText()));
			}
		}
	return help;
	}

	@FXML
	private void savePerson (ActionEvent e){

		if(!this.checkIfExistsId()){
			try {

				this.list.add(new PersonModel(this.nameText.getText(), this.surnameText.getText(), Long.parseLong(this.IDText.getText()), this.birthDatePicker.getValue()));
			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				System.out.println("NFE");
			}
		}else{

		}
	}


}