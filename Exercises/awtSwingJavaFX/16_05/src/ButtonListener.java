import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener {

	GenerateTicket myFrame;
	
	public ButtonListener(GenerateTicket myFrame1) {
		this.myFrame = myFrame1;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		new NewWindow(this.myFrame);
	}
	

}
