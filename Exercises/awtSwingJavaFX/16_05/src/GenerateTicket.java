import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;


public class GenerateTicket extends Frame {
	
	Label date;
	Label time ;
	Label number;
	Label waiting;
	Label branch;
	Button generate;
	
	TextField dateText;
	TextField timeText;
	TextField numberText;
	TextField waitingText;
	TextField branchText;
	
	public GenerateTicket() {
		setTitle("Ticket generation");
		setBounds(200, 200, 600, 800);
		
		setLayout(new GridLayout(6, 2));
		setVisible(true);
		
		date = new Label("Data: ");
		time = new Label("Czas: ");
		number = new Label("Numer: ");
		waiting = new Label("Liczba oczekujących: ");
		branch = new Label("Oddział: ");
		Button generate = new Button("Generate ticket");
		
		Panel firstPanel = new Panel();
		Panel secondPanel = new Panel();
		
		dateText = new TextField();
		timeText = new TextField();
		numberText = new TextField();
		waitingText = new TextField();
		branchText = new TextField();
		

		firstPanel.add(date);
		firstPanel.add(dateText);
		firstPanel.add(time);
		firstPanel.add(timeText);
		firstPanel.add(number);
		firstPanel.add(numberText);
		firstPanel.add(waiting);
		firstPanel.add(waitingText);
		firstPanel.add(branch);
		firstPanel.add(branchText);
		firstPanel.add(generate);
		
		generate.addActionListener(new ButtonListener(this));
		
		firstPanel.setLayout(new GridLayout(6, 2));
		secondPanel.setLayout(null);
		add(firstPanel);
		add(secondPanel);

		
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		
	}
}
