package com.marcin.MathOperation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class App 
{
    public static void main( String[] args )
    {
		ApplicationContext appCtx = new AnnotationConfigApplicationContext(CalculatorConfig.class);
		Calculator bean = appCtx.getBean(Calculator.class);
		bean.printResult();
//        Calculator calc = new Calculator();
//        calc.printResult();
    }
}
