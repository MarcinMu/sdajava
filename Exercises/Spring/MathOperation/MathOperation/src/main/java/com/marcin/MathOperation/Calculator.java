package com.marcin.MathOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component
public class Calculator {
	
	@Autowired
	public MathOperation mathOperation;
	
	@Autowired
	public MathOperation mathOperation2;
	
	public void printResult() {
		System.out.println(mathOperation.calculate(2.0 ,3.0));
	}
}
