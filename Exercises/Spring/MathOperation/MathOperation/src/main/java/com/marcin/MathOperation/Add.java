package com.marcin.MathOperation;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class Add implements MathOperation {

	public double calculate(double arg1, double arg2) {
		
		return (arg2+arg1);
	}

}
