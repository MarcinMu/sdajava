package com.marcin.MathOperation;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("Subtr")
public class Subtraction implements MathOperation {

	public double calculate(double arg1, double arg2) {
		
		return arg1 - arg2;
	}

}
