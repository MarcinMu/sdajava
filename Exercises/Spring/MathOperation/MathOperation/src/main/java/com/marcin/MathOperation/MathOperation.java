package com.marcin.MathOperation;

public interface MathOperation {
	public double calculate(double arg1, double arg2);
}
