package com.marcin.MathOperation;

import org.springframework.stereotype.Component;

@Component
public class Dividing implements MathOperation {

	public double calculate(double arg1, double arg2) {
		
		return arg1/arg2;
	}

}
