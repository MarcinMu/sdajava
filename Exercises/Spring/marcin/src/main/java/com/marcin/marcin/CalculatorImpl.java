package com.marcin.marcin;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class CalculatorImpl implements Calculator {
	
	@Autowired
	private FileParser parser;
	
	@Value("classpath:data.txt")
	private Resource inputFile;
	
	@Override
	public int calculateAverage() {

		List<Integer> data = parser.splitString(readLineFromFile());
		int sum = 0;
		for(int i = 0; i < data.size(); i++) {
			sum+=data.get(i);
		}
		sum=sum/data.size();
		return sum;
	}
	
	private String readLineFromFile() {
		String line = null;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputFile.getInputStream()))) {
			line = reader.readLine();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		return line;
	}
}
