package com.marcin.marcin;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FileParserImpl implements FileParser{
	
	@Value("${separationChar}")
	String splitChar;
	
	@Override
	public List <Integer> splitString(String inputData) {
		List <Integer> dataFromFile = new ArrayList<>();

		String parts [] = inputData.split(splitChar);
		for(int i = 0; i < parts.length; i++) {
			dataFromFile.add(Integer.parseInt(parts[i]));
		}
		return dataFromFile;
	}
}
