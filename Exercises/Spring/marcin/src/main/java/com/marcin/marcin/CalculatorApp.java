package com.marcin.marcin;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;



public class CalculatorApp {
	
	  public static void main( String[] args )
	    {
			ApplicationContext appCtx = new AnnotationConfigApplicationContext(CalculatorConfig.class);
			Calculator bean = appCtx.getBean(Calculator.class);
			System.out.println(bean.calculateAverage());

	    }
}
