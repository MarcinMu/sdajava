package com.marcin.marcin;

import java.util.List;

public interface FileParser {
	public List <Integer> splitString(String inputData) ;
}
