package com.marcin;

public class Rectangle {
	private double a;
	private double b;

	public Rectangle(double a, double b) {
		super();
		this.a = a;
		this.b = b;
	}
	
	public double calculateArea() {
		return this.a * this.b;
	}
}
