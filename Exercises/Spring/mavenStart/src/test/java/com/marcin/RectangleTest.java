package com.marcin;

import org.junit.Assert;
import org.junit.Test;

public class RectangleTest {
	
	@Test
	public void calculateAreaOfRectangle() {
		//given
		Rectangle myRectangle = new Rectangle(4, 5);
		// when
		double myRectangleArea = myRectangle.calculateArea();
		// then
		Assert.assertEquals(20, myRectangleArea, 0.0001);
	}
}
