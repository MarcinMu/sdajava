package pl.eurobet.core.team;

public interface TeamManager {
	
	public void addTeam(Team team);
	public void removeTeam(Team team);

}
