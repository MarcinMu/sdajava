package pl.eurobet.core.team;

import java.util.List;

import org.springframework.stereotype.Repository;

import pl.eurobet.core.AbstractBaseDao;

@Repository
public class TeamDaoImpl extends AbstractBaseDao<Team, Long> implements TeamDao {

	@Override
	protected Class<Team> supports() {
		return Team.class;
	}

	@Override
	public Team getByName(String teamName) {
		List <Team> allTeams = getHibernateTemplate().loadAll(supports());	
		for(Team myTeam: allTeams) {
			if(myTeam.getName().equals(teamName)){
				return myTeam;
			}
		}
		return null;
	}
	
}
