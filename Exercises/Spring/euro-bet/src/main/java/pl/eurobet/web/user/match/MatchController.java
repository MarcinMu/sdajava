package pl.eurobet.web.user.match;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import pl.eurobet.core.match.Match;
import pl.eurobet.core.match.MatchDao;
import pl.eurobet.core.match.MatchManager;
import pl.eurobet.core.match.MatchManagerImpl;
import pl.eurobet.core.team.Team;
import pl.eurobet.core.team.TeamDao;

@Controller
public class MatchController {
	
	

	@Autowired
	private MatchDao matchDao;
	
	@Autowired
	private TeamDao teamDao;
	
	@Autowired
	private MatchManager matchManager;
	
	@Autowired
	private MatchValidator matchValidator;
	
	@InitBinder     
	public void initBinder(WebDataBinder binder){
		binder.addValidators(matchValidator);
	}
	
	@RequestMapping("/user/matches")
	public ModelAndView showMatchesList() {
		ModelAndView modelAndView = new ModelAndView("match/show");
		List<Team> myTeams = teamDao.getAll();
		Map<Long,String> team = new LinkedHashMap<Long,String>();
		for(Team myTeam: myTeams) {
			team.put(myTeam.getId(), myTeam.getName());
		}
		modelAndView.addObject("matches", matchDao.getAll());
		modelAndView.addObject("teamList", team);
		modelAndView.addObject("match", new Match());
		return modelAndView;				
	}
	
	
	@RequestMapping("/admin/addMatch")
	public String addMatche(@ModelAttribute Match match) {
		matchDao.saveOrUpdate(match);
		//matchManager.addMatch(matchDate, hostTeam, guestTeam );
		return "redirect:/user/matches";
	}
	
	
}
