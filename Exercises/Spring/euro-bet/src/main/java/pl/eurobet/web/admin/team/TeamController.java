package pl.eurobet.web.admin.team;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import pl.eurobet.core.team.Team;
import pl.eurobet.core.team.TeamDao;
import pl.eurobet.core.team.TeamManager;

@Controller
public class TeamController {
	
	@Autowired
	private TeamDao teamDao;
	
	@Autowired
	private TeamManager teamManager;
	
	@RequestMapping("/admin/teams")
	public ModelAndView showTeamList() {
		ModelAndView modelAndView = new ModelAndView("team/show");
		modelAndView.addObject("teams", teamDao.getAll());
		return modelAndView;				
	}
	
	@RequestMapping(value = "/admin/addTeam", method = RequestMethod.POST)
	public RedirectView addTeam(@RequestParam("teamName") String teamName) {
		teamManager.addTeam(new Team(teamName));
		return new RedirectView("teams") ;
	}
	
	@RequestMapping(value = "/admin/removeTeam", method = RequestMethod.POST)
	public RedirectView removeTeam(@RequestParam("teamId") Long teamId){
		Team teamToBeDeleted = teamDao.getById(teamId);
		teamManager.removeTeam(teamToBeDeleted);
		return new RedirectView("teams");
	}
	

}
