package pl.eurobet.core.match;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import pl.eurobet.core.team.TeamDao;


@Component
public class MatchManagerImpl implements MatchManager {
	
	@Autowired
	private MatchDao matchDao;
	
	@Autowired
	private TeamDao teamDao;
	
	@Override
	@Transactional
	public void addMatch(Match match) {
		matchDao.saveOrUpdate(match);
	}



}
