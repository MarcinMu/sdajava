package pl.eurobet.core.team;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TeamManagerImpl implements TeamManager {

	@Autowired
	private TeamDao teamDao;
	
	@Override
	@Transactional
	public void addTeam(Team team) {
		teamDao.saveOrUpdate(team);
	}

	@Override
	@Transactional
	public void removeTeam(Team team) {
		teamDao.delete(team);
		
	}

}
