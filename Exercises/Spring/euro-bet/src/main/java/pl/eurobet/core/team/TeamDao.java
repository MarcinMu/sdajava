package pl.eurobet.core.team;

import pl.eurobet.core.BaseDao;

public interface TeamDao extends BaseDao<Team, Long> {

	Team getByName(String teamName);
	

}
