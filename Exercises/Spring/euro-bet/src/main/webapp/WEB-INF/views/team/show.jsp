<%@ page session="false"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>

<html lang="en">
<body>

	<h2> Existing teams </h2>
	<table>
		<c:forEach var="team" items="${teams}">
			<tr>
				<td>${team.name}</td>
				<td>
					<form name = 'teamDelete' action = "<c:url value = '/admin/removeTeam' />" method = 'POST'>
						<input type = "hidden" value = ${team.id} name = "teamId"> </input>
						<input type = "submit" value = "Delete" </input>
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>
	
	<h2> Add new team </h2>

	<table>
		<tr>
			<td> Team name </td>
			<td> Action </td>
		</tr>
		<tr>
			<form name='teamAddingForm' action="<c:url value='/admin/addTeam' />" method='POST'>
				<td> <input name = "teamName"> </input> </td>
				<td> <input type = "submit" value = "Add"> </td>
			</form>
		</tr>
		
	</table>

</body>
</html>