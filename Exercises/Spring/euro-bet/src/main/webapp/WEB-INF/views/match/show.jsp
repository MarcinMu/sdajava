<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<body>
	
	<h2> Current matches </h2>
	<table>
		<tr><td> Time </td> <td> host </td><td> Guest </td> <td> Result </td> <td> Action </td>
		<c:forEach var="match" items="${matches}">
			<tr>
				<td>${match.matchTime}</td><td>${match.hostTeam.name}</td><td>${match.guestTeam.name}</td>
				
			</tr>
		</c:forEach>
	</table>
	
	<sec:authorize access="hasRole('ROLE_ADMIN')">
	<h2> Add new match </h2>
		<table>
			<tr>
				<td> Match date </td>
				<td> Host </td>
				<td> Guest </td>
				<td> Action </td>
			</tr>
			<tr>
				<spring:url value="/admin/addMatch" var="addMatchAction" />	
				<form:form modelAttribute = "match" action="${addMatchAction}" method='POST'>
					<td> 
					<spring:bind path="matchTime">	
						<form:input path="matchTime" type = "datetime" value="2016-06-16 18:13" /></td>
						<form:errors path="matchTime" />
					</spring:bind>
					<td> 
						<spring:bind path= "hostTeam">
							<form:select path = "hostTeam.id">
								<form:option value = "NONE" label = "--- Select ---" />
								<form:options items = "${teamList}"/>
							</form:select>			
						</spring:bind>			
					 </td>
					 <td> 
						<spring:bind path= "guestTeam">
							<form:select path = "guestTeam.id">
								<form:option value = "NONE" label = "--- Select ---" />
								<form:options items = "${teamList}"/>
							</form:select>			
						</spring:bind>			
					 </td>
					<td> <input type = "submit" value = "Add"> </input> </td>
				</form:form>
			</tr>
			
		</table>
	</sec:authorize>	

</body>
</html>