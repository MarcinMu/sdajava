package com.kedzier.spring.hibernate.core;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class SpringHibernateApp {
	
	private static final Logger LOG = LoggerFactory.getLogger(SpringHibernateApp.class); 

	public static final void main(String[] args) throws IOException {
		ApplicationContext appCtx = new AnnotationConfigApplicationContext(CoreConfig.class);
//		EmployeeManager employeeManager = appCtx.getBean(EmployeeManager.class);
//		employeeManager.printEmployees();
		TeamsManager teamsManager = appCtx.getBean(TeamsManager.class);
		teamsManager.printTeamsSquad();

	}

}
