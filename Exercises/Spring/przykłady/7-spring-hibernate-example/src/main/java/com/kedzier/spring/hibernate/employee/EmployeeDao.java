package com.kedzier.spring.hibernate.employee;

import java.util.List;

public interface EmployeeDao {
	
	List<Employee> getAll();
	
	void add(Employee employeeToAdd);

	void delete(Employee employeeToRemove);
	
	void update(Employee employee);
}
