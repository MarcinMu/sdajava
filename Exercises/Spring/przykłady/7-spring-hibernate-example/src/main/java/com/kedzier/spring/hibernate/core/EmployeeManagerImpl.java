package com.kedzier.spring.hibernate.core;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.kedzier.spring.hibernate.employee.Employee;
import com.kedzier.spring.hibernate.employee.EmployeeDao;

@Component
public class EmployeeManagerImpl implements EmployeeManager {

	private static final Logger LOG = LoggerFactory.getLogger(EmployeeManagerImpl.class);

	
	@Autowired
	private EmployeeDao employeeDao;
	
	@Override
	@Transactional
	public void increaseEmployeesSalary(double number) {
		List<Employee> employees = employeeDao.getAll();
		for (Employee employee : employees) {
			employee.setMainSalary(employee.getMainSalary() * number);
			LOG.info("Podnosze pensje pracownika {} o {}", employee.getFullName(), number);
			employeeDao.update(employee);
			
		}


	}

	@Override
	@Transactional
	public void printEmployees() {
		List<Employee> employees = employeeDao.getAll();
		for (Employee employee : employees) {
			LOG.info("pracownik {} pracuje w zespole {}", employee.getFullName(), employee.getTeam().getTeamName());
		
			
		}
		
	}

}
