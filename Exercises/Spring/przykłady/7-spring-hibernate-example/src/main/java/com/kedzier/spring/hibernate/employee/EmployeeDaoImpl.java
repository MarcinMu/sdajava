package com.kedzier.spring.hibernate.employee;

import java.util.List;



import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


public class EmployeeDaoImpl extends HibernateDaoSupport implements EmployeeDao {


	
	@Override
	public List<Employee> getAll() {
		return getHibernateTemplate().loadAll(Employee.class);
	}

	@Override
	public void add(Employee employeeToAdd) {
		getHibernateTemplate().saveOrUpdate(employeeToAdd);
	}

	@Transactional
	@Override
	public void delete(Employee employeeToRemove) {
		getHibernateTemplate().delete(employeeToRemove);
		
	}

	@Override
	public void update(Employee employee) {
		getHibernateTemplate().saveOrUpdate(employee);
		
	}
	
	

}
