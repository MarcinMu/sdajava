package com.kedzier.spring.hibernate.teams;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import com.kedzier.spring.hibernate.employee.*;


@Entity(name = "zespoly")
public class Team {
	
	@Id
	@Column(name="id_zesp")
	private int id;
	
	@Column(name = "nazwa")
	private String teamName;
	
	@Column(name = "adres") 
	private String address;

	@OneToMany(mappedBy = "team")
	private Set<Employee> employees;
	
	public final Set<Employee> getEmployees() {
		return employees;
	}

	public final void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}

	public final int getId() {
		return id;
	}

	public final void setId(int id) {
		this.id = id;
	}

	public final String getTeamName() {
		return teamName;
	}

	public final void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public final String getAddress() {
		return address;
	}

	public final void setAddress(String address) {
		this.address = address;
	}
	
	
}
