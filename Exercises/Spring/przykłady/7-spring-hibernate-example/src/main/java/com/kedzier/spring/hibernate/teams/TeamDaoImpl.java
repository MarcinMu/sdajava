package com.kedzier.spring.hibernate.teams;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;


@Repository
public class TeamDaoImpl extends HibernateDaoSupport implements TeamDao {

	@Autowired
	public void setSessionFactoryBean(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	@Override
	public List<Team> getAll() {
		return getHibernateTemplate().loadAll(Team.class);
	}

	@Override
	public void delete(Team teamToDelete) {
		getHibernateTemplate().delete(teamToDelete);
		
	}

}
