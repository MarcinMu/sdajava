package com.kedzier.spring.hibernate.teams;

import java.util.List;


public interface TeamDao {

	List<Team> getAll();
	
	void delete(Team teamToDelete);
	
}
