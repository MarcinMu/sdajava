package com.kedzier.spring.hibernate.core;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.kedzier.spring.hibernate.employee.Employee;
import com.kedzier.spring.hibernate.teams.Team;
import com.kedzier.spring.hibernate.teams.TeamDao;

@Component
public class TeamsManager {

	@Autowired
	private TeamDao teamDao;
	
	private static final Logger LOG = LoggerFactory.getLogger(EmployeeManagerImpl.class);
	
	@Transactional
	public void printTeamsSquad(){
		List<Team> teams = teamDao.getAll();
		for(Team team: teams) {
//			Set<Employee> employees = team.getEmployees();
//			Employee [] a ;
//			employees.toArray(a);
			LOG.info("W zespole {} pracują {}", team.getTeamName(), team.getEmployees());
			LOG.info("Zespół {} zatrudnia {} pracowników", team.getTeamName(), team.getEmployees().size());
		}
	}
	
	
}
