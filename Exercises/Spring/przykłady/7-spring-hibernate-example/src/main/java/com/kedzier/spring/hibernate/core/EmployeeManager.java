package com.kedzier.spring.hibernate.core;

public interface EmployeeManager {
	
	void increaseEmployeesSalary(double number);
	
	void printEmployees();

}
