package com.kedzier.spring.hibernate.employee;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.kedzier.spring.hibernate.teams.Team;

@Entity(name = "pracownicy")
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_prac")
	private Long id;
	
	@Column(name = "nazwisko")
	private String fullName;
	
	@ManyToOne
	@JoinColumn(name = "etat")
	private String position;
	
	@Column(name = "zatrudniony")
	private Date employmentDate;
	
	@Column(name = "placa_pod")
	private Double mainSalary;
	
	@Column(name = "placa_dod")
	private Double additionalSalary;

	@ManyToOne
	@JoinColumn(name = "id_zesp")
	private Team team;
	
	public final Team getTeam() {
		return team;
	}

	public final void setTeam(Team team) {
		this.team = team;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Date getEmploymentDate() {
		return employmentDate;
	}

	public void setEmploymentDate(Date employmentDate) {
		this.employmentDate = employmentDate;
	}

	public Double getMainSalary() {
		return mainSalary;
	}

	public void setMainSalary(Double mainSalary) {
		this.mainSalary = mainSalary;
	}

	public Double getAdditionalSalary() {
		return additionalSalary;
	}

	public void setAdditionalSalary(Double additionalSalary) {
		this.additionalSalary = additionalSalary;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Employee [id=");
		builder.append(id);
		builder.append(", fullName=");
		builder.append(fullName);
		builder.append(", position=");
		builder.append(position);
		builder.append(", employmentDate=");
		builder.append(employmentDate);
		builder.append(", mainSalary=");
		builder.append(mainSalary);
		builder.append(", additionalSalary=");
		builder.append(additionalSalary);
		builder.append("]");
		return builder.toString();
	}

}
