package com.kedzier.spring.hibernate.job;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.kedzier.spring.hibernate.employee.Employee;

@Entity(name = "etaty")
public class Job {
	
	@OneToMany(mappedBy = "etat")
	@Column(name = "nazwa")
	private Set<Employee> employees;
	
	@Column(name = "nazwa")
	private String name;
	
	@Column(name = "placa_min")
	private int minSalary;
	
	@Column(name = "placa_max")
	private int maxSalary;

	public final String getName() {
		return name;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final int getMinSalary() {
		return minSalary;
	}

	public final void setMinSalary(int minSalary) {
		this.minSalary = minSalary;
	}

	public final int getMaxSalary() {
		return maxSalary;
	}

	public final void setMaxSalary(int maxSalary) {
		this.maxSalary = maxSalary;
	}
	
	
}
