package com.kedzier.spring.mvc.user.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.kedzier.spring.mvc.user.Favourites;
import com.kedzier.spring.mvc.user.User;

@Component
public class CustomFavouriteValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Favourites fav = (Favourites) target;
		if(!fav.isHotDog() && !fav.isHamburger() && !fav.isIceCreams() && !fav.isJava()  ) {
			errors.rejectValue("iceCreams", "validation.user.favourite.empty");
		}
		if(!fav.isJava() ) {
			errors.rejectValue("java", "validation.user.favourite.java");
		}
		
	}



}
