package com.kedzier.spring.mvc.user.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.kedzier.spring.mvc.user.User;

@Component
public class CustomUserValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return User.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		User user = (User) target;

		if (user.getSurname() == null || user.getSurname().length() < 3) {
			errors.rejectValue("surname", "validation.user.surname.empty");
		} 
		if (user.getName() == null || user.getName().length() < 3) {
			errors.rejectValue("name", "validation.user.name.empty");
		}


	}

}
