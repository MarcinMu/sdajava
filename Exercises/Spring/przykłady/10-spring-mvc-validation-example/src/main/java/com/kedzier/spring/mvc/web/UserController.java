package com.kedzier.spring.mvc.web;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.kedzier.spring.mvc.user.Favourites;
import com.kedzier.spring.mvc.user.User;
import com.kedzier.spring.mvc.user.validation.CustomFavouriteValidator;
import com.kedzier.spring.mvc.user.validation.CustomUserValidator;

@Controller
public class UserController {
	
	@Autowired
	private CustomUserValidator userValidator;
	
	@Autowired
	private CustomFavouriteValidator favValidator;
	
	@InitBinder     
	public void initBinder(WebDataBinder binder){
	     //binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true, 10));
	     binder.addValidators(userValidator);
	     binder.addValidators(favValidator);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView userAddForm() {
		ModelAndView modelAndView = new ModelAndView("createUser");
		modelAndView.addObject("user", new User());
		modelAndView.addObject("fav", new Favourites());
		return modelAndView;
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addUser(@ModelAttribute @Validated User user, BindingResult result, RedirectAttributes redirectAttributes) {
		
		if (result.hasErrors()) {
			return "createUser";
		} else {

			redirectAttributes.addFlashAttribute("msg", "User added successfully!");
			redirectAttributes.addFlashAttribute("user", user);
			
			// POST/REDIRECT/GET
			return "redirect:/show";
		}
	}
	
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public String showAddedUser() {
		return "showUser";
	}

}
