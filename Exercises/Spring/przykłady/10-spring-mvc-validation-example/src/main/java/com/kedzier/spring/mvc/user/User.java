package com.kedzier.spring.mvc.user;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.kedzier.spring.mvc.user.validation.PostalCode;

public class User {


	private String surname;
	
	private String name;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	@Temporal(TemporalType.DATE)
	private Date birthDate;	
	
	private String city;
		
	@PostalCode
	private String postalCode;
	
	private boolean hotDog;
	
	private boolean hamburger;
	
	private boolean iceCreams;
	
	private boolean java;
	

	public final boolean isJava() {
		return java;
	}

	public final void setJava(boolean java) {
		this.java = java;
	}

	public final boolean isHotDog() {
		return hotDog;
	}

	public final void setHotDog(boolean hotDog) {
		this.hotDog = hotDog;
	}

	public final boolean isHamburger() {
		return hamburger;
	}

	public final void setHamburger(boolean hamburger) {
		this.hamburger = hamburger;
	}

	public final boolean isIceCreams() {
		return iceCreams;
	}

	public final void setIceCreams(boolean iceCreams) {
		this.iceCreams = iceCreams;
	}

	public final String getName() {
		return name;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
		
	
	
}
