package com.kedzier.spring.mvc.user.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PostalCodeValidator implements ConstraintValidator<PostalCode, String> {

	@Override
	public void initialize(PostalCode code) {
	}

	@Override
	public boolean isValid(String code, ConstraintValidatorContext ctx) {
		if (code.matches("\\d{2}-\\d{3}")) {
			return true;
		}
		return false;
	}
}
