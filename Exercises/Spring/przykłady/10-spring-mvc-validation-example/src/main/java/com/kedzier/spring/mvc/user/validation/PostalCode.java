package com.kedzier.spring.mvc.user.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = PostalCodeValidator.class)
@Target(java.lang.annotation.ElementType.FIELD)
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface PostalCode {

	String message() default "Correct postal code format is XX-XXX";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
