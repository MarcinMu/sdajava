package com.kedzier.spring.mvc.user;

public class Favourites {
	
	private boolean hotDog;
	
	private boolean hamburger;
	
	private boolean iceCreams;
	
	private boolean java;

	public final boolean isHotDog() {
		return hotDog;
	}

	public final void setHotDog(boolean hotDog) {
		this.hotDog = hotDog;
	}

	public final boolean isHamburger() {
		return hamburger;
	}

	public final void setHamburger(boolean hamburger) {
		this.hamburger = hamburger;
	}

	public final boolean isIceCreams() {
		return iceCreams;
	}

	public final void setIceCreams(boolean iceCreams) {
		this.iceCreams = iceCreams;
	}

	public final boolean isJava() {
		return java;
	}

	public final void setJava(boolean java) {
		this.java = java;
	}
	
	
}
