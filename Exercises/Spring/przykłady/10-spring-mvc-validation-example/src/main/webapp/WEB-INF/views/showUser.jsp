<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<body>

	<h1>User added successfully</h1>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>field</th>
				<th>value</th>
			</tr>
		</thead>
			<tr>
				<td>Name</td>
				<td>${user.name}</td>					
			</tr>
			<tr>
				<td>Surname</td>
				<td>${user.surname}</td>					
			</tr>
			<tr>
				<td>Birth Date</td>
				<td><fmt:formatDate type="date" pattern="yyyy-MM-dd" value="${user.birthDate}" /></td>					
			</tr>
			<tr>
				<td>City</td>
				<td>${user.city}</td>					
			</tr>
			
			<tr>
				<td>Postal Code</td>
				<td>${user.postalCode}</td>					
			</tr>
	</table>
	
	<spring:url value="/" var="addUserFormUrl" />
	<button class="btn btn-primary" onclick="location.href='${addUserFormUrl}'">Try again</button>

</body>
</html>