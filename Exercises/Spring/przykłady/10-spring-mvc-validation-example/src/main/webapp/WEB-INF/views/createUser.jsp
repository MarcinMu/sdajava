<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<body>
	<h1><spring:message code="welcome.example" text="Hello! Lets add some user" /></h1>
	<spring:url value="/add" var="addUserAction" />
	<form:form method="post" modelAttribute="user"
		action="${addUserAction}">
		<table>
			<tr>
				<td><label>First Name</label></td>
				<td>
					<spring:bind path="name">
						<form:input path="name" type="text" id="name" />
						<form:errors path="name" />
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td><label>Surname</label></td>
				<td>
					<spring:bind path="surname">
						<form:input path="surname" type="text" id="surname" />
						<form:errors path="surname" />
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td>
					<label>Birth Date</label>
				</td>
				<td>
					<spring:bind path="birthDate">				
						<form:input path="birthDate" type="date" id="birthDate" />
						<form:errors path="birthDate" />
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td>
					<label>City</label>
				</td>
				<td>
					<spring:bind path="city">					
						<form:input path="city" type="text" id="city" />
						<form:errors path="city" />
					</spring:bind></td>
			</tr>
			<tr>
				<td>
					<label>Postal Code</label>
				</td>				
				<td>
					<spring:bind path="postalCode">						
						<form:input path="postalCode" type="text" id="postalCode" />
						<form:errors path="postalCode" />
					</spring:bind>
				</td>
			</tr>
		</table>
		<table>
			<tr> 
				<td>
					<label> User likes</label>
				</td>
			</tr>
			<tr>
				<td>
					<label> Java </label>
					<spring:bind path="java">
						<form:checkbox path = "java" id="java" />
						<form:errors path = "java" />
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td>
					<label> Hot Dog </label>
					<spring:bind path="hotDog">
						<form:checkbox path = "hotDog" id="hotdog" />
					</spring:bind>
				</td>
				<td>
					<label> Hamburger </label>
					<spring:bind path="hamburger">
						<form:checkbox path = "hamburger"  id="hamburger" />
					</spring:bind>
				</td>
				<td>
					<label> Ice Creams </label>
					<spring:bind path="iceCreams">
						<form:checkbox path = "iceCreams" id="iceCreams" />
						<form:errors path = "iceCreams" />
					</spring:bind>
				</td>
			</tr>
		</table>
		<button type="submit">Add</button>

	</form:form>



</body>
</html>