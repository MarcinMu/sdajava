package com.kedzier.spring.mvc.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kedzier.spring.mvc.service.User;

@Controller
public class GreetingController {
	
	@RequestMapping("/")
	public ModelAndView getGreetingForm() {
		ModelAndView modelAndView = new ModelAndView("greetingForm");
		modelAndView.addObject("user", new User());
		return modelAndView;
	}
	
	@RequestMapping(value = "/sayHello", method = RequestMethod.POST)
	public ModelAndView sayHello(@ModelAttribute User user) {
		ModelAndView modelAndView = new ModelAndView("greeting");
		modelAndView.addObject("userName", user.getName());
		return modelAndView;
	}
	
}
