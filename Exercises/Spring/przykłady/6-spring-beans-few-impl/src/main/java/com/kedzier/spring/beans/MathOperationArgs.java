package com.kedzier.spring.beans;

public class MathOperationArgs {
	
	private double arg1;
	private double arg2;
	private char operation;
	
	public MathOperationArgs(double arg1, double arg2, char operation) {
		super();
		this.arg1 = arg1;
		this.arg2 = arg2;
		this.operation = operation;
	}

	public double getArg1() {
		return arg1;
	}
	
	public double getArg2() {
		return arg2;
	}
	
	public char getOperation() {
		return operation;
	}

}
