package com.kedzier.spring.beans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class InputFileReader {
	
	@Value("${input.path}")
	private Resource inputFile;
	
	public List<MathOperationArgs> readFile() throws IOException {
		List<MathOperationArgs> result = new ArrayList<>();
		
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputFile.getInputStream()))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				result.add(readLine(line));
			}
		}
		return result;
	}

	private MathOperationArgs readLine(String line) {
		char operation = line.charAt(0);
		String commaSeparatedArgs = line.substring(1, line.length());
		String[] args = commaSeparatedArgs.split(",");
		return new MathOperationArgs(Double.valueOf(args[0]), Double.valueOf(args[1]), operation);
	}

}
