package com.kedzier.spring.beans.operations;

public interface MathOperation {
	
	double calculate(double arg1, double arg2);
	
	char operationChar();

}
