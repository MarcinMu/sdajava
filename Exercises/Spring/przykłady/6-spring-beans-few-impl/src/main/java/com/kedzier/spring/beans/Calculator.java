package com.kedzier.spring.beans;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kedzier.spring.beans.operations.MathOperation;

@Component
public class Calculator implements InitializingBean {

	@Autowired
	private List<MathOperation> availableOperations;

	@Autowired
	private InputFileReader inputFileReader;

	private Map<String, MathOperation> availableOperationsMap = new HashMap<>();;

	public void printCalculations() throws IOException {

		List<MathOperationArgs> arguments = inputFileReader.readFile();
		for (MathOperationArgs argument : arguments) {
			char operation = argument.getOperation();
			MathOperation mathOperation = availableOperationsMap.get(String.valueOf(operation));
			double arg1 = argument.getArg1();
			double arg2 = argument.getArg2();
			System.out.println("Result of operation " + operation + " on numbers " + arg1 + " and " + arg2 + " is " + mathOperation.calculate(arg1, arg2));

		}

	}

	@Override
	public void afterPropertiesSet() throws Exception {
		for (MathOperation operation : availableOperations) {
			availableOperationsMap.put(String.valueOf(operation.operationChar()), operation);
		}

	}

}
