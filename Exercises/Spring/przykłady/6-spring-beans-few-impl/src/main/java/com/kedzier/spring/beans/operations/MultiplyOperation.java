package com.kedzier.spring.beans.operations;

import org.springframework.stereotype.Component;

@Component
public class MultiplyOperation implements MathOperation {

	@Override
	public double calculate(double arg1, double arg2) {
		return arg1 * arg2;
	}
	
	@Override
	public char operationChar() {
		return '*';
	}

}
