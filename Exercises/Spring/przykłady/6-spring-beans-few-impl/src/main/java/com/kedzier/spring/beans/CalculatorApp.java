package com.kedzier.spring.beans;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CalculatorApp {
	
	public static final void main(String[] args) throws IOException {
		ApplicationContext appCtx = new AnnotationConfigApplicationContext(MathOperationsConfig.class);
		Calculator bean = appCtx.getBean(Calculator.class);
		bean.printCalculations();
	}

}
