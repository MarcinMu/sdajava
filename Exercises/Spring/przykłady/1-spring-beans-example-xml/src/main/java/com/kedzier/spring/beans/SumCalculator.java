package com.kedzier.spring.beans;

public interface SumCalculator {

	Integer calculateSum();

}