package com.kedzier.spring.beans;

import java.io.IOException;
import java.util.List;

public interface InputFileReader {

	List<Integer> readFile() throws IOException;

}