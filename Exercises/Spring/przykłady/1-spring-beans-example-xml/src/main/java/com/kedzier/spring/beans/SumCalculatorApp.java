package com.kedzier.spring.beans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SumCalculatorApp {
	
	public static final void main(String[] args) {
		ApplicationContext appCtx = new ClassPathXmlApplicationContext("app-ctx.xml");
		SumCalculator bean = appCtx.getBean(SumCalculatorImpl.class);
		System.out.println(bean.calculateSum());
	}

}
