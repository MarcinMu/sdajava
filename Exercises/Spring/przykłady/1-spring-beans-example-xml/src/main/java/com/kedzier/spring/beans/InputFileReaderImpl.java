package com.kedzier.spring.beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class InputFileReaderImpl implements InputFileReader {
	
	private URI filePath;
	
	@Override
	public List<Integer> readFile() throws IOException {
		
		List<Integer> result = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(new File(filePath)))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				result.add(Integer.valueOf(line));
			}
		}
		return result;
	}

	public void setFilePath(URI filePath) {
		this.filePath = filePath;
	}
	
	

}
