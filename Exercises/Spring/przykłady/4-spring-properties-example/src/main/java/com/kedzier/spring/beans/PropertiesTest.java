package com.kedzier.spring.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesTest {

	@Value("${name}")
	private String nameParam;
	
	@Value("${surname}")
	private String surnameParam;
	
	@Value("${age}")
	private String ageParam;

	public void printProperties() {
		
		System.out.println(nameParam);
		System.out.println(surnameParam);
		System.out.println(ageParam);
	}

}
