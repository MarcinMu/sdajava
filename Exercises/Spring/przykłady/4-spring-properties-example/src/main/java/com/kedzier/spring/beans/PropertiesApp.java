package com.kedzier.spring.beans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class PropertiesApp {
	
	public static final void main(String[] args) {
		ApplicationContext appCtx = new AnnotationConfigApplicationContext(PropertiesConfig.class);
		PropertiesTest bean = appCtx.getBean(PropertiesTest.class);
		bean.printProperties();
	}

}
