package com.kedzier.spring.beans;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.kedzier.spring.beans")
public class ResourcesConfig {

}
