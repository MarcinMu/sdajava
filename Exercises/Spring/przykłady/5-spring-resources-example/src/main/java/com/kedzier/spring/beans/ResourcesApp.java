package com.kedzier.spring.beans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ResourcesApp {
	
	public static final void main(String[] args) {
		ApplicationContext appCtx = new AnnotationConfigApplicationContext(ResourcesConfig.class);
		ResourcesTest bean = appCtx.getBean(ResourcesTest.class);
		bean.printResource();
	}

}
