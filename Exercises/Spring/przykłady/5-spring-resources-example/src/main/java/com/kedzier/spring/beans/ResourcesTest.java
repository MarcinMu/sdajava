package com.kedzier.spring.beans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class ResourcesTest {

	@Value("classpath:input.txt")
	private Resource fileInput;
	
	@Value("http://onet.pl")
	private Resource websiteInput;

	public void printResource() {
		
		try {
			printInputStream(fileInput.getInputStream());
			printInputStream(websiteInput.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void printInputStream(InputStream inputStream) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		}
	}

}
