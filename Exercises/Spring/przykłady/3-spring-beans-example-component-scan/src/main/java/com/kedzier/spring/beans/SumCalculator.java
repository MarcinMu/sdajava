package com.kedzier.spring.beans;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SumCalculator {
	
	@Autowired
	private InputFileReader fileReader;

	public Integer calculateSum() {
		
		try {
			List<Integer> numbersFromFile = fileReader.readFile();
			return calculateSum(numbersFromFile);
		} catch (IOException e) {
			System.err.println("Error while parsing input file:");
			e.printStackTrace();
			return null;
		}
		
	}

	private Integer calculateSum(List<Integer> numbersFromFile) {
		Integer sum = 0;
		for (Integer number : numbersFromFile) {
			sum+=number;
		}
		return sum;
	}

}
