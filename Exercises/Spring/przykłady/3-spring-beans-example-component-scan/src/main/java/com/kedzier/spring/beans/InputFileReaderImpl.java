package com.kedzier.spring.beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class InputFileReaderImpl implements InputFileReader {

	@Value("${input.file.path}")
	private String filePath;

	@Override
	public List<Integer> readFile() throws IOException {
		
		List<Integer> result = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(new File(filePath)))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				result.add(Integer.valueOf(line));
			}
		}
		return result;
	}

}
