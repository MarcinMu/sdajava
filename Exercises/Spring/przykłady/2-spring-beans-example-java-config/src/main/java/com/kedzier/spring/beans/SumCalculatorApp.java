package com.kedzier.spring.beans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SumCalculatorApp {
	
	public static final void main(String[] args) {
		ApplicationContext appCtx = new AnnotationConfigApplicationContext(SumCalculatorConfig.class);
		SumCalculator bean = appCtx.getBean(SumCalculator.class);
		System.out.println(bean.calculateSum());
	}

}
