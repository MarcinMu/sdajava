package com.kedzier.spring.beans;

import java.io.IOException;
import java.util.List;

public class SumCalculator {
	
	private InputFileReader fileReader;

	public SumCalculator(InputFileReader fileReader) {
		super();
		this.fileReader = fileReader;
	}

	public Integer calculateSum() {
		
		try {
			List<Integer> numbersFromFile = fileReader.readFile();
			return calculateSum(numbersFromFile);
		} catch (IOException e) {
			System.err.println("Error while parsing input file:");
			e.printStackTrace();
			return null;
		}
		
	}

	private Integer calculateSum(List<Integer> numbersFromFile) {
		Integer sum = 0;
		for (Integer number : numbersFromFile) {
			sum+=number;
		}
		return sum;
	}

}
