package com.kedzier.spring.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SumCalculatorConfig {

	@Bean
	public InputFileReader inputFileReader() {
		return new InputFileReaderImpl("numbers.txt");
	}

	@Bean
	public SumCalculator sumCalculator() {
		return new SumCalculator(inputFileReader());
	}

}
