package com.kedzier.spring.hibernate.employee;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDaoImpl extends HibernateDaoSupport implements EmployeeDao {

	@Autowired
	public void setSessionFactoryBean(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	@Override
	public List<Employee> getAll() {
		return getHibernateTemplate().loadAll(Employee.class);
	}

	@Override
	public void update(Employee employee) {
		getHibernateTemplate().saveOrUpdate(employee);
		
	}

}
