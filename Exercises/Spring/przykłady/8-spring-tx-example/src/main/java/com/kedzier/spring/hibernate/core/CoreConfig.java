package com.kedzier.spring.hibernate.core;

import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.h2.server.TcpServer;
import org.h2.tools.Server;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "com.kedzier.spring.hibernate")
public class CoreConfig {
	
	@Value("${db.hibernate.dialect}")
	private String hibernateDialect;
	
	@Value("${db.hibernate.showSql}")
	private String hibernateShowSql;
	
	@Value("${db.hibernate.formatSql}")
	private String hibernateFormatSql;

	@Value("${db.url}")
	private String dbUrl;

	@Value("${db.pass}")
	private String dbPassword;

	@Value("${db.user}")
	private String dbUserName;
	
	@Bean
	public static PropertyPlaceholderConfigurer placeholderConfigurer() {
		PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
		propertyPlaceholderConfigurer.setLocation(new ClassPathResource("app.properties"));
		return propertyPlaceholderConfigurer;
	}
	
	@Bean
	public LocalSessionFactoryBean sessionFactoryBean() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setPackagesToScan("com.kedzier.spring.hibernate");
		sessionFactory.setNamingStrategy(ImprovedNamingStrategy.INSTANCE);
		sessionFactory.setHibernateProperties(hibernateProperties());
		sessionFactory.setDataSource(dataSource());
		return sessionFactory;
	}
	
	@Bean
	public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) {
		PlatformTransactionManager txManager = new HibernateTransactionManager(sessionFactory);
		return txManager;
	}

	@Bean
	DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUrl(dbUrl);
		dataSource.setPassword(dbPassword);
		dataSource.setUsername(dbUserName);
		return dataSource;
	}
	
	private Properties hibernateProperties() {
	    Properties properties = new Properties();
	    properties.put("hibernate.dialect", hibernateDialect);
	    properties.put("hibernate.show_sql", hibernateShowSql);
	    properties.put("hibernate.format_sql", hibernateFormatSql);
	    return properties;
	
	}
	
	/*@Bean
	DataSource embeddedDataSource() {
		return new EmbeddedDatabaseBuilder()
				.setName("dataSource")
				.setType(EmbeddedDatabaseType.H2)
				.addScript("classpath:init-db.sql").build();
	}

	
	@Bean(initMethod = "start", destroyMethod = "stop")
	@DependsOn("h2WebServer")
	Server h2Server() throws SQLException {
		return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9092");
	}
	
	@Bean(initMethod = "start", destroyMethod = "stop")
	Server h2WebServer() throws SQLException {
		return Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082");
	}*/

}
