package com.kedzier.spring.hibernate.employee;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeManagerImpl implements EmployeeManager {

	@Autowired
	private EmployeeDao employeeDao;

	@Transactional
	@Override
	public void increaseAdditionalSalary() {
		List<Employee> employees = employeeDao.getAll();
		for (Employee employee : employees) {
			if (employee.getAdditionalSalary() != null) {
				employee.setAdditionalSalary(employee.getAdditionalSalary() * 10);
			}
		}
	}

}
