package com.kedzier.spring.hibernate.employee;

import java.util.List;

public interface EmployeeDao {
	
	List<Employee> getAll();
	
	void update(Employee employee);

}
