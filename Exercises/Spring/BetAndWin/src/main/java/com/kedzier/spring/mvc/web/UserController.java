package com.kedzier.spring.mvc.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kedzier.spring.mvc.service.User;
import com.kedzier.spring.mvc.service.UserRepository;

@Controller
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ModelAndView usersList() {
		ModelAndView modelAndView = new ModelAndView("displayUsers");
		modelAndView.addObject("users", userRepository.getAllUsers());
		return modelAndView;
	}
	
	@RequestMapping(value = "/user/add", method = RequestMethod.GET)
	public ModelAndView userAddForm() {
		ModelAndView modelAndView = new ModelAndView("createUser");
		modelAndView.addObject("user", new User());
		return modelAndView;
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String addUser(@ModelAttribute User user) {
		userRepository.addUser(user);
		return "redirect:/users";
	}
	
	@RequestMapping(value = "/user/{surname}/delete", method = RequestMethod.POST)
	public String deleteUser(@PathVariable String surname) {
		userRepository.deleteUserBySurname(surname);
		return "redirect:/users";
	}

}
