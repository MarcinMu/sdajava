package com.kedzier.spring.mvc.service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

	// please note that this field is not thread safe - just an example
	private Set<User> users = new HashSet<User>();
	
	public Set<User> getAllUsers() {
		return users;
	}
	
	public void addUser(User user) {
		users.add(user);
	}
	
	public void deleteUserBySurname(String surname) {
		Iterator<User> userIterator = users.iterator();
		while (userIterator.hasNext()) {
			User user = userIterator.next();
			if (surname.equals(user.getSurname())) {
				userIterator.remove();
			}
		}
	}
	
	
}
