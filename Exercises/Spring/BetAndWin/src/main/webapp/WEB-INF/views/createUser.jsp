<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<body>
	<spring:url value="/user" var="addUserUrl" />
	<spring:url value="/users" var="usersListUrl" />

	
	
	<form:form method="POST" action="${addUserUrl}" modelAttribute="user">
             <table>
                <tr>
                    <td><form:label path="name">Name</form:label></td>
                    <td><form:input path="name"/></td>
                </tr>
                 <tr>
                    <td><form:label path="surname">Surname</form:label></td>
                    <td><form:input path="surname"/></td>
                </tr>        
                <tr>
                    <td><input type="submit" value="Add"/></td>
                </tr>
            </table>
        </form:form>

	<button class="btn btn-primary"
		onclick="location.href='${usersListUrl}'">Back to users list</button>



</body>
</html>