function sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX, wylosowanaPozycjaY){
    if( plansza[wylosowanaPozycjaX][wylosowanaPozycjaY] !== 1 && 
        (typeof(plansza[wylosowanaPozycjaX-1]) === 'undefined' ||
            (plansza[wylosowanaPozycjaX-1][wylosowanaPozycjaY] !== 1 && 
            plansza[wylosowanaPozycjaX-1][wylosowanaPozycjaY+1] !== 1 && 
            plansza[wylosowanaPozycjaX-1][wylosowanaPozycjaY-1] === 0)) && 
        plansza[wylosowanaPozycjaX][wylosowanaPozycjaY-1] !== 1 && 
        plansza[wylosowanaPozycjaX][wylosowanaPozycjaY+1] !== 1 && 
        (typeof(plansza[wylosowanaPozycjaX+1]) === 'undefined' ||
            (plansza[wylosowanaPozycjaX+1][wylosowanaPozycjaY] !== 1 && 
            plansza[wylosowanaPozycjaX+1][wylosowanaPozycjaY+1] !== 1 && 
            plansza[wylosowanaPozycjaX+1][wylosowanaPozycjaY-1] !== 1))){
        return true;   
    } else{
        return false;  
    }    
}

function ustawStatki(liczbaStatkow, rozmiarPlanszyX, rozmiarPlanszyY) {
    var plansza = [];
    var zaMaloMiejscaNaTeStatki = 0;
    for (var i = 0; i < rozmiarPlanszyX; i++){
        plansza[i] = [];
        for(var j = 0; j < rozmiarPlanszyY; j++){
            plansza[i][j] = 0;
        }
    }
    
    while (liczbaStatkow>0) {
        var wylosowanaPozycjaX = Math.floor(Math.random()*rozmiarPlanszyX);
        var wylosowanaPozycjaY = Math.floor(Math.random()*rozmiarPlanszyY);
        if (sprawdzCzyMoznaWstawic(plansza, wylosowanaPozycjaX, wylosowanaPozycjaY)) {            
            plansza[wylosowanaPozycjaX][wylosowanaPozycjaY]  = 1;
            liczbaStatkow--;
            zaMaloMiejscaNaTeStatki = 0;
        } else{
            zaMaloMiejscaNaTeStatki++;  
            if (zaMaloMiejscaNaTeStatki>200){
                document.write("</BR>Nie upchne tutaj tych wszystkich statkow ale i tak se graj dalej</BR>")
                break;
            }
        }
    }
    return plansza;
}

function wyswietlPlansze(plansza, rozmiarPlanszyX, rozmiarPlanszyY, cheater){
    var alfabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'];
    document.write (" &nbsp; | ");
    for(var z = 0; z <rozmiarPlanszyX; z++){
        document.write (" ", alfabet[z], " | ");  
    }
    document.write ("</BR>");
    for(var g = 0; g<rozmiarPlanszyX; g++){
        document.write( g+1, " | " );
        for(var h = 0; h<rozmiarPlanszyY; h++){
            if (plansza[g][h] == 1 && cheater == true){
                document.write("  S  | ");
            } else if (plansza[g][h] == 0 || plansza[g][h] == 1 ) {
                document.write("  -  | ");
            } else if (plansza[g][h] == 2) {
                document.write("  Z  | ");
            } else if(plansza[g][h] == 3) {
                document.write("  X  | ");
            }else {
                return false;
            }         
        }
        document.write("</BR>");
    }
    document.write("<HR>");
    return true;
}

function takeCoordX(rozmiarPlanszyX){
    do {
       strzalX = prompt("Podaj Y ");  
       if (strzalX =='god' || strzalX =='exit'){
           return strzalX;
       }
    } while (strzalX < 1 || strzalX > rozmiarPlanszyX || isNaN(strzalX))
    document.write("  ", strzalX);
    return strzalX-1;
}

function takeCoordY(rozmiarPlanszyY){
    var translate = [];
    translate["A"] = 0;
    translate["B"] = 1;
    translate["C"] = 2;
    translate["D"] = 3;
    translate["E"] = 4;
    translate["F"] = 5;
    translate["G"] = 6;
    translate["H"] = 7;
    var y = 0; 
    do {
        y = prompt("Podaj X");
        if (y =='god' || y =='exit'){
           return y;
        }
    } while (typeof(translate[y])==='undefined'||translate[y]>=rozmiarPlanszyY)
    document.write("</BR> Podałeś: ", y);
    return translate[y];

}

function game(planszaStatki,liczbaStatkow,rozmiarPlanszyX, rozmiarPlanszyY){
    var cheater = false;
    var history = [];
    while(liczbaStatkow>0){
        var strzalY = takeCoordY(rozmiarPlanszyY);
        var strzalX = takeCoordX(rozmiarPlanszyX);    
        if (isNaN(strzalX)||isNaN(strzalY)){
            if (strzalX == 'god'|| strzalY == 'god'){
                cheater = true;
                document.write("</BR>")
            } else if (strzalX == 'Exit'|| strzalY == 'Exit'||strzalX == 'exit'|| strzalY == 'exit'){
                document.write("</BR>Poddałeś się </BR>")
                return false; 
            } else {
                document.write("</BR> Podana przez Ciebie wartość nie była poprawną liczbą, popraw się</BR>")
            }
        } else if ( planszaStatki [strzalX][strzalY] == 0 ){
            document.write("    Pudło</BR>");
            planszaStatki[strzalX][strzalY] = 3;
            history.push([strzalX, strzalY, "Pudło"]);
        } else if ( planszaStatki [strzalX][strzalY] == 1 ){
            planszaStatki[strzalX][strzalY] = 2;
            document.write("    Gratuluje Trafiłeś</BR>");
            liczbaStatkow--;
            history.push([strzalX, strzalY, "Trafienie"]);
        } else if ( planszaStatki [strzalX][strzalY] == 2 || planszaStatki [strzalX][strzalY] == 3 ) {
            document.write ("     Już tu strzelałeś mośku. Strzelaj jeszcze raz</BR>");
            history.push([strzalX, strzalY, "Dubel"]);
        } else {
            document.write (" Jakaś kupa");
        }
        
        wyswietlPlansze(planszaStatki, rozmiarPlanszyX, rozmiarPlanszyY, cheater);
        console.log (planszaStatki);
    }
    document.write(coverage( rozmiarPlanszyX, history.length),"%");
    return history;
}
function policzCzas(tartTime, endTime){
    var gameTime = endTime - tartTime;
    var sec = parseInt(gameTime/1000);
    var min = parseInt(sec/60);
    sec = sec % 60;
    sec = sec< 10 ? "0" + sec : sec; 
    var hour = parseInt(min/60);
    min = min % 60; 
    document.write ("</BR>Czas gry: </BR> Godzin: ",hour, "  Minut: ",min, "  Sekund: ", sec);
}

function coverage(dim, shots){
    return Math.round((shots/Math.pow(dim, 2)* 100)*100)/100;
}

function statki() {
    var rozmiarPlanszyX = 3;
    var rozmiarPlanszyY = 3;
    var liczbaStatkow = 1;
    var startTime = new Date();
    var planszaStatki = ustawStatki(liczbaStatkow , rozmiarPlanszyX, rozmiarPlanszyY);
    wyswietlPlansze(planszaStatki, rozmiarPlanszyX, rozmiarPlanszyY, false);
    var wynik = game(planszaStatki, liczbaStatkow,rozmiarPlanszyX, rozmiarPlanszyY);
    if (wynik !== false) {
        document.write("</BR>Gratuluje wygranej gry!</BR>");
        document.write("Przebieg gry był następujący:</BR>", wynik);
    }
    var endTime = new Date();
    policzCzas(startTime, endTime);
}
statki();