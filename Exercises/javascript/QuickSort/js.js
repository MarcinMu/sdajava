
function random(liczbaElementow){
    var tablicaLosowychWartosci = [];
    for(var i=0; i<liczbaElementow; i++){
        tablicaLosowychWartosci[i] = parseInt(Math.random()*100);        
    }
    return tablicaLosowychWartosci;
}

function mergeSortPom(tableToMerge, poczatek, srodek, koniec)
{

    var tabPom = [];
    var i = poczatek;
    var j = srodek+1;
    var k = 0; 
    while (i <= srodek && j <= koniec )
    {
        if( tableToMerge[i]<=tableToMerge[j])
        {
            tabPom[k]=tableToMerge[i];
            i++;
        }
        else{
            tabPom[k]=tableToMerge[j];
            j++;
        }       
        k++;
    }
    
    while(j<=koniec){
        tabPom[k]=tableToMerge[j];
        j++;
        k++;
    }
    
    while(i<=srodek){
        tabPom[k]=tableToMerge[i];
        i++;
        k++;
    }        
    
    if (koniec==7){
        document.write(tabPom);
    } 
}

function mergeSort(tableToMerge, poczatek, koniec)
{
    if (poczatek!=koniec){
        var srodek = parseInt((koniec+poczatek)/2);
        mergeSort(tableToMerge, poczatek, srodek);
        mergeSort(tableToMerge, srodek+1, koniec);  
        mergeSortPom(tableToMerge, poczatek, srodek, koniec) ;
    }
        
}

var table = [1,4,2,4,9,8,7,6];
mergeSort(table, 0, 7);
