function sum(array, callback){
    var sum = 0;
    for (var i = 0; i<array.length; i++){
        sum+=array[i];
    }
    if(typeof(callback)!=='undefined'){
        callback(sum); 
    }
    
    return sum;
}
//sum([1,2,3,4,5], function(a){console.log(a);});
//sum([1,2,3,4,5]);

//---------------------------------------------

function suma(array, callback){
    var sum = 0;
    for (var i = 0 ; i<array.length; i++){
        if (typeof(array[i])!== 'number' && isNaN(array[i])){
            return false;
        }
    }
    if(typeof(callback)!=='undefined'){
        callback(array); 
    }
}

//suma([1,2,3,4,5], function(a){alert(sum(a));});

//-------------------------------------------

function znajdzUjemne(array, callback1, callback2){
    var czySaUjemne = false;
    for (var i = 0; i<array.length; i++){
        if (array[i] < 0){
            if(typeof(callback1)!=='undefined'){
                callback1(array[i]); 
            }
        } else {
            if(typeof(callback2)!=='undefined'){
                callback2(array[i]); 
            }
        }
    }
}
//------------------------------------------------


function znajdzUjemne2(array, callback1, callback2){
    var isNegative = false;
    for (var i = 0; i<array.length; i++){
        if (array[i] < 0){
            isNegative = true;
            break;
        }
    }
    if (isNegative){
        if(typeof(callback1)!=='undefined'){
            callback1(array);       
        }
    } else {
        if(typeof(callback2)!=='undefined'){
            callback2(array); 
        }
    }
}


//znajdzUjemne2([1,-2,3,4,5], function(a){console.log(a);}, function(a){alert(a);} );
//----------------------------------------------

function forEach (array, action){
    for (var i = 0; i<array.length; i++){
        action(array[i]);
    }
}

function sum2(numbers){
    var total = 0;
    forEach(numbers, function(number){
        total+=number;
    });
    return total;
}

var tablica = [1,21,31,41,51];
//console.log(sum2(tablica));
//------------------------------------------------

function podzielnePrzezDwa (tablica){
    forEach(tablica, function(number){
       if(number % 2 === 0){
           console.log (number);
       }
    });
}
//podzielnePrzezDwa(tablica);

//------------------------------------------

function wypiszDuze (tablica, wypiszJe){
    var tablicaPom = [];
    forEach(tablica, function(number){
        if(number > 10){
            tablicaPom.push(number);
        }
    });
    if(typeof(wypiszJe)!=='undefined'){
        wypiszJe(tablicaPom);       
    }
}
//wypiszDuze(tablica, function(pomocniczaTablica){    console.log(pomocniczaTablica);} );

function Pojazd(name) {
        if(typeof(name) !== 'undefined'){
            this.name = name;
        }else{
            this.name ='';
        }
        
    this.speed = parseInt(Math.random()*400);

    this.getSpeed = function() {
        return this.speed;
    };
    this.getName = function() {
        return this.name;  
    };
    this.accelerate = function() {
        this.speed++;
    }
    this.break = function() {
        this.speed = 0;
    }
}
/*
var myCar = new Pojazd('Cinquecento');
var myCar2 = new Pojazd();
myCar.accelerate();
myCar.break();
//var predkosc = myCar.getSpeed();
console.log(myCar2.getSpeed());
console.log(myCar.getName());
*/

function Sorting() {
    this.table ;
    this.setTable = function(tab){
        this.table = tab;
    }
    this.getTable = function(){
        return this.table;
    }
    this.insertSort = function(){
        var i = 0;
        var tmp = 0;
        var j = 0;
        while (this.table[i])
        {
            j = i+1;
            while (this.table[j])
                {
                    if(this.table[i]>this.table[j])
                    {
                        tmp = this.table[i];
                        this.table[i] = this.table[j];
                        this.table[j] = tmp;
                    }
                    j++;
                }
            i++;
        }
        
    }
}
/*

var sortowanko = new Sorting();
sortowanko.setTable([23,12,5,7,89,55]);
sortowanko.insertSort();
console.log(sortowanko.getTable());
*/

function ArraySorter(array) {
    var table; //zmienna prywatna
    if(typeof(array) !=='undefined'){
        table = array; 
    }
    
    this.setArray = function (array) {
        table = array;
    }
    
    this.getArray = function () {
        return table;
    }
    
    function gen(length){
        table = [];
        for(var x = 0; x < length; x++) {
             table[x] = parseInt(Math.random() * 100000);
        }
    }
    
    this.randomArray = function() {
        gen(Math.round(Math.random() * 50 + 50))
    }
    
    function findMax(tab) {
        if (tab.length > 0) {
            var max = tab[0];
            for(var i = 1; i < tab.length; i++) {
                if (max < tab[i]) {
                    max = tab[i];
                }
            }
            return max;
        } else {
            return -1;
        }
    }
    
    this.dom = function() {
        var pom1 = [];
        var pom2 = [];
        var index = 0;

        for(var i = 0; i < table.length; i++) {
            if (pom1.indexOf(table[i]) >= 0) {
                pom2[pom1.indexOf(table[i])]++;
            } else {
                pom1[index] = table[i];
                pom2[index] = 1;
                index++;
            } 
        }
        var max = findMax(pom2);
        return pom1[pom2.indexOf(max)];
    }
    
    this.bubbleSort = function() {
        var INDEX1 = 0;
        var INDEX2 = 0;
        var TMP = 0;
        var CHANGE = false;

        do {
            CHANGE = false;
            INDEX1 = 0;
            INDEX2 = INDEX1 + 1;

            if (INDEX1 < table.length) {
                while (INDEX2 < table.length) {
                    if (table[INDEX1] > table[INDEX2]) {
                        tmp = table[INDEX1];
                        table[INDEX1] = table[INDEX2];
                        table[INDEX2] = tmp;
                        CHANGE = true;
                    }
                    INDEX1++;
                    INDEX2++;
                }
            }
        } while (CHANGE)
    }
    
    this.insertSort = function() {
        var index1 = 0;
        var index2 = 0;
        var tmp = 0;

        while (index1 < table.length) {
            index2 = index1 + 1;
            while (index2 < table.length) {
                if (table[index1] > table[index2]) {
                    tmp = table[index1];
                    table[index1] = table[index2];
                    table[index2] = tmp;
                }
                index2++;
            }
            index1++;
        }
    }
}
/*    
var sorter = new ArraySorter();
//sorter.randomArray();
console.log( sorter.getArray() );
var table2 = sorter.dom();
console.log(table2);
*/
    
function twoTablesOperations (tab1, tab2){
    var table1 = [];
    var table2 = [];
    var sim = null;
    var diff = null;
    if (typeof(tab1) !== 'undefined'){
        table1 = tab1;
    }
    if (typeof(tab2) !== 'undefined'){
        table2 = tab2;
    }    
    
    function similar() {
        var POM = [];
        var INDEX1 = 0;
        var INDEX2;

        while (INDEX1 < table1.length) {
            if (POM.indexOf(table1[INDEX1]) < 0) {
                INDEX2 = 0;
                while (INDEX2 < table2.length) {
                    if (table1[INDEX1] == table2[INDEX2]) {
                        POM.push(table1[INDEX1]);
                        break;
                    }
                    INDEX2++;
                }
            }
            INDEX1++;
        }
        return POM;
    }
    
    this.getSimilar = function(){
        if (sim ===null){
            sim = similar()
        }
        return sim;
    }
    this.getDifferent = function(){
        if (diff === null){
            diff = different()
        }
        return diff;
    }
    
    function different () {
        var simil = similar();
        var pom = [];
        for (var i = 0; i < table1.length; i++) {
            if (simil.indexOf(table1[i]) === -1 && pom.indexOf(table1[i]) === -1) {
                pom.push(table1[i]);
            }
        }
        for (var i = 0; i < table2.length; i++) {
            if (simil.indexOf(table2[i]) === -1 && pom.indexOf(table2[i]) === -1) {
                pom.push(table2[i]);
            }
        }
        return pom;
    }
    
    function gen(length){

        for(var x = 0; x < length; x++) {
             table1[x] = parseInt(Math.random() * 40);
        }
        for(var x = 0; x < length; x++) {
             table2[x] = parseInt(Math.random() * 40);
        }
        sim = null;
        diff = null;
    }
    
    this.setRandomArray = function() {
        gen(Math.round(Math.random() * 20 + 20))
    }
    
    this.setTable1 = function (tab1) {
        if (typeof(tab1) !== 'undefined'){
            table1 = tab1;
            sim = null;
            diff = null;
        }        
    }
    
    this.setTable2 = function (tab2) {
        if (typeof(tab2) !== 'undefined'){
            table2 = tab2;
            sim = null;
            diff = null;
        }        
    }
    
    this.getTable1 = function(){
        return table1;
    }
    this.getTable2 = function(){
        return table2;
    }
}
    
var funWithTables = new twoTablesOperations();
funWithTables.setTable2([2,4,1,33]);
funWithTables.setTable1([12,34,1,33]);
console.log(funWithTables.getTable1());
console.log(funWithTables.getTable2());
console.log(funWithTables.getDifferent());    
console.log(funWithTables.getSimilar());  
funWithTables.setRandomArray();
console.log(funWithTables.getTable1());
console.log(funWithTables.getTable2());
console.log(funWithTables.getDifferent());   
console.log(funWithTables.getDifferent()); 
console.log(funWithTables.getSimilar());
    

