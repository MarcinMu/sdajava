$(document).ready(function(){
    var gameBoard = new Board(10, 15);
    var snake = new Snake();
    $('.start-buton').click( function() {
        gameBoard.drawBoard();
        snake.placeSnake(2,2);
        gameBoard.drawSnake(snake.getSnake());
        gameBoard.giveHimFood();
        var interval = setInterval(function(){ 
            snake.moveSnake(); 
            var result = gameBoard.drawSnake(snake.getSnake());
            if(result ===-1){
                clearInterval(interval);
                alert("Przegrałeś");
            } else if (result === 2) {
               snake.giveSnakefood(); 
            }
        }, 1000); 
    });
    $('.move-right').click( function (){
        snake.changeDirection('right');
    });
    $('.move-up').click( function (){
        snake.changeDirection('up');
    });
    $('.move-left').click( function (){
        snake.changeDirection('left');    
    });
    $('.move-down').click( function (){
        snake.changeDirection('down');
    });
});

function Board(n, m) {
    var dim1 = n;
    var dim2 = m;
    var table = [];
    var oldSnake = null;
    
    for (var i = 0; i < dim1; i++){
        table[i] = [];
        for (var j = 0; j < dim2; j++) {
            table[i][j] = 0; 
        }
    }
    
    this.drawBoard = function () {
        $('.board').remove();
        var uberDiv = $("<div class = 'board'/>");
        for (var i = 0; i < dim1; i++){
            var rowDiv = $("<div class = 'rowDiv'/>");
            for ( var j = 0; j<dim2; j++){
                var div = $("<div class = 'cell'/>");
                if (table[i][j]===1) {
                    div.addClass('snake');
                } else if (table[i][j] === 2) {
                    div.addClass('food');
                }
                rowDiv.append(div);
            }
            uberDiv.append(rowDiv);
        }
        $('body').append(uberDiv);
    }
    
    this.drawSnake = function(params) {
        var result = 0; 
        var param = [params[0][0], params[0][1]];
        if (param[0] >= dim1 || param[1] >= dim2 || param[1] < 0 || param[0] < 0){
            result = -1;
        }
        clearSnake();
        oldSnake = [];
        oldSnake = rewriteTable(params);
        if (table[param[0]][param[1]] === 2 ) {
            this.giveHimFood();
            result = 2;
        }
        for(var i = 0; i < params.length; i++) {
            table[params[i][0]][params[i][1]] = 1; 
        }
        this.drawBoard();
        return result;
    }
    
    clearSnake = function () {
        if(oldSnake !== null) {
            for(var i = 0; i < oldSnake.length; i++) {
                table[oldSnake[i][0]] [oldSnake[i][1]] = 0;   
            }
        }
    }
    
    this.giveHimFood = function() {
        var x = parseInt(Math.random()*dim1);
        var y = parseInt(Math.random()*dim2);
        if (table[x][y] === 0) {
            table[x][y] = 2;
            this.drawBoard();
        } else {
            this.giveHimFood();
        }
    }
};

function Snake() {
    var position = [];
    var isFullStomage = false;
    var direction = 'right';
    this.placeSnake = function(x1, y1){
        position.push([x1, y1]);
    }
    this.giveSnakefood = function(){
        isFullStomage = true;
    }
    this.getSnake = function () {
        return position;
    }
    this.moveSnake = function() {
        var POM = [];
        POM = rewriteTable(position);
        var pomLength = 0;
        switch (direction){
            case 'down': position[0][0]++; break;
            case 'up': position[0][0]--; break;
            case 'left':position[0][1]--; break;
            case 'right': position[0][1]++; break;   
        }
        if(isFullStomage == true) {
            pomLength = POM.length + 1;
            isFullStomage = false;
        } else {
            pomLength = POM.length;
        }
        for (var i = 1; i < pomLength; i++) {
            position[i] = POM[i-1];
        }
    }
    this.changeDirection = function(newDirection) {
        direction = newDirection;
    }
};

function rewriteTable(table) {
    var moveTable = [];
    for (var i = 0; i < table.length; i++) {
        moveTable[i] = [];
        for (var j = 0; j<table[i].length; j++) {
           moveTable[i][j] =  table[i][j];
        }
    }
    return moveTable;
}
