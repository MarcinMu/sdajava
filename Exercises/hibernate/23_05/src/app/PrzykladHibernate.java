package app;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "przyklad_hiber")
public class PrzykladHibernate {
	private String imie;
	private String nazwisko;
	@Id
	private int pesel;
	private double kwota;
	
	public PrzykladHibernate() {
	}
	
	public PrzykladHibernate(String imie, String nazwisko, int pesel, double kwota) {
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.pesel = pesel;
		this.kwota = kwota;
	}
	
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public int getPesel() {
		return pesel;
	}
	public void setPesel(int pesel) {
		this.pesel = pesel;
	}
	public double getKwota() {
		return kwota;
	}
	public void setKwota(double kwota) {
		this.kwota = kwota;
	}
	
	
}
