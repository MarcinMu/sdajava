package app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import java.util.List;
import java.util.Iterator;




public class Hibernate {
	
	private SessionFactory sf;
	
	public static void main(String[] args) {
		Hibernate hb = new Hibernate();
		try {
			hb.sf = new Configuration().configure().buildSessionFactory();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		hb.addRecords(hb);
		hb.listRecords(hb);
 	}

	public void addRecords(Hibernate hb) {
		Session s = hb.sf.openSession();
		Transaction t = s.beginTransaction();

		try {
			PrzykladHibernate ph = new PrzykladHibernate("Marcin", "Kowalski", 312132234, 100);
//			PrzykladHibernate ph2 = new PrzykladHibernate("Marfcin", "Kowdalski", 34313434, 100);
//			PrzykladHibernate ph3 = new PrzykladHibernate("Mafrcin", "Kowfalski", 3432434, 100);
			s.save(ph);
//			s.save(ph2);
//			s.save(ph3);
			t.commit();

			
		} catch (Exception e) {
			if(t != null)
				t.rollback();
			e.printStackTrace();
		} finally {
			s.close();
		}
	}

	public void listRecords(Hibernate hb){
		Session s = hb.sf.openSession();
		List dane = s.createQuery("FROM przyklad_hiber").list();
		for(Iterator i = dane.iterator(); i.hasNext();) {
			PrzykladHibernate ph_temp = (PrzykladHibernate) i.next();
			System.out.println("imie:" + ph_temp.getImie());
		}
		//s.close();
	}
}
