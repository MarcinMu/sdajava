package cars;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class Controller {
	
	public void addRecord(CarsModel myCar){
		Session s = Main.sf.openSession();
		Transaction t = s.beginTransaction();
		try {
			s.save(myCar);
			t.commit();
		} catch (Exception e) {
			if( t != null) t.rollback();
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
 	}
	
	public void addManyRecords(List<CarsModel> myCars) {
		Session s = Main.sf.openSession();
		Transaction t = s.beginTransaction();
		try {
			for (int i = 0; i < myCars.size(); i++) {
				s.save(myCars.get(i));
			}
			t.commit();
		} catch (Exception e) {
			if( t != null) t.rollback();
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
	}
	
	public List readRecord() {
		List result = null;
		Session s = Main.sf.openSession();
		Transaction t = s.beginTransaction();
		try {
			result = s.createQuery("FROM CarsModel").list();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
		return result;
	}
	
	public void modifyRecord(CarsModel myCar) {
		Session s = Main.sf.openSession();
		Transaction t = s.beginTransaction();
		try {
			s.update(myCar);
			t.commit();
		} catch (Exception e) {
			if( t != null) t.rollback();
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
	}
	public void removeRecord(CarsModel myCar) {
		Session s = Main.sf.openSession();
		Transaction t = s.beginTransaction();
		try {
			s.delete(myCar);
			t.commit();
		} catch (Exception e) {
			if( t != null) t.rollback();
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
	}
	public void removeRecord(int id) {
		Session s = Main.sf.openSession();
		Transaction t = null;
		try {
			t = s.beginTransaction();
			CarsModel myCar = (CarsModel) s.get(CarsModel.class, id);
			s.delete(myCar);
			t.commit();
		} catch (Exception e) {
			if( t != null) t.rollback();
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
	}
}
