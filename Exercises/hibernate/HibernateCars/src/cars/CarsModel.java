package cars;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cars")
public class CarsModel {
	@Id
	private int id;
	private String make;
	private String model;
	private int production_year;
	
	public CarsModel(int id, String make, String model, int production_year) {
		this.id = id;
		this.make = make;
		this.model = model;
		this.production_year = production_year;
	}
	public CarsModel() {
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getProduction_year() {
		return production_year;
	}
	public void setProduction_year(int production_year) {
		this.production_year = production_year;
	}
	@Override
	public String toString() {
		return "Our "+ id + " car data are following:  make= " + make + ", model= " + model + ", production_year= " + production_year
				+ " ";
	}
	
	
}
