package cars;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;

import javax.persistence.criteria.CriteriaBuilder.Case;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {

	public static SessionFactory sf;

	public static void main(String[] args) {
		UserInterface myInterface = new UserInterface();
		try {
			java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);
			Main.sf = new Configuration().configure().buildSessionFactory();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		run(myInterface);

		// prinAllReecords(cc);

		// prinAllReecords(cc);
		// cc.removeRecord(myCar2);
		// prinAllReecords(cc);
		// cc.removeRecord(14);
		// prinAllReecords(cc);

		//
		// System.exit(0);
	}

	private static void run(UserInterface myInterface) {

		int chosenOption = 0;
		Scanner myScanner = new Scanner(System.in);
		do {
			Controller cc = new Controller();
			myInterface.showMenu();
			chosenOption = myScanner.nextInt();
			
			switch (chosenOption) {
			case 1:
				prinAllReecords(cc);
				break;
			case 2:
				addRecord(cc, myScanner);
				break;
			case 3:
				List<CarsModel> myCars = new ArrayList<>();
				myCars.add(new CarsModel(11, "Hyundai", "Getz", 2005));
				myCars.add(new CarsModel(13, "Ford", "Mondeo", 2005));
				myCars.add(new CarsModel(14, "Ford", "Mondeo", 2008));
				cc.addManyRecords(myCars);
				break;
			case 4:
				CarsModel myCar2 = new CarsModel(11, "Opel", "Astra", 3009);
				cc.modifyRecord(myCar2);
				break;
			case 5:
				System.out.println("Give me id of record to remove: ");
				int id = myScanner.nextInt();
				cc.removeRecord(id);
				break;
			default:
				prinAllReecords(cc);
				break;
			}
		} while (chosenOption != 6);
		myScanner.close();
		System.exit(0);
	}

	private static void addRecord(Controller cc, Scanner myScanner) {
		int id = 0;
		String make = "";
		String model = "";
		int productionYear = 0;
		System.out.println("Give car id");
		if (myScanner.hasNextLine())
			id = myScanner.nextInt();
		System.out.println("Give me make");
		if (myScanner.hasNextLine())
			make = myScanner.next();
		System.out.println("Give me model");
		if (myScanner.hasNextLine())
			model = myScanner.next();
		System.out.println("Give me production year");
		if(myScanner.hasNextLine())
			productionYear = myScanner.nextInt();
		cc.addRecord(new CarsModel(id, make, model, productionYear));
	}

	private static void prinAllReecords(Controller cc) {
		List readRecords = cc.readRecord();
		for (Iterator i = readRecords.iterator(); i.hasNext();) {
			CarsModel ph_temp = (CarsModel) i.next();
			System.out.println(ph_temp.toString());
		}
	}

}
