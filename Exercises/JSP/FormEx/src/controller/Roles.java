package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Roles
 */
@WebServlet("/Roles")
public class Roles extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Roles() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		HttpSession session = request.getSession();
		if(action.equalsIgnoreCase("delete")) {
			int id = Integer.parseInt(request.getParameter("id")) ;
			new RolesController(id).deleteRole();
			response.sendRedirect("RolesManaging.jsp");
		} else if (action.equals("edit")) {
			session.setAttribute("editId", request.getParameter("id") );
			response.sendRedirect("editRole.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		HttpSession ses = request.getSession();
		if(action.equalsIgnoreCase("edit")){
			int id = Integer.parseInt( (String) request.getParameter("roleId")) ;
			new RolesController(id, (String)  request.getParameter("roleName")).editRole();
			System.out.println(id);
			System.out.println((String)  ses.getAttribute("roleName"));
			ses.removeAttribute("editId");
			response.sendRedirect("RolesManaging.jsp");
		} else if(action.equalsIgnoreCase("add") ) {
			String roleName = (String) request.getParameter("roleName");
			RolesController rc = new RolesController(roleName);
			rc.addRole();
			response.sendRedirect("RolesManaging.jsp");
		}
	}

}
