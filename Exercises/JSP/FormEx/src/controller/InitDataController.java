package controller;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.*;

public class InitDataController {
	
	public InitDataController() {
		this.createAccounts();
		this.createRoles();
		this.createTasks();
	}

	public void createAccounts() {
		Session s = AppController.getSession().openSession();
		Transaction t = s.beginTransaction();
		UsersModel um = new UsersModel("admin","admin", "Marcin", "Musia�", 0);
		UsersModel um2 = new UsersModel("wieslaw","qwe123", "Wies�aw", "Kowalski", 1);
		try {
			s.save(um);
			s.save(um2);
			t.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			s.close();
		}
	}
	
	public void createRoles() {
		Session s = AppController.getSession().openSession();
		Transaction t = s.beginTransaction();
		RolesModel adminRole = new RolesModel("admin");
		RolesModel userRole = new RolesModel("user");
		RolesController rc = new RolesController("admin");
		RolesController rc2 = new RolesController("user");
		try {
			s.save(adminRole);
			s.save(userRole);
			t.commit();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void createTasks(){
		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();
			TasksModel tm1 = new TasksModel(0, "admin", "title", "content");
			TasksModel tm2 = new TasksModel(0, "admin", "title2", "content2");
			TasksModel tm3 = new TasksModel(0, "wieslaw", "title3", "content3");
			TasksModel tm4 = new TasksModel(0, "wieslaw", "title4", "content4");
			s.save(tm1);
			s.save(tm2);
			s.save(tm3);
			s.save(tm4);
			
			t.commit();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
