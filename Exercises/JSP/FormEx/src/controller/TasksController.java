package controller;


import org.hibernate.Session;
import org.hibernate.Transaction;

import model.TasksModel;

public class TasksController {
	private int id;
	private String login;
	private String title;
	private String content;
	
	public TasksController() {
		
	}

	public TasksController(int id, String login, String title, String content) {
		super();
		this.id = id;
		this.login = login;
		this.title = title;
		this.content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public void addTask(int id, String login, String title, String content) {
		
		TasksModel newtask = new TasksModel(id, login, title, content);
		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();
			s.save(newtask);
			t.commit();
			s.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void removeTask(int taskId) {
		try {
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();
			TasksModel taskToBeRemoved = s.get(TasksModel.class, taskId);
			s.delete(taskToBeRemoved);
			t.commit();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
	}

	
}
