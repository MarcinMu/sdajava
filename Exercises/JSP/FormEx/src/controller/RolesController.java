package controller;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.RolesModel;

public class RolesController {
	private RolesModel rolesModel;
	
	public RolesController(String name) {
		super();
		this.rolesModel = new RolesModel(name);
	}

	public RolesController(int id, String name) {
		super();
		this.rolesModel = new RolesModel(id, name);
	}
	
	public RolesController() {
	}
	
	public RolesController(int id) {
		this.rolesModel = new RolesModel(id);
	}


	public String getRoleName() {
		return rolesModel.getRoleName();
	}

	public void setRoleName(String roleName) {
		this.rolesModel.setRoleName(roleName);
	}
	
	public int getRoleId(){
		return rolesModel.getId();
	}

	public RolesModel getRolesModel() {
		return rolesModel;
	}

	public void setRolesModel(RolesModel rolesModel) {
		this.rolesModel = rolesModel;
	}
	
	public List<RolesModel> getAllRoles() {
		Session s = AppController.getSession().openSession();
		Transaction t = s.beginTransaction();
		List rolesList = null;
		try {
			rolesList = s.createQuery("FROM RolesModel").list();
			s.close();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return rolesList;
	}
	
	public void addRole() {
		Session s = AppController.getSession().openSession();
		Transaction t = s.beginTransaction();
		try {
			s.save(this.rolesModel);
			t.commit();
		} catch(Exception e) {
			System.out.println(e.getMessage());
			if(t != null) {
				t.rollback();	
			}
		} finally {
			s.close();
		}
	}
	
	public void deleteRole() {
		Session s = AppController.getSession().openSession();
		Transaction t = s.beginTransaction();
		try {
			s.delete(s.get(RolesModel.class, this.rolesModel.getId()));
			t.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			if(t != null) {
				t.rollback();	
			}
		} finally {
			s.close();
		}
	}
	public void editRole() {
		Session s = AppController.getSession().openSession();
		Transaction t = s.beginTransaction();
		try {
			s.update(this.rolesModel);
			t.commit();
			s.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
