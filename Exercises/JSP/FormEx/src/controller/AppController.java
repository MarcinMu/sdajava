package controller;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class AppController {
	public static SessionFactory sf;
	public static boolean isInitiated = false;
	public static void setSession() {
	    if (! isInitiated ) {
			try {
				sf = new Configuration().configure().buildSessionFactory();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} 
	    	new InitDataController();
	    	isInitiated = true;
	    }
	}
	
	public static SessionFactory getSession(){
		return sf;
	}
}
