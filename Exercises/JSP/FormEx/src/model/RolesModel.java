package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "userRoles")
public class RolesModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String roleName;
	
	public RolesModel() {
		super();
	}

	public RolesModel(String roleName) {
		super();
		this.roleName = roleName;
	}
	
	public RolesModel(int id, String roleName) {
		super();
		this.id = id;
		this.roleName = roleName;
	}
	
	public RolesModel(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
