<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page
	import="java.util.*,model.*,controller.*, org.hibernate.*,org.hibernate.cfg.*, java.util.Iterator"%>
	<%boolean isAdmin = session.getAttribute("role").toString().equalsIgnoreCase("0"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel = "stylesheet" href = "style.css" type = "text/css">
</head>
<body>
	<div class = "menu">
		<ul>
		<li> <a href="./Zaloguj?akcja=wyloguj"> Log Out  </a> </li>
		<li> <a href ="./taskManaging.jsp" >Manage your tasks </a> </li>
		<% if(isAdmin) { %>
			<li> <a href = "./Users.jsp" > Manage users </a>  </li>
			<li> <a href = "./RolesManaging.jsp" > Manage Roles </a> </li>
		<% } %>
		</ul>	
	</div>
	<div class = "usersManaging">
		<h3>Users data:</h3>
		<table>
			<tr>
				<td>Id</td>
				<td>Login</td>
				<td>Role</td>
				<td>First Name </td>
				<td>Surname</td>
				
				<%
				if(isAdmin) {
					%>
					<td>Password</td>
					<td>Action</td>
					<%
				}
				%>
			</tr>
			<%
				Session s = AppController.getSession().openSession();
				Transaction t = s.beginTransaction();
				int k = 0;
				try {
					List data = s.createQuery("FROM UsersModel").list();
					for (Iterator i = data.iterator(); i.hasNext();) {
						UsersModel u = (UsersModel) i.next();
						k++;
					%>
					<tr>
						<td>
							<%
								out.println(k) ;
							%>
						</td>
						<td>
							<%
								out.println(u.getLogin());
							%>
						</td>
						<td>
							<%
								int role = u.getRole();
								if(role == 0) out.println("Admin");
								if(role == 1) out.println("User");
							%>
						</td>
						<td>
							<%
								out.println(u.getFirstName()) ;
							%>
						</td>
						<td>
							<%
								out.println(u.getSurname()) ;
							%>
						</td>
						<%
						if(isAdmin) {
						%>
						<td>
							<%
								out.println(u.getPassword()) ;
							%>
						</td>
						<td>
							<a href ="./edition.jsp?login=<% out.println(u.getLogin());%>" >Edit</a>
						</td>
						<td>
							<a href ="./Zaloguj?akcja=remove&login=<% out.println(u.getLogin());%>" >Remove</a>
						</td>
						<% } %>
					</tr>
					<%
				}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				} finally {
					s.close();
				}
			if(isAdmin) {
			%>
			</table>
			<br>
			Add new user: 
			<br>
			<form action = "./Zaloguj?akcja=dodaj" method = "POST">
				Login:	<input type = "text" name = "login"/><br>
				Hasło:	<input type = "text" name = "password"/><br>
				First Name: <input type = "text" name = "firstName"/><br>
				Surname: <input type = "text" name = "surname"/> <br>
				Role: <select name = "role"><option value = "0">Admin</option><option value = "1">User</option></select>
				<br>
				<input type = "submit" value = "Add" class = "button"/> <br>
			</form>
		<% } %>
	</div>
	
</body>
</html>