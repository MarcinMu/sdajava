<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% HttpSession ses = request.getSession(); %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Role edition</title>
</head>
<body>
	<h2>Edit role</h2>
	<form action="./Roles?action=edit" method = "POST">
		<input type = "text" name = "roleName"/>
		<input type = "submit" value = "Save"/>
		<input type = "hidden" name = "roleId" value = "${sessionScope.editId}" />
	</form>

</body>
</html>