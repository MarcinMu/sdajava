package controllerTest;

import org.junit.Assert;
import org.junit.Test;
import controller.*;

public class AppControllerTest {
	
	@Test
	public void setSessionShouldChangeIsInitiatedToTrue(){
		//given
		AppController.isInitiated = false;
		//when
		AppController.setSession();
		//then
		Assert.assertEquals(true, AppController.isInitiated);
	}
}
