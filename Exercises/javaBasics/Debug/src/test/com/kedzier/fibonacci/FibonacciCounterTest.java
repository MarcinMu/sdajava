package com.kedzier.fibonacci;

import org.junit.Assert;
import org.junit.Test;

public class FibonacciCounterTest {
	
	@Test
	public void shouldReturnOneForFirstElement() {
		
		// given
		FibonacciCounter tested = new FibonacciCounter();
		
		// when
		long result = tested.fibonacci(1);
		
		// then
		Assert.assertEquals(1, result);
	}
	
	@Test
	public void shouldReurnoneForSecondElement() {
		
		// given
		FibonacciCounter tested = new FibonacciCounter();
		
		// when
		long result = tested.fibonacci(2);
		
		// then
		Assert.assertEquals(1, result);
		
	}
	
	@Test
	public void shouldReurnoneForTenthElement() {
		
		// given
		FibonacciCounter tested = new FibonacciCounter();
		
		// when
		long result = tested.fibonacci(10);
		
		// then
		Assert.assertEquals(55, result);
		
	}
	
}
