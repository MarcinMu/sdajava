package com.kedzier.scrable;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class DictionaryTest {
	
	@Test
	public void shouldReturnEmptySetForEmptyFile() {
		// given
		Dictionary dictionary = new Dictionary("emptyTestDict.txt");
		
		// when
		Set<Word> words = dictionary.getWords();
		
		// then
		Assert.assertTrue(words.isEmpty());
	}
	
	@Test
	public void shouldReturnNotEmptySetForNotEmptyFile() {
		// given
		Dictionary dictionary = new Dictionary("testDict.txt");
		
		// when
		Set<Word> words = dictionary.getWords();
		
		// then
		Assert.assertEquals(words.size(), 5);
		Assert.assertTrue(words.contains(new Word("testowy")));
		Assert.assertTrue(words.contains(new Word("napis")));
		Assert.assertTrue(words.contains(new Word("i")));
		Assert.assertTrue(words.contains(new Word("jeszcze")));
		Assert.assertTrue(words.contains(new Word("jeden")));
	}

}
