package com.kedzier.scrable;

import java.util.Set;
import org.junit.Assert;
import org.junit.Test;

public class PermutationsGeneratorTest {

	@Test
	public void shouldReturnSingleEementSetForOneLengthString() {
		// given
		String singleCharString = "a";
		PermutationsGenerator generator = new PermutationsGenerator(singleCharString);

		// when
		Set<Word> results = generator.generatePermutations();

		// then
		Assert.assertEquals(results.size(), 1);
		Assert.assertEquals(results.iterator().next().getChars(), singleCharString);
	}

	@Test
	public void shouldReturnFourPermutationsForTwoCharsString() {
		// given
		String singleCharString = "ab";
		PermutationsGenerator generator = new PermutationsGenerator(singleCharString);

		// when
		Set<Word> results = generator.generatePermutations();

		// then
		Assert.assertEquals(4, results.size());
		Assert.assertTrue(results.contains(new Word("ab")));
		Assert.assertTrue(results.contains(new Word("ba")));
		Assert.assertTrue(results.contains(new Word("b")));
		Assert.assertTrue(results.contains(new Word("a")));
	}

}
