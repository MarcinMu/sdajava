package com.kedzier.selectionsort;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.kedzier.selectionsort.SelectionSort;


public class SelectionSorterTest {

	@Test
	public void shouldSortNotSortedList() {
		// given
		SelectionSort sorter = new SelectionSort(Arrays.asList(1, 4, 5, 2, 3));

		// when
		sorter.sort();
		
		// then
		List<Integer> result = sorter.getNumbers();
		Assert.assertEquals((int) 1, (int) result.get(0));
		Assert.assertEquals((int) 2, (int) result.get(1));
		Assert.assertEquals((int) 3, (int) result.get(2));
		Assert.assertEquals((int) 4, (int) result.get(3));
		Assert.assertEquals((int) 5, (int) result.get(4));
	}
	
	@Test
	public void shouldSortReversedList() {
		// given
		SelectionSort sorter = new SelectionSort(Arrays.asList(5, 4, 3, 2, 1));

		// when
		sorter.sort();
		
		// then
		List<Integer> result = sorter.getNumbers();
		Assert.assertEquals((int) 1, (int) result.get(0));
		Assert.assertEquals((int) 2, (int) result.get(1));
		Assert.assertEquals((int) 3, (int) result.get(2));
		Assert.assertEquals((int) 4, (int) result.get(3));
		Assert.assertEquals((int) 5, (int) result.get(4));
	}
	
	@Test
	public void shouldNotChangeList() {
		// given
		SelectionSort sorter = new SelectionSort(Arrays.asList(1, 2, 3, 4, 5));

		// when
		sorter.sort();
		
		// then
		List<Integer> result = sorter.getNumbers();
		Assert.assertEquals((int) 1, (int) result.get(0));
		Assert.assertEquals((int) 2, (int) result.get(1));
		Assert.assertEquals((int) 3, (int) result.get(2));
		Assert.assertEquals((int) 4, (int) result.get(3));
		Assert.assertEquals((int) 5, (int) result.get(4));
		
	}

}
