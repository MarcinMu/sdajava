package com.kedzier.selectionsort;

import java.util.List;

/**
 * Klasa sortuj�ca liczby z u�yciem algorytmu selection sort
 * @author kedzier
 *
 */
public class SelectionSort {
	
	private final List<Integer> numbers;
	
	public SelectionSort(List<Integer> numbers) {
		this.numbers = numbers;
	}
	
	public List<Integer> getNumbers() {
		return numbers;
	}

	public void sort() {
		for (int i = 0; i < numbers.size(); i++) {
			moveMinToBegining(numbers.subList(i, numbers.size()));
		}
	}
	
	private void moveMinToBegining(List<Integer> sublist) {
		int min = sublist.get(0);
		int minIndex = 0;
		for (int i = 1; i < sublist.size(); i++) {
			Integer number = sublist.get(i);
			if (number < min) {
				min = number;
				minIndex = i;
			}
		}
		replace(sublist, minIndex, 0);
	}
	
	private void replace(List<Integer> sublist, int indexFrom, int indexTo) {
		Integer temp = sublist.get(indexTo);
		sublist.set(indexTo, sublist.get(indexFrom));
		sublist.set(indexFrom, temp);
	}
	
}
