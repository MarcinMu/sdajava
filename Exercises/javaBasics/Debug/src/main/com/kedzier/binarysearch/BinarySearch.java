package com.kedzier.binarysearch;

import java.util.Arrays;
import java.util.List;

public class BinarySearch {

	public static void main(String[] args) {
		System.out.println(binarySearch(Arrays.asList(2,5,8,10,12), 7));
	}
	
	private static Integer binarySearch(List<Integer> source, Integer numberToFind) {
		
		int sourceSize = source.size();
		int middleIndex = (int) Math.ceil(sourceSize / 2);
		Integer valueInMiddle = source.get(middleIndex);
		if (valueInMiddle == numberToFind) {
			return valueInMiddle;
		} else if (valueInMiddle < numberToFind) {
			return binarySearch(source.subList(middleIndex + 1, sourceSize), numberToFind);
		} else if (valueInMiddle > numberToFind) {
			return binarySearch(source.subList(0, middleIndex - 1), numberToFind);
		}
		
		return 0;
	}

}
