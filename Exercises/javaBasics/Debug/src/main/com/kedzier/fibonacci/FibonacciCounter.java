package com.kedzier.fibonacci;

public class FibonacciCounter {
	
	public long fibonacci(long n) {
		validateInput(n);
		if (n == 1 || n == 2) {
			return 1;
		} else {
			return fibonacci(n - 2) + fibonacci(n - 1);
		}
	}

	private void validateInput(long n) {
		if (n < 1) {
			throw new IllegalArgumentException("Only natural numbers are supported! " + n + " is not a natural number.");
		}
		
	}
}
