package com.kedzier.scrable;

import java.util.Set;
import java.util.TreeSet;

public class PermutationsGenerator {

	private final String input;

	public PermutationsGenerator(String input) {
		this.input = input;
	}

	public Set<Word> generatePermutations() {
		return generatePermutations("", input);
	}

	private Set<Word> generatePermutations(String prefix, String str) {
		Set<Word> results = new TreeSet<>();
		int n = str.length();
		
		if (!"".equals(prefix)) {
			results.add(new Word(prefix));
		}
		
		for (int i = 0; i < n; i++) {
			results.addAll(generatePermutations(prefix + str.charAt(i), str.substring(0, i) + str.substring(i + 1, n)));
		}
		
		return results;
	}

}
