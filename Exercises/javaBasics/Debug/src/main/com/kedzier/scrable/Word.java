package com.kedzier.scrable;

public class Word implements Comparable<Word> {
	
	private String chars;
	
	private int value = 0;

	public Word(String chars) {
		this.chars = chars;
	}

	public final String getChars() {
		return chars;
	}

	public final int getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return chars;
	}

	@Override
	public int compareTo(Word other) {
		if (other == null) {
			return 1;
		}
		int lenghtComparison = new Integer(chars.length()).compareTo(other.getChars().length());
		return lenghtComparison;
	}
	
	

}
