package com.kedzier.scrable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Dictionary {
	
	private BufferedReader reader;
	
	private Set<Word> words;

	public Dictionary(String dictionaryPath) {
		try {
			this.reader = new BufferedReader(new FileReader(dictionaryPath));
			parseFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Set<Word> getWords() {
		return words;
	}
	
	public boolean containsWord(Word word) {
		return words.contains(word);
	}

	private void parseFile() throws IOException {
		Set<Word> words = new HashSet<>();
		String line = null;
		while ((line = reader.readLine()) != null) {
			String[] wordsFromLine = line.split(", ");
			words.addAll(stringArrayToWords(wordsFromLine));
		}
	}

	private Set<Word> stringArrayToWords(String[] strings) {
		Set<Word> words = new HashSet<>();
		for (String string : strings) {
			words.add(new Word(string));
		}
		return words;
	}
	
	

}
