package com.kedzier.scrable;

public class WordsFinder {

	public static void main(String[] args) {
		
		String input = "ialinec";
		PermutationsGenerator permutationsGenerator = new PermutationsGenerator(input);
		Dictionary dictionary = new Dictionary("words.txt");
		
		for (Word possibleWord : permutationsGenerator.generatePermutations()) {
			if (dictionary.containsWord(possibleWord)) {
				System.out.println(possibleWord);
			}
		}
	}	

}
