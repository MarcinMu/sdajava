
public class Rhombus extends Quadrangle {
	private double h; 
	public Rhombus(double a) {
		super();
		this.b = 0;
		this.a = a;
		this.h = 0;
	}
	public Rhombus(double a, double h) {
		super();
		this.b = 0;
		this.a = a;
		this.h = h;
	}

	@Override
	public double calculateSurface() {
		// TODO Auto-generated method stub
		return this.a * this.h;
	}
	
}
