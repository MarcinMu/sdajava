
public class Rectangle extends Quadrangle {
	public Rectangle(double a, double b) {
		super();
		this.a = a;
		this.b = b;
	}
	
	@Override
	public double calculateSurface() {
		
		return this.a * this.b;
	}
	
}
