
import java.awt.image.ShortLookupTable;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.stylesheets.LinkStyle;

public class Main {

	public static void main(String[] args) throws IOException {
		// playWithFigures();
		// playWithCalculator();
		// List<Integer> list = new
		// ArrayList<Integer>(Arrays.asList(2,1,2,4,5,7,1,5,9,15, 20 , 16, 15));
		// System.out.println("list without 5 and its multiplicity: " +
		// playWithIterator(list));
		// lookAtMyCompany();
		// //switchTest();
		// //readFromFile();
		// makeTableAndSortIt();
		useEquals();
	}

	private static void useEquals() {
		String first = "text";

		String second = new String("text");

		System.out.println(first == "text");

		System.out.println(second == "text");

		System.out.println(first.equals("text"));

		System.out.println(second.equals("text"));

		System.out.println(first == second);

		System.out.println(first.equals(second));

	}

	private static void makeTableAndSortIt() {
		int[] table = new int[100];
		for (int i = 0; i < table.length; i++) {
			table[i] = (int) (Math.random() * 100);
		}
		System.out.println("My unsorted list: " + Arrays.toString(table));

		boolean isSwitched = true;
		while (isSwitched) {
			isSwitched = false;
			for (int i = 0; i < table.length - 1; i++) {
				if (table[i] > table[i + 1]) {
					int temp = table[i + 1];
					table[i + 1] = table[i];
					table[i] = temp;
					isSwitched = true;
				}
			}
		}
		System.out.println("My sorted list: " + Arrays.toString(table));

	}

	public static void readFromFile() throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader("myTextFile.txt"));
		String lineFromFile = reader.readLine();
		while (lineFromFile != null) {
			int numberInLine = Integer.parseInt(lineFromFile);
			System.out.println(lineFromFile);
			System.out.println(numberToString(numberInLine));
			lineFromFile = reader.readLine();
		}
	}

	public static String numberToString(int number) {
		List<String> tableWithNumbers = Arrays.asList("Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven",
				"Eight", "Nine");
		number %= 10;
		return tableWithNumbers.get(number);
	}

	public static void switchTest() {
		String color = "red";
		switch (color) {
		case "red":
			System.out.println("czerwony");
		case "blue":
			System.out.println("niebieski");
		default:
			System.out.println("inny");
		}
	}

	public static void lookAtMyCompany() {
		Company myCompany = new Company();
		System.out.println("My branches: ");
		for (Branch myBranch : myCompany) {
			System.out.println(myBranch);
		}
		System.out.println("My branches printed with while loop: ");
		Iterator<Branch> iterator = myCompany.iterator();
		while (iterator.hasNext()) {
			Branch next = iterator.next();
			System.out.println(next);
		}
	}

	public static List<Integer> playWithIterator(List<Integer> list) {
		Integer number = list.get(0);
		for (Iterator<Integer> i = list.iterator(); i.hasNext(); number = i.next()) {
			if (number % 5 == 0) {
				i.remove();
			}
			if (number % 2 == 0) {
				System.out.println(number);
			}
		}
		return list;
	}

	public static void playWithCalculator() {
		System.out.println(Calculator.add(2, 3));
		System.out.println(Calculator.add((float) 2.4, (float) 3.5));
		System.out.println(Calculator.add(true, false));
	}

	public static void playWithFigures() {
		Figure[] myFigures = new Figure[3];
		myFigures[0] = new Square(4);
		myFigures[1] = new Circle(3);
		myFigures[2] = new Rectangle(3, 5);

		pringAllFiguresSurface(myFigures);
	}

	public static void pringAllFiguresSurface(Figure[] myFigures) {
		String result = "";
		for (int i = 0; i < myFigures.length; i++) {
			result += i + 1 + " figure surface: " + myFigures[i].calculateSurface() + "   ,   ";
		}
		System.out.println(result);

	}
}
