
public class Circle implements Figure {
	
	double PI = 3.14;
	private double radius;
	public Circle(double r) {
		this.radius = r;
	}
	@Override
	public double calculateSurface() {
		
		return PI * Math.pow(this.radius, 2); 
	}

}
