

public interface Figure {
	public double calculateSurface();
}
