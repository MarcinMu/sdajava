
public class Branch {
	private String branchName ;

	public Branch(String branchName) {
		super();
		this.branchName = branchName;
	}

	
	
	@Override
	public String toString() {
		return "Branch [branchName=" + this.branchName + "]";
	}



	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
}
