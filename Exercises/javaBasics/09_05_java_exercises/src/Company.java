import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Company implements Iterable<Branch> {
	private List<Branch> branchesList ;

	public Company() {
		super();
		this.branchesList = new ArrayList<Branch>(Arrays.asList(new Branch("Rzeszowski"), new Branch("Radomski"), new Branch( "Wroc�awki"))) ;
	}

	@Override
	public Iterator<Branch> iterator() {
		return branchesList.iterator();
	}
	
	
}
