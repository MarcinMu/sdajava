import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class EmployeeTest {
	
	@Test
	public void compareTwoEmployeesByLastName () {
		// given
		Employee firstEmployee = new Employee("Marian", "Kowalski", 33, 20);
		Employee secondEmployee = new Employee("Wac�aw", "Nowak", 33, 20);
		// when
		int result = firstEmployee.compareTo(secondEmployee);
		// then
		Assert.assertEquals(-3, result);
	}
	
	@Test
	public void sortEmployees () {
		//given
		Employee firstEmployee = new Employee("Marian", "Kowalski", 33, 20);
		Employee secondEmployee = new Employee("Wac�aw", "Nowak", 33, 20);
		Employee thirdEmployee = new Employee("Marian", "Niemczyk", 33, 20);
		Employee fourthEmployee = new Employee("Wac�aw", "Gromadzki", 33, 20);
		List<Employee> employeeList = Arrays.asList(firstEmployee, secondEmployee, thirdEmployee, fourthEmployee);
		List<Employee> sortedList = Arrays.asList(fourthEmployee, firstEmployee, thirdEmployee, secondEmployee);
		// when
		Collections.sort(employeeList);
		// then
		assertThat(employeeList, is(sortedList));
	}
}
