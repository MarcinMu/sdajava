import org.junit.Assert;
import org.junit.Test;


public class PointTest {
	
	@Test
	public void twoEqualObjectShouldBeEqualTest(){
		// Given
		Point firstPoint = new Point(4, 5);
		Point secondPoint = new Point(4, 5);
		// When
		boolean isEqual = firstPoint.equals(secondPoint);
		// Then
		Assert.assertEquals(true, isEqual);
	}
	
	@Test
	public void notEqualObjectShouldReturnFalse() {
		// Given
		Point firstPoint = new Point(1, 4);
		Point secondPoint = new Point(2, 5);
		// When
		boolean isEqual = firstPoint.equals(secondPoint);
		// Then
		Assert.assertEquals(false, isEqual);
	}
}
