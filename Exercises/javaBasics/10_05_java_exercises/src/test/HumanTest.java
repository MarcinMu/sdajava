import org.junit.Assert;
import org.junit.Test;

public class HumanTest {
	
	@Test
	public void personShouldHasFirstName(){
		// given
		Human person = new Human("Marian", "Duda");
		// when
		boolean isFirstName = ( person.getFirstName() != null);
		// then
		Assert.assertEquals(true, isFirstName);

	}
	@Test
	public void personShouldHasProperFirstName(){
		// given
		Human person = new Human("Marian", "Duda");
		// when
		String firstName = person.getFirstName();
		// then
		Assert.assertEquals("Marian", firstName);

	}
}
