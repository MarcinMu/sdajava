import java.util.Comparator;

public class AgeComparator implements Comparator<Employee> {

	@Override
	public int compare(Employee first, Employee second) {
		return first.getAge() - second.getAge();
	}

}
