import java.util.Arrays;

import org.junit.internal.Throwables;

public class Exceptions {
	
	public static void printString(String text) {
		System.out.println(text.length());
	}
	
	public static void playWithExceptions() throws Exception {
		try {
			System.out.println(Arrays.toString("n.a.pis".split(".")));
			printString(null);
		} catch (Exception e) {
			e.printStackTrace();
			e.fillInStackTrace();
			//throw e;
			throw new Exception("Wyj�tek : ", e);
		} 
		
	}
}
