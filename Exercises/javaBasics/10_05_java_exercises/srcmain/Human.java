
public class Human {
	private String firstName;
	private String lastName;
	private long pesel;
	
	public Human(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName =  lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getPesel() {
		return pesel;
	}

	public void setPesel(long pesel) {
		this.pesel = pesel;
	}
	
}
