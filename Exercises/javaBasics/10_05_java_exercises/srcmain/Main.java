import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {

	public static void main(String[] args) throws Exception {
		playWithStrings();
	}
	


	private static void playWithStrings() {
		String [] strings = {"adf", "fsdf", "fasdf", "dfasd", "drtttt"};
		System.out.println(buildBigString(strings));
		
	}
	
	private static StringBuilder buildBigString(String[] a ) {
		StringBuilder b = new StringBuilder();
		
		for (int i = 0; i < a.length; i++) {
			if (i > 0) b.append(",");
			b.append(a[i].substring(0, 3).toUpperCase());
			
		}
		
		return b;
	}


	public static void playWithEnums() {
		System.out.println(SignalizationColors.RED);
		System.out.println(SignalizationColors.GREEN);
		System.out.println(SignalizationColors.YELLOW);
	}
	
	public static void playWithEmployeesComparator(){
		Employee firstEmployee = new Employee("Marian", "Kowalski", 11, 20);
		Employee secondEmployee = new Employee("Wac�aw", "Nowak", 33, 20);
		Employee thirdEmployee = new Employee("Marian", "Niemczyk", 2, 20);
		Employee fourthEmployee = new Employee("Wac�aw", "Gromadzki", 99, 20);
		List<Employee> employeeList = Arrays.asList(firstEmployee, secondEmployee, thirdEmployee, fourthEmployee);
		
		System.out.println(employeeList);
		Collections.sort(employeeList, new AgeComparator());
		//Collections.sort(employeeList, );
		System.out.println(employeeList);
	}
}
