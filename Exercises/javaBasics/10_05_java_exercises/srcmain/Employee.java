
public class Employee implements Comparable<Employee> {
	private String firstName;
	private String lastName;
	private int age;
	private int seniority;

	public Employee(String firstName, String lastName, int age, int seniority) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.seniority = seniority;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSeniority() {
		return seniority;
	}

	public void setSeniority(int seniority) {
		this.seniority = seniority;
	}

	@Override
	public int compareTo(Employee o) {
		if (this.lastName.compareTo(o.lastName)!= 0) {
			return this.lastName.compareTo(o.lastName);
		} else {
			return this.firstName.compareTo(o.firstName);
		}
	} 
	

	@Override
	public String toString() {
		return "Employee [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + ", seniority="
				+ seniority + "]";
	}
	
}
