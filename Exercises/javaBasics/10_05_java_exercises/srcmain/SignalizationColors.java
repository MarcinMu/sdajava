
public enum SignalizationColors {
	
	RED ("red", 255, 0, 0), 
	GREEN ("green", 0, 255, 0),
	YELLOW ("yellow", 0, 255, 0);
	
	private String name;
	private int r;
	private int g;
	private int b;
	
	private SignalizationColors(String name, int r, int g, int b) {
		this.name = name;
		this.r = r;
		this.g = g;
		this.b = b;
	}
	
	@Override
	public String toString() {
		return "(" + this.r + "," + this.g+"," + this.b + ")";
	}
	
}
