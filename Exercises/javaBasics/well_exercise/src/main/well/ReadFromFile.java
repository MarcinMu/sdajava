package well;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ReadFromFile {
	private String path;

	public ReadFromFile(String path) {
		super();
		this.path = path;
	}

	public List<Integer> readFromFile() {
		List<Integer> result = new ArrayList<>();
		BufferedReader reader;
		try {
			File myFile = new File(this.path);
			if (!myFile.exists()) {
				throw new WrongPathException(this.path);
			}
			reader = new BufferedReader(new FileReader(this.path));
			String lineFromFile = reader.readLine();
			while (lineFromFile != null) {
				result.add(Integer.parseInt(lineFromFile));
				lineFromFile = reader.readLine();
			}
		} catch (WrongPathException e) {
			System.out.println("File: \"" + e.getPath() + "\" doesn't exist");
		} catch (Exception e) {
			System.out.println("I can't read from file" + e);
		} 

		return result;
	}

}
