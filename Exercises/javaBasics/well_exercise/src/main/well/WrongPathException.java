package well;

public class WrongPathException extends Exception {
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public WrongPathException(String path) {
		super();
		this.path = path;
	}
}
