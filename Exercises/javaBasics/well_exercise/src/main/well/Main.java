package well;

public class Main {

	public static void main(String[] args) {
		Well myWell = new Well("studnia.txt");
		Rollers myRollers = new Rollers("krazki.txt", "wynik.txt");
		myRollers.placeManyRollersInWell(myWell);
		myRollers.writeRollersState();
	}

}
