package well;

import java.util.List;


public class Well {
	private List<Integer> wellStructure;
	private String readPath;
	
	public Well(String readPath) {
		super();
		this.readPath = readPath;
		constructWell();
	}
	
	public void constructWell(){
		ReadFromFile fileToBeReaden = new ReadFromFile(readPath);
		this.wellStructure = fileToBeReaden.readFromFile();
		//System.out.println("Well structure: "+ this.wellStructure);
	}
	
	public int getWellWidth(int depth) {
		return this.wellStructure.get(depth);
	}

	public int getWellDepth() {
		return this.wellStructure.size();
	}
	
}
