package well;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;


public class WriteToFile {
	private String path;

	public WriteToFile(String path) {
		super();
		this.path = path;
	}
	
	public void writeToFile(List<String> text) {
		try {
			File myFile = new File(this.path);
			if (myFile.exists()) {
				throw new WrongPathException(this.path);
			}
			BufferedWriter writer = new BufferedWriter(new FileWriter(this.path));
			java.util.Collections.sort(text);
			for (String line : text) {
				writer.write(line);
				writer.newLine();
				writer.flush();
			}
			writer.close();
		} catch (WrongPathException e) {
			System.out.println("File: \"" + e.getPath() + "\" already exists");
		}

		catch (Exception e) {
			System.out.println("I can't write to file" + e);
		}
	}
}
