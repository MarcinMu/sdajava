package well;

import java.util.ArrayList;
import java.util.List;

public class Rollers {
	private List<Integer> rollersQueue;
	private String rollerFilePath;
	private String writePath;
	private List<Integer> rollersPositions;

	public Rollers(String rollerFilePath, String writePath) {
		super();
		this.rollerFilePath = rollerFilePath;
		this.writePath = writePath;
		this.rollersPositions = new ArrayList<>();
		readRollersFromFile();
	}

	public void readRollersFromFile(){
		ReadFromFile rollerFile = new ReadFromFile(this.rollerFilePath);
		this.rollersQueue = rollerFile.readFromFile();
		System.out.println("Rollers queue: " + this.rollersQueue);
	}
	
	public Integer getNextRoller() {
		return rollersQueue.remove(0);
	}
	
	public void setRollerPosition(Integer rollerPosition) {
		this.rollersPositions.add(rollerPosition);
	}
	
	public void writeRollersState() {
		WriteToFile fileToBeWritten = new WriteToFile(writePath);
		List<String> textToBeWritten = new ArrayList<>();
		for (int i = 0; i < this.rollersPositions.size(); i++) {
			textToBeWritten.add(Integer.toString(this.rollersPositions.get(i)));
		}
		System.out.println("Rollers result: " + this.rollersPositions);
		fileToBeWritten.writeToFile(textToBeWritten);	
	}
	
	public void placeManyRollersInWell(Well myWell) {
		int numberOfRollers = this.rollersQueue.size();
		for (int i = 0; i < numberOfRollers; i++) {
			this.placeRollerInWell(myWell);
		}
	}
	
	public void placeRollerInWell(Well myWell) {
		Integer currentRollerSize = this.rollersQueue.remove(0);
		int currentDepth = 0;
		int wellWidth = 0;
		int maxDepth = myWell.getWellDepth();
		if(!this.rollersPositions.isEmpty()){
			maxDepth = this.rollersPositions.get(rollersPositions.size() - 1) -1 ;
		}
		wellWidth = myWell.getWellWidth(currentDepth);
		while(wellWidth >= currentRollerSize && currentDepth < maxDepth)
		{
			currentDepth++;
			if(currentDepth != maxDepth) {
				wellWidth = myWell.getWellWidth(currentDepth);
			}
		} 
		setRollerPosition(currentDepth );
	}
}
