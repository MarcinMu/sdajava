package well;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.validator.PublicClassValidator;

public class WellTest {
	@Test
	public void getWellDepthTest(){
		// given
		Well myWell = prepareWell();
		// when
		int size = myWell.getWellDepth();
		// then
		Assert.assertEquals(6, size);
	}
	
	@Test
	public void getWellSize() {
		// given
		Well myWell = prepareWell();
		// when
		int size = myWell.getWellWidth(3);
		// then
		Assert.assertEquals(6, size);
	}
	
	private Well prepareWell(){
		List<String> wellStructure = new ArrayList<>();
		wellStructure.add("4");
		wellStructure.add("5");
		wellStructure.add("6");
		wellStructure.add("8");
		wellStructure.add("9");
		wellStructure.add("1");
		prepareFileWithWellStructure(wellStructure);
		return new Well("studniaTest.txt");
	}
	
	private void prepareFileWithWellStructure(List<String> wellStructure){
		for(int i = 0; i < wellStructure.size(); i++) {
			WriteToFile fileToBeWritten = new WriteToFile("studniaTest.txt");
			fileToBeWritten.writeToFile(wellStructure);
		}
	}
}
