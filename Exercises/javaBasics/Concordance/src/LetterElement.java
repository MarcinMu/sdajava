import java.util.Vector;

public class LetterElement {
	
	public char letter;
	Vector<String> wordElement;
	

	public LetterElement(char letter) {
		this.letter = letter;
		this.wordElement = new Vector<String>();
	}
	
	public String printLetterWithElements() {
		String words = "";
		for(int i = 0; i < wordElement.size(); i++) {
			words+= wordElement.get(i) 	+ " , ";
		}
		return words;
	}
}
