import java.util.Vector;

public class CreateConcordance {
	Vector<LetterElement> concordance = new Vector<LetterElement>();

	public CreateConcordance(String[] words) {
		super();
		for (int i = 0; i < words.length; i++) {
			char firstLetter = words[i].charAt(0);
			LetterElement temp = isElementExist(firstLetter);
			if (temp != null) {
				insertElement(temp);
				insertWord(words[i]);
			} else {
				LetterElement newElement = new LetterElement(firstLetter);
				insertElement(newElement);
				insertWord(words[i]);
			}
		}
	}

	public LetterElement isElementExist(char firstLetter) {
		for (int i = 0; i < concordance.size(); i++) {
			if (firstLetter == concordance.get(i).letter) {
				return concordance.get(i);
			}
		}
		return null;
	}

	public void insertElement(LetterElement element) {
		if (isCharExists(element.letter)) {
			return;
		}
		for (int i = 0; i < concordance.size(); i++) {
			if ((int) element.letter <= (int) concordance.get(i).letter) {
				concordance.add(i, element);
				return;
			}
		}
		concordance.add(element);
	}

	@Override
	public String toString() {
		String pom = "";
		for (int i = 0; i < concordance.size(); i++) {
			pom += concordance.get(i).letter;
			pom +=" : ";
			pom += concordance.get(i).printLetterWithElements();
		}
		return pom;
	}

	public boolean isCharExists(char toCheck) {
		for (int i = 0; i < concordance.size(); i++) {
			if (toCheck == concordance.get(i).letter) {
				return true;
			}
		}
		return false;
	}

	public void insertWord(String word) {
		char charToFInd = word.charAt(0);
		for (int i = 0; i < concordance.size(); i++) {
			if (charToFInd == concordance.get(i).letter) {
				if (!checWordExist(concordance.get(i), word)) {
					concordance.get(i).wordElement.add(word);
				}
			}
		}
	}

	public boolean checWordExist(LetterElement letter, String word) {
		for (int i = 0; i < letter.wordElement.size(); i++) {
			if (letter.wordElement.get(i) == word) {
				return true;
			}
		}
		return false;
	}

}
