package com.kedzier.refactor.car;

import java.util.ArrayList;
import java.util.List;

public class CarApp {

	public static void main(String[] args) {

		List<Car> cars = new ArrayList<Car>();
		
		Car car1 = new Car();
		car1.color = "blue";
		car1.model="passat";
		car1.price= 100_000.0;
		
		Car car2 = new Car();
		car2.color = "red";
		car2.model="ferrari";
		car2.price= 1_000_000.0;
		
		cars.add(car1);
		cars.add(car2);
		
		

	}

}
