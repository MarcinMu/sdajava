package com.kedzier.refactor.sum;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SumNumbersFromFileApp {

	public static void main(String[] args) {

		File input = new File("numbers.txt");
		List<Integer> sums = new ArrayList<>();
		List<Double> avgs = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(input))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] lineParts = line.split(",");
				Integer count = 0;
				Integer sum = 0;
				for (int i = 0; i < lineParts.length; i++) {
					if (lineParts[i].trim().length() > 0) {
						count++;
						sum += Integer.decode(lineParts[i].trim());
					}
				}
				sums.add(sum);
				if (sum != 0) {
					avgs.add((double) (sum / count));
				} else {
					avgs.add(0.0);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Integer sum = 0;
		for (Integer partialSum : sums) {
			sum += partialSum;
		}
		
		System.out.println("Sum of all numbers is: " + sum);
		System.out.println("--------------------------");
		
		for (int i = 1; i <= sums.size(); i++) {
			System.out.println("Sum of line " + i + " is: " + sums.get(i - 1));
			System.out.println("Avg of line " + i + " is: " + avgs.get(i - 1));
			System.out.println("--------------------------");
		}
		

	}

}
