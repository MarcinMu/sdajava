
public class ListElement {
	
	private int el;
	private ListElement next;
	private ListElement prev;
	private String elementName;
	private int randomRange = 100;
	
	public ListElement(String elementName) {
		this.elementName = elementName; 
		this.next = null;
		this.prev = null;
		this.el = (int) (Math.random() * randomRange);
	}
	
	public ListElement(ListElement prev) {
		this.el = (int) (Math.random() * randomRange);
		this.next = null;
		this.prev = prev;
	}
	
	public ListElement() {
		this.el = (int) (Math.random() * randomRange);
		this.prev = null;
	}
	
//	public ListElement(ListElement next, String elementName) {
//		this.next = next;
//		this.elementName = elementName; 
//	}
	
	public ListElement getNext(){
		return this.next;
	}
	
	public ListElement getPrev() {
		return this.prev;
	}
	
	public ListElement getNextElement() {
		return this.next;
	}
	
	public String getName () {
		return this.elementName;
	}
	
	public void setNextElement(ListElement next) {
		this.next = next;
	}
	
	public void setName (String name) {
		this.elementName = name;
	}
	
	public void removeMeFromList(){
		if(this.next != null) this.next.setPrev(this.prev);
		if(this.prev != null) this.prev.setNext(this.next);
		this.next = null;
		this.prev = null;
	}
	
	public void addMeToTheList(ListElement prev, ListElement next) {
		this.prev = prev;
		this.next = next;
		if (prev != null)		this.prev.setNext(this);
		if (next != null)       this.next.setPrev(this);
	}
	
	public void setNext(ListElement next){
		this.next= next;
	}
	
	public void setPrev(ListElement prev){
		this.prev= prev;
	}
	
	@Override
	public String toString() {
		return elementName;
	}
	
	public int getEl(){
		return this.el;
	}
}
