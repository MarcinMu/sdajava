
public class List {

	private ListElement startElement;
	private ListElement endElement;
//	private int numberOfElements;
	
	public List() {
		startElement = null;
		endElement = null;
	}
	
//	private void incrementNumberOfElements(){
//		numberOfElements++;
//	}
//	
//	private void incrementNumberOfElements(){
//		numberOfElements++;
//	}
	
	public void insertFirst(String elementName) {
		if (isEmpty()) {
			ListElement newElement = new ListElement(elementName);
			startElement = newElement;
			endElement = newElement;
		}
	}
	
	public void insertFirst() {
		if (isEmpty()) {
			ListElement newElement = new ListElement();
			startElement = newElement;
			endElement = newElement;
		}
	}
	
	public void insertAtEnd (String elementName) {
		ListElement newElement = new ListElement(elementName);
		if (isEmpty()) {
			startElement = newElement;
		} else {
			endElement.setNextElement(newElement);
		}
		endElement = newElement;
	}
	
	public void insertAtEnd () {
		ListElement newElement = new ListElement(endElement);
		if (isEmpty()) {
			startElement = newElement;
		} else {
			endElement.setNextElement(newElement);
		}
		endElement = newElement;
	}
	
	public ListElement removeAndTakeFirst() {
		if(isEmpty()) {
			return null;
		} else {
			ListElement temp = startElement;
			startElement = startElement.getNextElement();
			return temp;
		}
	}
	
	public boolean isEmpty() {
		return (startElement == null);
	}
	
	public void print () {
		ListElement temp = startElement;
		while (temp != null) {
			System.out.print(temp.getEl()+" ");
			temp = temp.getNextElement();
		}
	}
	
	public String toString() {
		String result = "";
		ListElement temp = startElement;
		while (temp != null) {
			result += temp.getName()+ " ";
			temp = temp.getNextElement();
		}
		return result;
	}
	
	public String getLastElementName() {
		return this.endElement.getName();
	}
	
	public ListElement getElementWihtHighestValue (ListElement highestElement) {
		ListElement currentElement = highestElement.getNextElement();		
		while(currentElement!=null) {
			if(highestElement.getEl() < currentElement.getEl()){
				highestElement = currentElement;
			}
			currentElement = currentElement.getNextElement();
		}
		return highestElement;
	}
	
	public void sortLIst() {
		ListElement currentElement = this.startElement;
		while (currentElement != null) {
			ListElement temp = getElementWihtHighestValue(currentElement);
			if (currentElement.getEl() < temp.getEl()){
				temp.removeMeFromList();
				if(currentElement == this.startElement){
					this.startElement = temp;
				}
				temp.addMeToTheList(currentElement.getPrev(), currentElement);			
			} else {
				currentElement = currentElement.getNextElement();
			}
		}
	}
 	
}
