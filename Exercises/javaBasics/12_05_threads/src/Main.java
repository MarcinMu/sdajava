import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		for (int i = 0; i < 3; i++) {
			executorService.execute(new myThread());
		}
		executorService.shutdown();
//		Runnable myThread = new myThread();
//		Thread t1 = new Thread(myThread, "pierwszy");
//		Thread t2 = new Thread(myThread, "drugi");
//		Thread t3 = new Thread(myThread, "trzeci");
//		t1.start();
//		t2.start();
//		t3.start();
	}

}
