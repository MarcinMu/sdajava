package exercises.collections;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {
		// cutList();
		// createTreeWithPersons();
		// playWithMaps();
		writeToFile(new ArrayList<>(Arrays.asList("fsdfsd", "dafds", "DFDFDF", "Madagaskar", "Hong kong")),
				"myFile.txt");
	}

	public static void writeToFile(List<String> text, String path) {
		try {
			File myFile = new File(path);
			if (!myFile.exists()) {
				throw new WrongPathException(path);
			}
			BufferedWriter writer = new BufferedWriter(new FileWriter(path));
			java.util.Collections.sort(text);
			for (String line : text) {
				writer.write(line);
				writer.newLine();
				writer.flush();
			}
			writer.close();
		} catch (WrongPathException e) {
			System.out.println("File: \"" + e.getPath() + "\" doesn't exist");
		}

		catch (Exception e) {

		}
	}

	public static List<String> readFromFile(String path) {
		List<String> result = new ArrayList<String>() ;
		try {
			File myFile = new File(path);
			if (!myFile.exists()) {
				throw new WrongPathException(path);
			}
			BufferedReader reader = new BufferedReader(new FileReader(path));
			reader.readLine();
		} catch (WrongPathException e) {
			System.out.println("File: \"" + e.getPath() + "\" doesn't exist");
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	public static void playWithMaps() {
		List<Person> employees = createPersonsWithCompanies();
		Map<String, List<String>> companyEmployees = createPeopleMap(employees);
		System.out.println(employees);
		System.out.println(companyEmployees);
	}

	public static Map<String, List<String>> createPeopleMap(List<Person> input) {
		Map<String, List<String>> result = new HashMap<>();
		for (Person person : input) {
			String company = person.getCompany();
			if (!result.containsKey(company)) {
				result.put(company, new ArrayList<>());
			}
			result.get(company).add(person.getLastName());
		}
		return result;
	}

	public static List<Person> createPersonsWithCompanies() {
		int personNumber = 10;
		List<Person> allEmployees = new ArrayList<>();
		for (int i = 0; i < personNumber; i++) {
			allEmployees.add(new Person());
		}
		return allEmployees;
	}

	public static void createTreeWithPersons() {
		int personNumber = 5;
		Set<Person> people = new TreeSet<>();
		people.add(new Person("Maciej", "Wikary"));
		people.add(new Person("Maciej", "Wikary"));
		for (int i = 0; i < personNumber; i++) {
			people.add(new Person());
		}
		System.out.println(people);
	}

	public static void cutList() {
		List<Integer> toCut = new LinkedList<>();
		List<Integer> result = new LinkedList<>();
		ListExercise.completeList(toCut);
		result = ListExercise.returnSmallerList(toCut);
		System.out.println(toCut);
		System.out.println(result);
	}

}
