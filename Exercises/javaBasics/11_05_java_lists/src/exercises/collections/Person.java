package exercises.collections;

import java.util.Comparator;

public class Person implements Comparable<Person> {
	private String firstName;
	private String lastName;
	private String company;
	

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Person(String firstName, String lastName, String company) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.company = company;
	}
	
	public Person(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.company = null;
	}

	public Person() {
		super();
		String [] companies = {"Nestle", "Wawel" };
		String [] charTable = { "Maria", "bf", "cd" , "df", "eg", "gf", "dfasdf", "ggg", "ffsd", "fsdf", "dfsadf", "fdsafsdf", "zakoscielny"};
		this.firstName = charTable[(int) (Math.random()*charTable.length)];
		this.lastName = charTable[(int) (Math.random()*charTable.length)];
		this.company = companies[(int) (Math.random()*companies.length)];
	}
	



	@Override
	public int compareTo(Person other) {
		return this.lastName.compareTo(other.lastName);
	}

	@Override
	public String toString() {
		return firstName + " " + lastName + " works in " + company + "  ";
	}



	
	
}
