package exercises.collections;

import java.util.LinkedList;
import java.util.List;

public class ListExercise {
	
	public static List<Integer> returnSmallerList(List<Integer> toCut) {
		int average = calculateAverage(toCut);
		List<Integer> result = new LinkedList<>();
		for (int i = 0; i < toCut.size(); i++) {
			if(toCut.get(i) < average) {
				result.add(toCut.get(i));
			}
		}
		return result;
		
	}
	public static int calculateAverage(List<Integer> toCalculate) {
		int sum= 0;
		int i = 0;
		for (i = 0; i < toCalculate.size(); i++) {
			sum+=toCalculate.get(i);
		}
		sum /= i;
		return sum;
	}
	public static List<Integer> completeList(List<Integer> input) {
		int size = 100;
		for (int i = 0; i < size; i ++ ) {
			input.add((int) (Math.random()*size));
		}
		return input;
	}
}
