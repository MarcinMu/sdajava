package raport.generator;

import java.util.ArrayList;
import java.util.List;

public interface ReportRepository {
	public List<Report> myReports = new ArrayList<>();
	public void saveReport(Report myReport);
	public Report getReport(int index);
	
}
