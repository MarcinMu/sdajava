package ticTacToe;

public class main {

	public static void main(String[] args) {
		Game game = new Game();	
		game.addPlayers();
		game.displayGrid();
		game.play(game);

	}
}
