package ticTacToe;

import java.util.Scanner;



public class Game {
	private Grid grid;
	private Player player1;
	private Player player2;
	
	public Player getPlayer1() {
		return player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public Game() {
		grid = new Grid();
	}
	
	public void addPlayers() {
		player1 = new Player(1);
		player2 = new Player(2);
	}
	
	public void displayGrid() {
		grid.displayBoard(player1.getSign(), player2.getSign());
	}
	
	public void play(Game game){
		Player currentPlayer = player2;
		boolean deadEnd = false;
		int x, y; 
		Move myMove = new Move();
		do {
			if(currentPlayer.playerNumber==1){
				currentPlayer = player2;
			} else {
				currentPlayer = player1;
			}
			myMove.makeAMove(currentPlayer, grid, game);
			if (grid.endOfGrid()){
				System.out.println("");
				System.out.println("Dead heat ! ");
				deadEnd = true;
			}
		} while(!grid.isWIn(myMove.getX(), myMove.getY(), currentPlayer.playerNumber) && !deadEnd);
		if (!deadEnd) {
			System.out.println("");
			System.out.println(currentPlayer.getName() + " won! Congratulations ! ");			
		}
		System.out.println("Thank you for a game.");
	}
	
	

}
