package ticTacToe;

import java.util.Scanner;

public class Player {
	
	private String name;
	private String sign;
	private static char[] defaultChars = {'o','x'};
	public int getPlayerNumber() {
		return playerNumber;
	}

	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}

	public int playerNumber;
	public Player(int playerNumber) {
		super();
		this.playerNumber = playerNumber;
		this.name = this.takeName();
		this.sign = this.takeSign();
	}
	
	public Player() {
		super();
	}

	public String takeName(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Graczu "+ this.playerNumber+ " podaj swoje imie ");
		String playerName = scanner.nextLine();
		if(playerName.length()>0) 
			return(playerName);
		else return takeName();
	}
	
	public String takeSign(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Graczu "+ this.name+ " wprowadz swoj znak default: " + String.valueOf(defaultChars[playerNumber-1]) );
		String playerSign = scanner.nextLine();
		if(playerSign.length()>0 && playerSign.length()<2) {
			return(playerSign);			
		}else if (playerSign.length()==0){
			return String.valueOf(defaultChars[playerNumber-1]);
		}
		else {
			System.out.println("Bledna liczba znakow");
			return takeSign();
		}
	}
	
	@Override
	public String toString() {
		return "Player [name=" + name + ", sign=" + sign + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
