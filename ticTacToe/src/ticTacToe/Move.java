package ticTacToe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Move {
	
	private int x;
	private int y;
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Move() {
		this.x = -1;
		this.y = -1;
	}
	
	public void makeAMove(Player currentPlayer, Grid grid, Game game) {
		Scanner scanner = new Scanner(System.in);
		try {
			System.out.println("");
			System.out.println(currentPlayer.getName() + " Podaj x: ");
			this.x = scanner.nextInt();
			scanner.nextLine();
			System.out.println(currentPlayer.getName() + " Podaj y: ");
			this.y = scanner.nextInt();	
			scanner.nextLine();
			//scanner.close();
			if(!grid.addPosition(this.x, this.y, currentPlayer.playerNumber)){
				System.err.println("Poda�e� nieprawid�ow� warto��, spr�buj jeszcze raz ");
				makeAMove(currentPlayer, grid, game);
			} else {
				game.displayGrid();		
			}
		} catch (InputMismatchException e) {
			System.out.println("Poda�e� b��dn� warto��");
			makeAMove(currentPlayer, grid, game);
		}
	}
}