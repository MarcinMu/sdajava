package ticTacToe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Grid {
	private int width;
	private int height;
	private int moves;
	private int [][] gameBoard;

	public Grid(){
		this.takeTableSize();
		this.initGameBoard();
	}
	
	public void takeTableSize(){
		System.out.println("Jaki rozmiar planszy chcesz ?");
		Scanner scanner = new Scanner(System.in);
		try{
			int size = scanner.nextInt();
			this.setWidth(size);
			this.setHeight(size);
		}
		catch(InputMismatchException e){
			System.out.println("Podany przez Ciebie rozmiar jest nieprawid�owy, ogarnij si�");
			this.takeTableSize();
		}

	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setWidth (int width) {
		this.width= width;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void initGameBoard() {
		gameBoard = new int[width][height];
		moves = 0; 
	}
	
	public boolean addPosition(int x, int y, int player){
		if(x > 0 && x < width+1 && y > 0 && y < height+1 && gameBoard[x-1][y-1] == 0 ){
			gameBoard[x-1][y-1] = player;
			moves++;
			return true;
		}else {
			return false;
		}
	}
	
	public void displayBoard(String player1Char, String player2Char) {
		System.out.print("x\\y | ");
		for (int column = 1; column<width+1; column++){
			System.out.print(column+" | ");
		}
		for(int i=0; i < width; i++) {
			System.out.println("  ");
			int numberToPrint = i + 1; 
			System.out.print(" "+ numberToPrint +"  |");
			for (int j=0; j < height; j++) {
				switch (gameBoard[i][j]) {
				case 1:
					System.out.print(" "+player1Char+" |");
					break;
				case 2:
					System.out.print(" " + player2Char+ " |");
					break;
				default:
					System.out.print("   |");
					break;
				}
			}
		}
	}
	
	public boolean isWIn(int x, int y, int playerNumber) {
		x--;
		y--;
        boolean goOut=false;
        int i;
        int j;
        int result=0;
        int maxWidth = width;
        if (width > 5) {
        	maxWidth = 5;
        }
        if(y > 5) {
            i = y - 5;
        } else {
            i = 0;
        }
        do {
            if(gameBoard[x][i] == playerNumber) {
                result++;
                if (result == maxWidth) {
                    return true;
                }
            } else {
                result = 0;
            }
            i++;
        }while(i < width && i < y+5);
        result = 0;
        if(x > 5) {
            j = x - 5;
        } else {
            j = 0;
        }
        do {
            if(gameBoard[j][y] == playerNumber) {
                result++;
                if (result == maxWidth) {
                    return true;
                }
            } else {
                result = 0;
            }
            j++;
        }while(j < height && j < x+5);
        result = 0;
        if(x > 5) {
            i = x - 5;
        } else {
            i = 0;
        }

        if(x > 5) {
            j = x - 5;
        } else {
            j = 0;
        }

        do {
            if(gameBoard[i][j] == playerNumber) {
                result++;
                if (result == maxWidth) {
                    return true;
                }
            } else {
                result = 0;
            }
            i++;
            j++;
        }while(i < height && i < x+5 && j < height && j < x+5);

        return false;
    }
	
	public boolean endOfGrid () {
		if (moves == width * height){
			return true;
		}
		return false;
	}

}
