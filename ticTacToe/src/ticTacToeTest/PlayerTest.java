package ticTacToeTest;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import junit.framework.TestCase;
import ticTacToe.Player;

public class PlayerTest extends TestCase {

	public void testTakeNameShouldReturnPlayerName() throws Exception {
		//given
			Player myPlayer = new Player();
			myPlayer.setPlayerNumber(1);
			InputStream in = new ByteArrayInputStream("SuperMan".getBytes());
			System.setIn(in);
		//when
			String result = myPlayer.takeName();
		//then
			assertEquals("SuperMan", result);
	}
	
	public void testTakeSignShouldReturnPlayerSign() throws Exception {
		//given
			Player myPlayer = new Player();
			myPlayer.setPlayerNumber(1);
			myPlayer.setName("SuperMan");
			InputStream in = new ByteArrayInputStream("a".getBytes());
			System.setIn(in);
		//when
			String result = myPlayer.takeSign();
		//then
			assertEquals("a", result);
	}
	
	public void testTakeSignShouldReturnDefault() throws Exception {
		//given
			Player myPlayer = new Player();
			myPlayer.setPlayerNumber(1);
			myPlayer.setName("SuperMan");
			InputStream in = new ByteArrayInputStream("\n".getBytes());
			System.setIn(in);
		//when
			String result = myPlayer.takeSign();
		//then
			assertEquals("o", result);
	}
	
}
