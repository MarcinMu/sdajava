package com.marcin.web.customer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.marcin.core.customer.Customer;
import com.marcin.core.customer.CustomerDao;

@RunWith(MockitoJUnitRunner.class)
public class CustomerValidatorTest {

	@InjectMocks 
	CustomerValidator myValidator = new CustomerValidator();
	
	@Mock
	private CustomerDao customerDao;
	private CustomerValidator customerValidator;
	private Customer customer;
	
	@Autowired
	public Errors errors;
	
	@Test
	public void isCustomerExistShouldReturnTrue(){
		//given
			List<Customer> existingCustomers = new ArrayList<Customer>();
			Customer myExistingCustomer = new Customer();
			myExistingCustomer.setId(1L);
			existingCustomers.add(myExistingCustomer);
			Mockito.when(customerDao.getAll()).thenReturn(existingCustomers);
		//when
			boolean result = myValidator.isCustomerExist(1L);
		//then
			Assert.assertEquals(true, result);
	}
	
	@Test
	public void isCustomerExistShouldReturnFalse(){
		//given
			List<Customer> existingCustomers = new ArrayList<Customer>();
			Customer myExistingCustomer = new Customer();
			myExistingCustomer.setId(1L);
			existingCustomers.add(myExistingCustomer);
			Mockito.when(customerDao.getAll()).thenReturn(existingCustomers);
		//when
			boolean result = myValidator.isCustomerExist(2L);
		//then
			Assert.assertEquals(false, result);
	}
	
	@Test
	public void validateShouldReturnError(){
		//given
			customer = new Customer();
			errors = new BeanPropertyBindingResult(customer, "customer");
			Customer myEmptyCustomer = new Customer();
		//when
			myValidator.validate(myEmptyCustomer, errors);
		//then
			Assert.assertTrue(errors.hasErrors());	
	}
	
}
