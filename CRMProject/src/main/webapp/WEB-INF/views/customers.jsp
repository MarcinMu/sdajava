<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<spring:url value = "/css/style.css" var = "style"/>
		<link href = "${style}" rel = "stylesheet" />		
	</head>
	<body>
		
		<c:url value="/user/addCustomer" var= "addCustomer"/>
		<c:url value="/user/editCustomer" var= "editCustomer"/>
		<c:url value="/user/deleteCustomer" var= "deleteCustomer"/>
		<c:url value="/user/order" var= "order"/>
		
		<jsp:include page="menu.jsp" />
		
		<div class = "content">
			<h3> Our Customers: </h3>
			<table>
				<tr>
					<td> id </td>
					<td> First Name </td>
					<td> Surname </td>
					<td> Action </td>

				</tr>
				<c:forEach var = "customer" items = "${customers}" >
					<tr>
						<td>${customer.id}</td>
						<td>${customer.firstName}</td>
						<td>${customer.surname}</td>
						<td>
							<form action = "${editCustomer}" method = "post" id = "editCustomer">
								<input type = "hidden" value = "${customer.id}" name = "customerId">
								<input type = "submit" value = "Edit"></input>
							</form>
							<form action = "${deleteCustomer}" method = "post" id = "deleteCustomer">
								<input type = "submit" value = "Del"></input>
							</form>
							<form action = "${order}" method = "post" id = "order">
								<input type = "hidden" value = "${customer.id}" name = "customerId">
								<input type = "submit" value = "Order"></input>
							</form>
						</td>
					</tr>
				</c:forEach>
			</table>
			<h3> Add or edit customer: </h3>
			<table>
				<tr>
					<td> id </td>
					<td> First Name </td>
					<td> Surname </td>
					<td> Action </td>
				</tr>
				<tr>
					<form:form modelAttribute = "customer" action= "${addCustomer}" method = "post" id = "addcustomer">
						<td>
							<spring:bind path = "id">
								<form:input path = "id" type = "text"/>
								<form:errors path = "id" />
							</spring:bind>
						</td>
						<td>
							<spring:bind path = "firstName">
								<form:input path = "firstName" type = "text" /> 
								<form:errors path = "firstName" />
							</spring:bind>
						</td>
						<td>
							<spring:bind path = "surname">
								<form:input path = "surname" type = "text" />
								<form:errors path = "surname" />
							</spring:bind>
						</td>
						<td>
							<input type= "submit" value = "Save"/>
						</td>
					</form:form>
				</tr>
			</table>			
		</div>
	
	</body>
</html>