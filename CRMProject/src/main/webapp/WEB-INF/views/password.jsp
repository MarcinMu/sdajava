<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
	<head>
		<spring:url value = "/css/style.css" var = "style"/>
		<link href = "${style}" rel = "stylesheet" />
	</head>
	<body>
		<c:url value="/logout" var="logoutUrl" />
		<c:url value="userlist" var = "userList" />
		<div class = "menu">	
			<ul>
				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<li>	
					<form action="${userList}" method="get" id="userListLink">
						<input type="submit" value="Manage Users"></input>	
					</form>
					</li>
				</sec:authorize>	
				<li>
					<form action="${logoutUrl}" method="post" id="logoutForm">
						<input type="submit" value="Logout"></input>	
					</form>
				</li>
			</ul>
		</div>
		<div class = "content">
			<c:url value = "/admin/newpassword" var = "newPassword" />
			
			<h4>Change user <B> ${currentUser.firstname} ${currentUser.surname} </B> password </h4>
			<form action = "${newPassword}" method = "post" id = "newPassword">
				<input type = "hidden" value = "${currentUser.id}" name = "currentUser"/>
				<input type = "text" name = "newPassword" > </input>
				<input type = "submit" value = "Save"> </input>
			</form>
			
			<c:if test = "${isSuccess == 'true'}">
				<h2> ${currentUser.firstname}  ${currentUser.surname} password was changed </h2>
			</c:if>
		</div>
	</body>
</html>