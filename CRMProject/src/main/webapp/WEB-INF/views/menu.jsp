<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<spring:url value = "/css/style.css" var = "style"/>
		<link href = "${style}" rel = "stylesheet" />	
		<c:url value="/logout" var="logoutUrl" />
		<c:url value="/admin/userlist" var = "userList" />
		<c:url value="/user/customerList" var="customerList" />
	</head>
	<body>
		<div class = "menu">	
			<h3> Hello ${username} ! </h3>
			<ul class = "menu">
				<li>
					<form action = "${customerList}" method = "get" id = "customerList">
						<input type = "submit" value = "Customers"/>
					</form>
				</li>
				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<li>	
					<form action="${userList}" method="get" id="userListLink">
						<input type="submit" value="Manage Users"></input>	
					</form>
					</li>
				</sec:authorize>	
				<li>
					<form action="${logoutUrl}" method="post" id="logoutForm">
						<input type="submit" value="Logout"></input>	
					</form>
				</li>
			</ul>
		</div>
	</body>
</html>