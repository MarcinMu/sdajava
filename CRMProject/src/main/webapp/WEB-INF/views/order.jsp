<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<spring:url value = "/css/style.css" var = "style"/>
		<link href = "${style}" rel = "stylesheet" />	
		<title> Order </title>
	</head> 
	<body>
		<c:url value="/user/addOrder" var = "addOrder"/>
		<jsp:include page="menu.jsp" />
		<div class = "content">
			<h4> Customer: </h4>
				${customer.firstName} ${customer.surname}  (Id: ${customer.id})
			<h4> Order: </h4>
			<form:form modelAttribute = "order" action ="${addOrder}" method = "post" id = "addOrder" >
				<spring:bind path = "id">
					<form:input path = "id" type = "hidden"/>
				</spring:bind>
				<spring:bind path = "customer">
					<form:input path = "customer" value = "${customer.id}" type = "hidden" />
					<form:errors path = "customer" />
				</spring:bind>
				<input  type = "hidden" value = "${customer.id}" name = "customerId" />
				<spring:bind path = "productName">
					<label>Product:</label>
					<form:input path = "productName" type = "text" />
					<form:errors path = "productName" />
				</spring:bind>
				</br>
				<spring:bind path = "status">
					<label>Status:</label>
					<form:select path = "status">
						<form:option value = "NONE" label = "-----" />
						<form:options items = "${statuses}" />
					</form:select>
					<form:errors path = "status"/>
				</spring:bind>
				</br>
				<input type = "submit" value = "Save"/>
			</form:form>
		</div>
	</body>
</html>