<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head> 
<spring:url value = "/css/style.css" var = "style"/>
<link href = "${style}" rel = "stylesheet" />

</head>
<body>
	<c:url value="/admin/adduser" var = "adduser" />
	<c:url value="/admin/changepassword" var = "changePassword" />
	<c:url value="/admin/disactivate" var = "disactivate" />
		
	<sec:authorize access="hasRole('ROLE_ADMIN')">
	<jsp:include page="menu.jsp" />
	<div class = "content">
		<h2> Existing users: </h2>
		<table>
			<tr>
				<td>id</td>
				<td>login</td>
				<td>Surname</td>
				<td>First name</td>
				<td>Role</td>
				<td>Date of creation</td>
				<td>Actions</td>
			</tr>
			<c:forEach var="user" items="${users}">
				<tr>
					<td>${user.id}</td>
					<td>${user.login}</td>
					<td>${user.surname}</td>
					<td>${user.firstname}</td>
					<td>${user.role.getHumanFriendlyName()}</td>
					<td>${user.creationDate}</td>
					<td>
						<form action = "${changePassword}" method = "post" id = "changePassword">
							<input type = "hidden" value = "${user.id}" name = "currentUser">
							<input type = "submit" value = "Change password"></input>
						</form>
					</td>
					<td>
						<form action = "${disactivate}" method = "post" id = "changePassword">
							<input type = "hidden" value = "${user.id}" name = "currentUser">
							<input type = "submit" value = "Disactivate user"></input>
						</form>
					</td>
				</tr>
			</c:forEach>
		</table>	
		
		<h2> Add new user </h2>
		<form:form modelAttribute = "user" action="${adduser}" method="post" id="adduser">
			<table>
				<tr>
					<td>login</td>
					<td>Surname</td>
					<td>First name</td>
					<td>Role</td>
				</tr>
				<tr>
				
					<td> 
						<spring:bind path = "login">
							<form:input path = "login" type = "text" />
							<form:errors path = "login" />
						</spring:bind>
					</td>
					<td>
						<spring:bind path = "surname">
							<form:input path = "surname" type = "text" />
							<form:errors path = "surname" />
						</spring:bind>
					</td>
					<td>
						<spring:bind path = "firstName">
							<form:input path = "firstName" type = "text" /> 
							<form:errors path = "firstName" />
						</spring:bind>
					</td>
					<td>
						<spring:bind path = "role">
							<form:select path = "role">
								<form:option value = "NONE" label = "----------" />
								<form:options items = "${usersRoles}" />
							</form:select>
							<form:errors path = "role" />
						</spring:bind>
					</td>
				</tr>
			</table>
			<input type="submit" value="Add user"/>	
		</form:form>
	</div>	
	</sec:authorize>
</body>
</html>