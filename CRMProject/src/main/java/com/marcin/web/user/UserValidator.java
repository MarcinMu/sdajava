package com.marcin.web.user;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.marcin.core.user.User;
import com.marcin.core.user.UserDao;
import com.marcin.core.user.UserRole;


@Component
public class UserValidator implements Validator {

	@Autowired
	private UserDao userdao;
	
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }
 
    public void validate(Object target, Errors errors) {
    	User user = (User) target;
        
    	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "validation.user.emptyField");
    	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "validation.user.emptyField");
    	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "validation.user.emptyField");
    	
    	if(loginAlreadyExist(user)){
    		errors.rejectValue("login", "validation.user.loginExists");
    	}
    	
    	if(roleIsNotChosen(user)){
    		errors.rejectValue("role", "validation.user.wrongRole");
    	}
    	
    }

    private boolean roleIsNotChosen(User user) {
		UserRole userRoles [] = userdao.getRoles();
		for(UserRole currentUserRole: userRoles) {
			if(currentUserRole.equals(user.getRole())){
				return false;
			}
		}
		return true;
	}

	private boolean loginAlreadyExist(User user) {
    	List<User> users = userdao.getAll();
    	for(User currentUser: users) {
    		if(currentUser.getLogin().equals(user.getLogin())){
    			return true;
    		}
    	}
    	return false;
    }
}
