package com.marcin.web.user;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.marcin.core.user.User;
import com.marcin.core.user.UserDao;
import com.marcin.core.user.UserProvider;
import com.marcin.core.user.UserRole;


@Controller
public class UserController {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserValidator userValidator;
	
	@Autowired
	private UserProvider userProvider;
	
	@InitBinder     
	public void initBinder(WebDataBinder binder){
		binder.addValidators(userValidator);
	}
	
	@RequestMapping("/admin/userlist")
	public ModelAndView openUsersListPage() {
		ModelAndView model = new ModelAndView("userslist");
		model.addObject("user", new User());
		addRolesToModel(model);
		return model;
	}
	
	private void addRolesToModel(ModelAndView model) {
		UserRole [] usersRoles = userDao.getRoles();
		Map<String, String> possibleRoles = new LinkedHashMap<String, String>();
		for(UserRole role: usersRoles) {
			possibleRoles.put(role.toString(), role.getHumanFriendlyName());
		}
		model.addObject("username", userProvider.getLoggedUser().getFirstName() );
		model.addObject("usersRoles", possibleRoles);
		model.addObject("users", userDao.getAll());
	}

	@RequestMapping("/admin/changepassword")
	public ModelAndView openChangePassword(@RequestParam(value = "currentUser") Long currentUser) {
		ModelAndView model = new ModelAndView("password");
		model.addObject("currentUser", userDao.getById(currentUser));
		return model;
	}
	
	@RequestMapping("/admin/newpassword")
	public ModelAndView saveNewPassword(@RequestParam(value = "currentUser") Long currentUser, @RequestParam(value = "newPassword") String newPassword) {
		ModelAndView model = new ModelAndView("password");
		model.addObject("currentUser", userDao.getById(currentUser));
		User myUser = userDao.getById(currentUser);
		myUser.setPassword(newPassword);
		userDao.saveOrUpdate(myUser);
		model.addObject("isSuccess", "true");
		return model;
	}
	
	@RequestMapping(value = "/admin/adduser", method = RequestMethod.POST)
	public ModelAndView addUser(@ModelAttribute @Validated User user, BindingResult bindingResult) {
		if(bindingResult.hasErrors()){
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("userslist");
			addRolesToModel(modelAndView);
			return modelAndView;
		}
		userDao.saveOrUpdate(user);
		return openUsersListPage() ;
	}
}
