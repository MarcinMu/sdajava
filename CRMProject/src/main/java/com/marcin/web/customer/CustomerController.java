package com.marcin.web.customer;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.marcin.core.customer.Customer;
import com.marcin.core.customer.CustomerDao;
import com.marcin.core.user.UserProvider;

@Controller
public class CustomerController {

	@Autowired
	UserProvider userProvider;
	
	@Autowired
	CustomerValidator customerValidator;
	
	@Autowired
	CustomerDao customerDao;
	
	@InitBinder("customer")
	public void initBinder(WebDataBinder binder){
		binder.addValidators(customerValidator);
	}
	
	@RequestMapping("user/customerList")
	public ModelAndView openCustomerList() {
		ModelAndView model = new ModelAndView("customers");
		model.addObject("customer", new Customer());
		model.addObject("customers", customerDao.getAll() );
		model.addObject("username", userProvider.getLoggedUser().getFirstName());	
		return model;
	}
	
	@RequestMapping(value = "/user/addCustomer", method = RequestMethod.POST)
	public ModelAndView saveCustomer(@ModelAttribute @Validated Customer customer, BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			ModelAndView model = new ModelAndView();
			model.setViewName("customers");
			model.addObject("customers", customerDao.getAll());
			model.addObject("username", userProvider.getLoggedUser().getFirstName());	
			return model;
		}
		customerDao.saveOrUpdate(customer);
		return openCustomerList();
	}
	
	@RequestMapping(value = "/user/editCustomer", method = RequestMethod.POST)
	public ModelAndView editCustomer(@RequestParam(value = "customerId") Long customerId){
		ModelAndView model = new ModelAndView("customers");
		model.addObject("customers", customerDao.getAll() );
		model.addObject("username", userProvider.getLoggedUser().getFirstName());
		model.addObject("customer", customerDao.getById(customerId));
		return model;
	}
	
}
