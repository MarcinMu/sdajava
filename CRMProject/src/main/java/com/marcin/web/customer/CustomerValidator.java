package com.marcin.web.customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.marcin.core.customer.Customer;
import com.marcin.core.customer.CustomerDao;

@Component
public class CustomerValidator implements Validator {

	@Autowired
	private CustomerDao customerDao;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Customer.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Customer customer = (Customer) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "validation.customer.emptyField");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "validation.customer.emptyField");
		
		
		if(customer.getId()!=null && !isCustomerExist(customer.getId())) {
			errors.rejectValue("id", "validation.customer.unexistingId");
		}
	}
	
	public boolean isCustomerExist(Long customerId) {
		List<Customer> existingCustomers = customerDao.getAll();
		for(Customer currentCustomer: existingCustomers) {
			if(currentCustomer.getId().equals(customerId)){
				return true;
			}
		}
		return false;
	}

}
