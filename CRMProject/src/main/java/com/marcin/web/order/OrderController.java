package com.marcin.web.order;



import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.marcin.core.customer.Customer;
import com.marcin.core.customer.CustomerDao;
import com.marcin.core.order.Order;
import com.marcin.core.order.OrderDao;
import com.marcin.core.order.OrderStatus;
import com.marcin.core.user.UserProvider;

@Controller
public class OrderController {

	@Autowired
	UserProvider userProvider;
	
	@Autowired
	CustomerDao customerDao;
	
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	OrderValidator orderValidator;
	
	@InitBinder("order")
	public void initBinder(WebDataBinder binder){
		binder.addValidators(orderValidator);
	}
	
	@RequestMapping(value = "/user/order", method = RequestMethod.POST)
	public ModelAndView openOrderPage(@RequestParam(value = "customerId") Long customerId){
		ModelAndView model = new ModelAndView("order");
		model.addObject("customer", customerDao.getById(customerId));
		model.addObject("existingOrder", getCustomersOrder(customerId));
		model.addObject("username", userProvider.getLoggedUser().getFirstName());
		model.addObject("statuses",getStatuses());
		model.addObject("order", new Order());
		return model;
	}
	
	@RequestMapping(value = "/user/addOrder", method = RequestMethod.POST)
	public ModelAndView saveOrder(@ModelAttribute @Validated Order order, BindingResult bindingResult, @RequestParam(value = "customerId") Long customerId) {
//		Long customerId = customer.getId();
		if(bindingResult.hasErrors()){
			ModelAndView model = new ModelAndView();
			model.setViewName("order");
			model.addObject("customer", customerDao.getById(customerId));
			model.addObject("existingOrder", getCustomersOrder(customerId));
			model.addObject("username", userProvider.getLoggedUser().getFirstName());
			model.addObject("statuses",getStatuses());
			return model;
		}
		orderDao.saveOrUpdate(order);
		return openOrderPage(customerId);
	}
	
	List<Order> getCustomersOrder(Long customerId){
		List<Order> existingOrders = orderDao.getAll();
		if (existingOrders.size() <1){
			return null;
		}
		List<Order> result = new ArrayList<>();
		for(Order currentOrder:existingOrders){
			if(currentOrder.getCustomer().getId() == customerId){
				result.add(currentOrder);
			}
		}
		return result;//result;
	}
	
	private Map<String, String> getStatuses(){
		OrderStatus [] statuses = orderDao.getStatuses();
		Map<String, String> result = new LinkedHashMap<String, String>();
		for(OrderStatus currentStatus: statuses) {
			result.put(currentStatus.toString(), currentStatus.getNameToPrint());
		}
		return result;
	}
	
}
