package com.marcin.web.order;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.marcin.core.order.Order;

@Component
public class OrderValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Order.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Order order = (Order) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productName", "validation.order.emptyField");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "status", "validation.order.emptyField");
	}

}
