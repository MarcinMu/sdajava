package com.marcin.core.config;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import org.h2.tools.Server;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.format.FormatterRegistry;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.marcin.core.order.StringToCustomer;

@Configuration
@ComponentScan(basePackages = "com.marcin.core")
@EnableTransactionManagement
public class AppConfig extends WebMvcConfigurerAdapter {

	@Value("${db.hibernate.dialect}")
	private String hibernateDialect;

	@Value("${db.hibernate.showSql}")
	private String hibernateShowSql;

	@Value("${db.hibernate.formatSql}")
	private String hibernateFormatSql;

	@Value("${db.hibernate.hbm2ddl.auto}")
	private String hibernateAutoDdl;

	@Value("${db.hibernate.hbm2ddl.import_files}")
	private String hibernateInitScript;

	@Bean
	public static PropertyPlaceholderConfigurer placeholderConfigurer() {
		PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
		propertyPlaceholderConfigurer.setLocation(new ClassPathResource("app.properties"));
		return propertyPlaceholderConfigurer;
	}
	
	@Bean
	public ConversionService conversionService() {
		ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
        bean.setConverters(getConverters());
        bean.afterPropertiesSet();
        ConversionService object = bean.getObject();
        return object;
	}
	
    private Set<Converter> getConverters() {
        Set<Converter> converters = new HashSet<Converter>();

        converters.add(new StringToCustomer());

        return converters;
    }
	
	@Bean
	public LocalSessionFactoryBean sessionFactoryBean() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setPackagesToScan("com.marcin.core");
		sessionFactory.setNamingStrategy(ImprovedNamingStrategy.INSTANCE);
		sessionFactory.setHibernateProperties(hibernateProperties());
		sessionFactory.setDataSource(dataSource());
		return sessionFactory;
	}

	@Bean
	public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) {
		PlatformTransactionManager txManager = new HibernateTransactionManager(sessionFactory);
		return txManager;
	}

	@Bean
	DataSource dataSource() {
		return new EmbeddedDatabaseBuilder().setName("dataSource").setType(EmbeddedDatabaseType.H2).build();
	}

	@Bean(initMethod = "start", destroyMethod = "stop")
	@DependsOn("h2WebServer")
	Server h2Server() throws SQLException {
		return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9092");
	}

	@Bean(initMethod = "start", destroyMethod = "stop")
	Server h2WebServer() throws SQLException {
		return Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082");
	}

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", hibernateDialect);
		properties.put("hibernate.show_sql", hibernateShowSql);
		properties.put("hibernate.format_sql", hibernateFormatSql);
		properties.put("hibernate.hbm2ddl.auto", hibernateAutoDdl);
		properties.put("hibernate.hbm2ddl.import_files", hibernateInitScript);
		return properties;

	}
	
    @Override
    public void addFormatters(FormatterRegistry formatterRegistry)
    {
        formatterRegistry.addConverter(getMyConverter());
    }

    @Bean
    public StringToCustomer getMyConverter()
    {
        return new StringToCustomer();
    }


}
