package com.marcin.core.user;

import java.util.List;

import com.marcin.core.BaseDao;

public interface UserDao extends BaseDao<User, Long> {
	
	User getByLogin(String login);

	UserRole[] getRoles();

}
