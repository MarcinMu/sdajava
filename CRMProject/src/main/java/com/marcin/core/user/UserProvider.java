package com.marcin.core.user;

public interface UserProvider {
	
	User getLoggedUser();
	
	void saveLoggedUser(String username);

}
