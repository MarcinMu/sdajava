package com.marcin.core.user;

import java.util.Date;




import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class User {
	
	@Id
	@GeneratedValue(generator = "userSeq")
	@SequenceGenerator(name = "userSeq", sequenceName = "user_seq")
	private Long id;
	
	@Column
	private String login;
	
	@Column
	private String password;
	
	@Column
	private String surname;
	
	@Column
	private String firstname;
	
	@Column
	@Enumerated(EnumType.STRING)
	private UserRole role;

//	@Column
//	private boolean isActive;
	
	@Column
	private Date creationDate = new Date();

	public final Long getId() {
		return id;
	}

	public final void setId(Long id) {
		this.id = id;
	}

	public final String getLogin() {
		return login;
	}

	public final void setLogin(String login) {
		this.login = login;
	}

	public final String getPassword() {
		return password;
	}

	public final void setPassword(String password) {
		this.password = password;
	}

	public final String getSurname() {
		return surname;
	}

	public final void setSurname(String surname) {
		this.surname = surname;
	}

	public final String getFirstName() {
		return firstname;
	}

	public final void setFirstName(String firstName) {
		this.firstname = firstName;
	}

	public final UserRole getRole() {
		return role;
	}

	public final void setRole(UserRole role) {
		this.role = role;
	}

	
	
//	public final boolean isActive() {
//		return isActive;
//	}
//
//	public final void setActive(boolean isActive) {
//		this.isActive = isActive;
//	}

	public final String getFirstname() {
		return firstname;
	}

	public final void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public final Date getCreationDate() {
		return creationDate;
	}

	public final void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	

}
