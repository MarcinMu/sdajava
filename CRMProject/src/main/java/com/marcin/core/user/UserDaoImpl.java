package com.marcin.core.user;

import com.marcin.core.*;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;


@Repository
public class UserDaoImpl extends AbstractBaseDao<User, Long> implements UserDao {

	@Override
	protected Class<User> supports() {
		return User.class;
	}

	public User getByLogin(String login) {
		return (User) currentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("login", login))
				.uniqueResult();
	}

	@Override
	public UserRole [] getRoles() {
		UserRole[] userRoles = UserRole.values();
		return userRoles;
	}


}
