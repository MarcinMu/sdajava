package com.marcin.core.user;

public enum UserRole {
	
	NONE("------"),
	ROLE_USER("User"),
	ROLE_ADMIN("Admin");
	
	String humanFriendlyName;
	
	private UserRole(String name){
		humanFriendlyName = name;
	}

	public final String getHumanFriendlyName() {
		return humanFriendlyName;
	}

}
