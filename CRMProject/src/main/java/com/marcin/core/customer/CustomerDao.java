package com.marcin.core.customer;

import com.marcin.core.BaseDao;

public interface CustomerDao extends BaseDao<Customer, Long> {
	
}
