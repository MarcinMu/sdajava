package com.marcin.core.customer;

import org.springframework.stereotype.Repository;

import com.marcin.core.AbstractBaseDao;

@Repository
public class CustomerDaoImpl extends AbstractBaseDao<Customer, Long> implements CustomerDao {

	@Override
	protected Class<Customer> supports() {
		return Customer.class;
	}

}
