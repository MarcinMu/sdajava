package com.marcin.core.customer;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.marcin.core.order.Order;

@Entity
public class Customer {
	
	@Id
	@GeneratedValue(generator = "customerSeq")
	@SequenceGenerator(name = "customerSeq", sequenceName = "customer_seq" )
	private Long id;
	
	@Column
	private String firstName;
	
	@Column
	private String surname;
	
	@Column
	private Date creationDate;
	
	@OneToMany(mappedBy = "customer")
	private Set<Order> orders;

	public final Long getId() {
		return id;
	}

	public final void setId(Long id) {
		this.id = id;
	}

	public final String getFirstName() {
		return firstName;
	}

	public final void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public final String getSurname() {
		return surname;
	}

	public final void setSurname(String surname) {
		this.surname = surname;
	}

	public final Date getCreationDate() {
		return creationDate;
	}

	public final void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	
}
