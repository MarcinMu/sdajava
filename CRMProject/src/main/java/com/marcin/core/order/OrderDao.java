package com.marcin.core.order;

import java.util.List;

import com.marcin.core.BaseDao;
import com.marcin.core.customer.Customer;

public interface OrderDao extends BaseDao<Order, Long> {
	OrderStatus [] getStatuses();
}
