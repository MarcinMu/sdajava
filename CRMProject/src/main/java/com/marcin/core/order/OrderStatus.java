package com.marcin.core.order;

public enum OrderStatus {
	
	ORDER_PLACED("Order Placed"),
	ORDER_PAYED("Customer has payed"),
	ORDER_DELIVERED("Order delivered"),
	ORDER_CLOSED("Closed");
	
	String nameToPrint;
	
	private OrderStatus(String name) {
		nameToPrint = name;
	}

	public final String getNameToPrint() {
		return nameToPrint;
	}

}
