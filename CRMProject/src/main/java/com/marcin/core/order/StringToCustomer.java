package com.marcin.core.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import com.marcin.core.customer.Customer;
import com.marcin.core.customer.CustomerDao;

public class StringToCustomer implements Converter<String, Customer> {

	@Autowired
	CustomerDao customerDao;
	
	@Override
	public Customer convert(String source) {
		return customerDao.getById(Long.parseLong(source)); 
	}

}
