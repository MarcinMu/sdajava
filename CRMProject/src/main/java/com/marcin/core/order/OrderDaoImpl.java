package com.marcin.core.order;


import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.marcin.core.AbstractBaseDao;
import com.marcin.core.customer.Customer;

@Repository
public class OrderDaoImpl extends AbstractBaseDao<Order, Long> implements OrderDao {

	@Override
	protected Class<Order> supports() {
		return Order.class;
	}

	@Override
	public OrderStatus[] getStatuses() {
		return OrderStatus.values();
	}

//	@Override
//	public Order getAllOrdersById(Customer customer) {
//		getHibernateTemplate().get(Order.class, customer.getId());
//		
//	}





}
