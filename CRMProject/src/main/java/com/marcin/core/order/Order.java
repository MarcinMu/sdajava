package com.marcin.core.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.marcin.core.customer.Customer;

@Entity(name = "ORDER_FOR_PRODUCT")
public class Order {

	@Id
	@GeneratedValue(generator = "orderSeq")
	@SequenceGenerator(name = "orderSeq", sequenceName = "order_seq")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "id_customer")
	private Customer customer;
	
	@Column
	private String productName;
	
	@Column
	@Enumerated(EnumType.STRING)
	private OrderStatus status;

	public final Long getId() {
		return id;
	}

	public final void setId(Long id) {
		this.id = id;
	}

	public final Customer getCustomer() {
		return customer;
	}

	public final void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public final String getProductName() {
		return productName;
	}

	public final void setProductName(String productName) {
		this.productName = productName;
	}

	public final OrderStatus getStatus() {
		return status;
	}

	public final void setStatus(OrderStatus status) {
		this.status = status;
	}
	

}
