

create sequence user_seq;
INSERT INTO user(id, login, password, firstname, surname, role) values (nextval('user_seq'), 'login', 'login', 'Marcin', 'Example', 'ROLE_USER');
INSERT INTO user(id, login, password, firstname, surname, role) values (nextval('user_seq'), 'admin', 'admin', 'Wieslaw', 'Admin', 'ROLE_ADMIN');

commit;
