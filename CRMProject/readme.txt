Technologia: Java, JSP, JDBC, Hibernate, Spring, CSS, Javascript, Html

Temat: System CRM do zarz�dzania danymi klient�w i procesem sprzeda�y.

Funkcjonalno��: System webowy do zarz�dzania kontaktami klient�w z firm�. Pracownicy loguj� si� do systemu z r�nymi uprawnieniami ( konsultant pierwszej linii, konsultant drugiej linii, administrator, kierownik) i w zale�no�ci od uprawnie� mog� wykonywa� w systemie nast�puj�ce czynno�ci:

- logowanie 
- dodawanie/edycja/dezaktywacja u�ytkownik�w
- dodanie/edycja klienta
- przegl�danie listy klient�w ( z filtrowaniem )
- przydzielanie konsultant�w pierwszej linii i konsultant�w drugiej linii do klient�w
- wprowadzanie zam�wienia na produkt 
	- wyszukiwanie produktu z bazy 
	- rezerwacja produktu
	- obieg zam�wienia
- dodawanie uwag
---------------
opcjonalne:
- przegl�danie zada� do realizacji w oddzielnym widoku.
- dodawanie, edycja zada� powsta�ych w interakcji z klientem



+++++++++++++++++++++++++++++++++++++++++++++++++++++++

UseCasy:

1. Nazwa

	logowanie

2. Identyfikator
	
	p1

3. Aktorzy

	u�ytkownik

4. Udzia�owcy / zainteresowani i cele

	u�ytkownik - uzyskanie dost�pu do aplikacji. System - ochrona dost�pu do danych	

5. Kr�tki opis

	uzyskanie dost�pu do aplikacji

6. Warunki wst�pne

	u�ytkownik musi posiada� konto w systemie

7. Warunki ko�cowe

	U�ytkownik dostaje dost�p do odpowiednich funkcjonalno�ci w zale�no�ci od posiadancyh uprawnie�.

8. G��wny przep�yw zdarze�

	a. u�ytkownik trafia na ekran logowania
	b. u�ytkownik wprowadza login, has�o, zatwierdza
	c. system sprawdza poprawno�� danych
	d. je�li dane s� poprawne przejd� do e.
	   je�li dane s� niepoprawne przejd� do f.
	e. system przekierowuje u�ytkownika na ekran z list� klient�w
	f. system wy�wietla informacje o niepoprawnych danych
	g. system prosi o ponowne podanie danych

9. Alternatywny przep�yw zdarze�.
	brak
10. Specjalne wymagania.
	brak


==========================================================================


1. Nazwa

	dodawanie/edycja/dezaktywacja u�ytkownik�w

2. Identyfikator

	p2

3. Aktorzy

	administrator

4. Udzia�owcy / zainteresowani i cele
	
	administrator - przegl�danie u�ytkownik�w, dodawanie u�ytkownik�w, eycja ich danych, blokowanie dost�pu.
	u�ytkownik - uzyskanie dost�pu do aplikacji

5. Kr�tki opis
	
	administrator mo�e zarz�dza� u�ytkownikami.

6. Warunki wst�pne
	
	Administrator musi by� zalogowany.

7. Warunki ko�cowe

	Istniej� u�ytkownicy z pe�nymi danymi.

8. G��wny przep�yw zdarze�

	a. administrator widzi list� u�ytkownik�w
	b. administrator wype�nia formularz z danymi u�ytkownik�w i zatwierdza.
	c. system sprawdza wype�nienie wymaganych p�l.
		- je�li dane niepoprawne wy�wietla stosowny komunikat.
	d. system zapami�tuje wprowadzonego u�ytkownika
	e. system wy�wietla zaktualizowan� list� u�ytkownik�w.

9. Alternatywny przep�yw zdarze�.

	brak

10. Specjalne wymagania.

	dost�p wy��cznie dla administratora.


================================================================================

1. Nazwa

	dodanie/edycja klienta

2. Identyfikator

	p3

3. Aktorzy

	konsultant

4. Udzia�owcy / zainteresowani i cele
	
	konsultant - dodanie nowego klienta, edycja danych istniej�cego	

5. Kr�tki opis
	
	konsultant mo�e doda� nowego klienta / przeedytowa� istniej�cego
	

6. Warunki wst�pne

	brak

7. Warunki ko�cowe

	zapisanie nowego klienta z danymi.

8. G��wny przep�yw zdarze�

	a. konsultant na g��wnej stronie ma link do dodania klienta
	b. po klikni�ciu w link otwiera si� okno z widokiem klienta z polami w fomie formularza
	c. konsultant wype�nia dane i klika zatwierd�
	d. system waliduje dane
		- je�li dane s� niepoprawne system wy�wietla stosowny komunikat
	e. system zapami�tuje wprowadzone dane i wy�wietla komunikat o sukcesie

9. Alternatywny przep�yw zdarze�.
	
	brak

10. Specjalne wymagania.

	brak

==========================================

1. Nazwa
	
	przegl�danie listy klient�w 

2. Identyfikator

	p4

3. Aktorzy

	konsultant, kierownik

4. Udzia�owcy / zainteresowani i cele

	konsultant, kierownik - chc� zobaczy� list� klient�w
	
5. Kr�tki opis

	U�ytkownicy maj� dost�p do listy klient�w.

6. Warunki wst�pne

	U�ytkownik musi by� zalogowany

7. Warunki ko�cowe

	Dane powinny zosta� wy�wietlone w formie tabelki, gdzie ka�dy wiersz reprezentuje jednego klienta i zawiera: jego imi�, nazwisko, g��wny numer telefonu, g��wny mail, przycisk do wej�cia w ekran edycji.

8. G��wny przep�yw zdarze�

	a. u�ytkownik po zalogowaniu przechodzi do ekranu z list� klient�w.
	b. system wy�wietla list�  20 ostatnio edytowanych klient�w 
	c. system umo�liwia przej�cie na kolejn� stron� z kolejnymi klientami
	d. przy ka�dym kliencie jest link do edycji
	e. na g�rze strony znajduje si� formularz wyszukiwania klienta
	f. na g�rze strony znajduje si� link do dodania nowego klienta.

9. Alternatywny przep�yw zdarze�.

	brak.

10. Specjalne wymagania.
	
	brak.

==========================================

1. Nazwa

	przydzielanie konsultant�w pierwszej linii i konsultant�w drugiej linii do klient�w

2. Identyfikator

	p5

3. Aktorzy

	konsultant, kierownik

4. Udzia�owcy / zainteresowani i cele

	konsultant - chce wiedzie� kt�rych klient�w powinien obs�ugiwa�
	kierownik - chce wiedzie� kt�rych klient�w nikt nie ods�uguje, chce mie� mo�liwo�� zmiany przypisania.

5. Kr�tki opis

	przypisanie / zmiana konsultanta mo�liwa jest na ekranie z widokiem danych klienta
	przypisany konsultant jest widoczny na ekranie z list� klient�w, oraz na ekranie z widokiem klienta.

6. Warunki wst�pne

	musi istnie� konsultant, klient.

7. Warunki ko�cowe

	konsultant jest powi�zany z klientem i widoczny na ekranie danych klienta oraz li�cie klient�w

8. G��wny przep�yw zdarze�

	a. system na ekranie danych klient�w wy�wietla pole : przypisany konsultant
	b. u�ytkownik klika w pole i widzi list� rozwijaln� z konsultantmi spo�r�d kt�rych wybiera jednego.
	c. u�ytkownik klika zapisz.
	d. system zapisuje informacje

9. Alternatywny przep�yw zdarze�.

	brak

10. Specjalne wymagania.

	brak

==========================================

1. Nazwa

	wprowadzanie zam�wienia na produkt 

2. Identyfikator

	p6

3. Aktorzy

	konsultant

4. Udzia�owcy / zainteresowani i cele

	konsultant - wprowadzenie zam�wienia na produkt.

5. Kr�tki opis

	konsultant mo�e wprowadzi� zam�wienie na produkt i ustali� jego status jako: zam�wienie z�o�one, rezerwacja, umowa podpisana, zam�ienie op�acone, zam�wienie dostarczone.

6. Warunki wst�pne

	istnieje ju� klient.

7. Warunki ko�cowe

	zam�wienie zostaje zapisane w bazie.

8. G��wny przep�yw zdarze�

	a. u�ytkownik na ekranie danych klienta oraz na li�cie klient�w klika w link: dodaj zam�wienie
	b. otwiera si� okno zam�wienia
	c. w oknie zam�wienia wy�wietla si� imi� i nazwisko klienta oraz link do ekranu danych klienta
	d. u�ytkownik w pole wyszukiwania produktu wpisuje unikalny numer produktu
	e. system wyszukuje produkt w bazie i wy�wietla jego szczeg�y
	f. system wy�wietla list� wybieraln� z domy�lnym statusem: "zam�wienie z�o�one"
	g. u�ytkownik mo�e wybra� status z listy
	h. u�ytkownik klika zapisz
	i. system zapami�tuje zam�wienie
	j. system wy�wietla na li�cie klient�w informacje o zam�wieniu i link do edycji zam�wienia
	

9. Alternatywny przep�yw zdarze�.

	brak

10. Specjalne wymagania.

	brak

==========================================

1. Nazwa

	- dodawanie uwag

2. Identyfikator

	p7

3. Aktorzy

	konsultant

4. Udzia�owcy / zainteresowani i cele

	konsultant - umieszczenie dowolnej notatki w danych klienta i/lub w danych zam�wienia

5. Kr�tki opis

	konsultant powinien mie� mo�liwo�� umieszczenia notatki zar�wno w danych klienta jak i w danych zam�wienia, oraz przegl�dania historii notatek wraz z datami, edycja raz umieszczonej notatki nie powinna by� mo�liwa.

6. Warunki wst�pne

	istnieje klient i opcjonalnie zam�wienie dla tego klienta.

7. Warunki ko�cowe

	Notatka zostaje zapisana i wy�wietlona w historii

8. G��wny przep�yw zdarze�

	a. u�ytkownik wchodzi na widok klienta / widok zam�wienia
	b. system wy�wietla na dole strony pole do wpisania notatki i znajduj�cy si� obok przycisk / ikonk� zapisz notatk�
	c. u�ytkownik wype�nia notatk� i klika zapisz.
	d. system zapami�tuje notatk�
	e. system wy�wietla notatk� w historii znajduj�cej si� poni�ej notatki wraz z dat� dodania.

9. Alternatywny przep�yw zdarze�.

	brak

10. Specjalne wymagania.

	brak
