<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="java.util.*,model.*,controller.*, org.hibernate.*,org.hibernate.cfg.*, java.util.Iterator"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel = "stylesheet" href = "style.css" type = "text/css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task manager</title>
</head>
<body>
	<h1>${sessionScope.login} here are your tasks</h1>
	<form action="./Tasks?akcja=saveTask" method="POST">
		<table>
			<tr>
				<td>Title</td><td>Content</td>
			</tr>
			<tr>
				<td><input name = "title" type="text" /></td><td><input name = "content" type="text" /></td><td><input value="Save" type="Submit" /></td>
			</tr>
		</table>
	</form>
	<table>
		<tr>
			<td>Id</td>
			<td>Title</td>
			<td>Content</td>
		</tr>
		<br>
		<%
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();
			HttpSession ses = request.getSession();
			String loggedUser = (String) ses.getAttribute("login");
			try {
				List data = s.createQuery("FROM TasksModel WHERE ownerLogin = '" + loggedUser +"'" ).list();
				for (Iterator i = data.iterator(); i.hasNext();) {
					TasksModel u = (TasksModel) i.next();
				%>
				<tr>
					<td>
						<%
							out.println(u.getId()) ;
						%>
					</td>
					<td>
						<%
							out.println(u.getTitle()) ;
						%>
					</td>
					<td>
						<%
							out.println(u.getContent()) ;
						%>
					</td>
					<td>
						<a href = "./Tasks?akcja=removeTask&taskId=<%out.println(u.getId()) ; %>">Remove</a>	
					</td>
				</tr>
				<%
			}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				s.close();
			}
		%>
	</table>
	<br>
</body>
</html>