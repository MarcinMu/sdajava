<%@page import="java.util.Iterator"%>
<%@ page import="controller.*, model.*, java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Roles managing</title>
</head>
<body>
	<h2>Roles managing</h2>
	<h3>Existing users:</h3>
	<table>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Number of users</th>
			<th>Action</th>
		</tr>
		<%
		List<RolesModel> roles = new RolesController().getAllRoles();
		for(Iterator i = roles.iterator(); i.hasNext();){
			RolesModel role = (RolesModel) i.next();
			
			%>
			<tr>
				<td><%out.println(role.getId()); %></td>
				<td><%out.println(role.getRoleName()); %></td>
				<td></td>
				<td><a href = "./Roles?action=edit&id=<% out.println(role.getId()); %>">Edit</a>&bull;
				    <a href = "./Roles?action=delete&id=<% out.println(role.getId()); %>">Delete</a></td>
			</tr>
			<%
		}
		%>
	</table>
	<h3>Add role: </h3>
	<form action="./Roles?action=add" method = "POST">
		Name: <input type = "text" name = "roleName"/>
		<input type= "submit" value= "save"/>
	</form>
</body>
</html>