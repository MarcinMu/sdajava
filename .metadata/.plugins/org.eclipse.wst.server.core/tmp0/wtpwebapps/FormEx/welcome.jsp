<%@ page language="java" contentType="text/html; charset=ISO-8859-2"
	pageEncoding="UTF-8"%>
<%@ page
	import="java.util.*,model.*,controller.*, org.hibernate.*,org.hibernate.cfg.*, java.util.Iterator"%>
<%boolean isAdmin = session.getAttribute("role").toString().equalsIgnoreCase("0"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">
<link rel = "stylesheet" href = "style.css" type = "text/css">
<title>Insert title here</title>
</head>
<body>
	<h2>Welcome ${sessionScope.login}!</h2>
	<ul>
	<li> <a href="./Zaloguj?akcja=wyloguj"> Log Out  </a> </li>
	<li> <a href ="./taskManaging.jsp" >Manage your tasks </a> </li>
	<% if(isAdmin) { %>
		<li> <a href = "./Users.jsp" > Manage users </a>  </li>
		<li> <a href = "./RolesManaging.jsp" > Manage Roles </a> </li>
	<% } %>
	</ul>

</body>
</html>